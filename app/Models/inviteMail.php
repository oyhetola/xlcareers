<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class inviteMail extends Model
{
    protected $fillable = ['recipient', 'subject', 'message', 'sender', 'job_id'];

    Public function job(){
        return $this->belongsTo('App\jobOpening', 'job_id', 'id');
    }
}
