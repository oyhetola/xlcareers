<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class candidate extends Model
{
    protected $fillable = ['email', 'firstname', 'lastname', 'middlename', 'resume', 'cover_letter',
                           'country_id', 'state_id', 'job_id', 'source_of_info', 'job_number'];

    public function jobOpening(){
        return $this->belongsTo('App\jobOpening', 'job_id', 'id');
    }

    public function country(){
        return $this->belongsTo('App\Models\country', 'country_id', 'id');
    }

    public function state(){
        return $this->belongsTo('App\Models\state', 'state_id', 'id');
    }
}
