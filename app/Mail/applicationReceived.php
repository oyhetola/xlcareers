<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class applicationReceived extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    Public $name;
    Public $jobid;
    public function __construct($user)
    {
        $this->name = $user->firstname;
        $this->jobid= $user->job_number;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Emails.successfulApplication')->subject('APPLICATION RECEIVED')->from('no-reply@xlafricagroup.com', 'CAREERS@XLAFRICAGROUP');
    }
}
