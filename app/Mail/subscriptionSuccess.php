<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class subscriptionSuccess extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    Public $identity;
    public function __construct($subscribe)
    {
        $this->identity = $subscribe->id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Emails.subscriptionReceived')->subject('JOB POSTING NOTIFICATION SUBSCRIPTION SUCCESSFUL.')->from('no-reply@xlafricagroup.com');
    }
}
