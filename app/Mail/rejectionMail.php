<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class rejectionMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $recipient, $jobnumber, $jobtitle;
    public function __construct($candidate)
    {
        $this->recipient = $candidate->firstname;
        $this->jobnumber = $candidate->job_number;
        $this->jobtitle = $candidate->jobOpening->title;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Emails.rejectMail')->subject('UPDATE ON YOUR APPLICATION')->from('no-reply@xlafricagroup.com', 'no-reply@xlafricagroup.com')->replyTo(Auth::user()->email);
    }
}
