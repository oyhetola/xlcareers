<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class interviewInviteMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $body;
    public $replyaddress;
     public $title;

//
    public function __construct($body, $sub)
    {
        $this->body = $body;
        $this->replyaddress = Auth::user()->email;
        $this->title = $sub;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Emails.inviteMail')->subject($this->title)->from($this->replyaddress, 'CAREERS@XLAFRICAGROUP')->replyTo($this->replyaddress);
//        return $this->view('Emails.inviteMail')->subject('lop')->from('o.abe@xlafricagroup.com', 'CAREERS@XLAFRICAGROUP');
    }
}
