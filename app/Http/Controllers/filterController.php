<?php

namespace App\Http\Controllers;

use App\jobOpening;
use App\Models\approvedCandidate;
use App\Models\candidate_educationInfo;
use App\Models\candidate_personalInfo;
use App\Models\clientInfo;
use App\Models\employedCandidate;
use App\Models\recommendedCandidate;
use App\Models\result;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;

class filterController extends Controller
{
    function filterCandidates(Request $request){
        return $result = result::distinct()->select('user_id')->get();
        /*switch ($request->candidateType){
            case "tested":
                for($i=0; $i<count($request->all()); $i++){
                    if($request->country != "All"){
                       $result =  $result->where();
                    }
                    if()
                }
        }*/
    }

    function filterRecommended(Request $request){
        //return $request->all();
        $lnp = [];
        if($request->job == "All" && $request->client == "All"){
            $rcandidates = recommendedCandidate::all();
            for($i=0; $i < count($rcandidates); $i++){
                array_push($lnp, $rcandidates[$i]->user->personalInfo);
                $lnp[$i]['firstname'] = $rcandidates[$i]->user->firstname;
                $lnp[$i]['lastname'] = $rcandidates[$i]->user->lastname;
                $lnp[$i]['resume'] = $rcandidates[$i]->user->resume;
                $r = $rcandidates[$i]->user->result;
                $sendBack = [];
                foreach ($r as $d){
                    $category = $d->category->category;
                    $score = $d->score;
                    array_push($sendBack, $category." : ".$score);
                }
                $lnp[$i]['result'] = implode(',', $sendBack);
            }

            return response()->json(['rcandidates'=>$lnp]);
        }else {
            if($request->job != "All"){
                $rcandidates = recommendedCandidate::where('jobOpening_id', $request->job);
                $rcandidates = $rcandidates->get();

                for($i=0; $i < count($rcandidates); $i++){
                    array_push($lnp, $rcandidates[$i]->user->personalInfo);
                    $lnp[$i]['firstname'] = $rcandidates[$i]->user->firstname;
                    $lnp[$i]['lastname'] = $rcandidates[$i]->user->lastname;
                    $lnp[$i]['resume'] = $rcandidates[$i]->user->resume;
                    $r = $rcandidates[$i]->user->result;
                    $sendBack = [];

                    foreach ($r as $d){
                        $category = $d->category->category;
                        $score = $d->score;
                        array_push($sendBack, $category." : ".$score);
                    }
                    $lnp[$i]['result'] = implode(',', $sendBack);
                }
                return response()->json(['rcandidates'=>$lnp]);
            }
            return response()->json(['rcandidates'=>[]]);
        }

        ///recommendedCandidate::where()
    }


    function filterApproved(Request $request){
        $lnp = [];
        if($request->job == "All"){
            $rcandidates = approvedCandidate::all();
            for($i=0; $i < count($rcandidates); $i++){
                array_push($lnp, $rcandidates[$i]->user->personalInfo);
                $lnp[$i]['firstname'] = $rcandidates[$i]->user->firstname;
                $lnp[$i]['lastname'] = $rcandidates[$i]->user->lastname;
                $lnp[$i]['resume'] = $rcandidates[$i]->user->resume;
            }

            return response()->json(['rcandidates'=>$lnp]);
        }else{
            if($request->job != "All"){
                $rcandidates = approvedCandidate::where('jobOpening_id', $request->job);
                $rcandidates = $rcandidates->get();

                for($i=0; $i < count($rcandidates); $i++){
                    array_push($lnp, $rcandidates[$i]->user->personalInfo);
                    $lnp[$i]['firstname'] = $rcandidates[$i]->user->firstname;
                    $lnp[$i]['lastname'] = $rcandidates[$i]->user->lastname;
                    $lnp[$i]['resume'] = $rcandidates[$i]->user->resume;
                    $r = $rcandidates[$i]->user->result;
                    $sendBack = [];

                    foreach ($r as $d){
                        $category = $d->category->category;
                        $score = $d->score;
                        array_push($sendBack, $category." : ".$score);
                    }
                    $lnp[$i]['result'] = implode(',', $sendBack);
                }
                return response()->json(['rcandidates'=>$lnp]);
            }
        }

    }

    function filterApplied(Request $request){
        $lnp = [];
        if($request->job == "All" && $request->client == "All"){
            $rcandidates = recommendedCandidate::all();
            for($i=0; $i < count($rcandidates); $i++){
                array_push($lnp, $rcandidates[$i]->user->personalInfo);
                $lnp[$i]['firstname'] = $rcandidates[$i]->user->firstname;
                $lnp[$i]['lastname'] = $rcandidates[$i]->user->lastname;
                $lnp[$i]['resume'] = $rcandidates[$i]->user->resume;
                $r = $rcandidates[$i]->user->result;
                $sendBack = [];
                foreach ($r as $d){
                    $category = $d->category->category;
                    $score = $d->score;
                    array_push($sendBack, $category." : ".$score);
                }
                $lnp[$i]['result'] = implode(',', $sendBack);
            }
            $ne =[];
            if($request->discipline != null && $request->discipline != ""){
                $arrayOfDisciplines = candidate_educationInfo::where('fieldOfStudy', 'LIKE', '%'.$request->discipline.'%' )->select('user_id', 'fieldOfStudy')->distinct()->get();
                for($i=0; $i < count($arrayOfDisciplines); $i++){
                    array_push($ne, $arrayOfDisciplines[$i]->user_id);
                }
            }
            $new = [];
            for($i=0; $i < count($lnp); $i++){

                    if($request->discipline != null && $request->discipline != ""){
                        if(in_array($lnp[$i]['user_id'], $ne)){
                            array_push($new, $lnp[$i]);
                        }
                    }else{
                        array_push($new, $lnp[$i]);
                    }

            }

            return response()->json(['rcandidates'=>$new]);
        }else{
            if($request->job != "All"){
                $rcandidates = recommendedCandidate::where('jobOpening_id', $request->job);
                $rcandidates = $rcandidates->get();

                for($i=0; $i < count($rcandidates); $i++){
                    array_push($lnp, $rcandidates[$i]->user->personalInfo);
                    $lnp[$i]['firstname'] = $rcandidates[$i]->user->firstname;
                    $lnp[$i]['lastname'] = $rcandidates[$i]->user->lastname;
                    $lnp[$i]['resume'] = $rcandidates[$i]->user->resume;
                    $r = $rcandidates[$i]->user->result;
                    $sendBack = [];

                    foreach ($r as $d){
                        $category = $d->category->category;
                        $score = $d->score;
                        array_push($sendBack, $category." : ".$score);
                    }
                    $lnp[$i]['result'] = implode(',', $sendBack);
                }
                $ne =[];
                if($request->discipline != null && $request->discipline != ""){
                    $arrayOfDisciplines = candidate_educationInfo::where('fieldOfStudy', 'LIKE', '%'.$request->discipline.'%' )->select('user_id', 'fieldOfStudy')->distinct()->get();
                    for($i=0; $i < count($arrayOfDisciplines); $i++){
                        array_push($ne, $arrayOfDisciplines[$i]->user_id);
                    }
                }
                $new = [];
                for($i=0; $i < count($lnp); $i++){

                    if($request->discipline != null && $request->discipline != ""){
                        if(in_array($lnp[$i]['user_id'], $ne)){
                            array_push($new, $lnp[$i]);
                        }
                    }else{
                        array_push($new, $lnp[$i]);
                    }

                }
                return response()->json(['rcandidates'=>$lnp]);
            }
            return response()->json(['rcandidates'=>[]]);
        }

        ///recommendedCandidate::where()
    }

    function filterTested(Request $request){
        try{

            /*if($request->discipline != null && $request->discipline != ""){
                $dis = candidate_educationInfo::where('fieldOfStudy', 'LIKE', '%'.$request->discipline.'%')->first();
                $users = candidate_personalInfo::where('id', '<>', null)->where('user_id', $dis->user_id);
            }else{
                $users = candidate_personalInfo::where('id', '<>', null);
            }*/
            $users = candidate_personalInfo::select('user_id', 'countryOfResidence_id', 'stateOfResidence_id', 'regionOfResidence_id','date_of_birth');
            $getresult = result::select('user_id')->distinct()->get();
            $results = [];
            foreach ($getresult as $result){
                array_push($results, $result->user_id);
            }

            if($request->country != "All"){
                $users = $users->where('countryOfResidence_id', $request->country);
            }
            if($request->region != "All"){
                $users = $users->where('regionOfResidence_id', $request->region);
            }
            if($request->state != "All"){
                $users = $users->where('stateOfResidence_id', $request->state);
            }
            if($request->agestart != "All" && $request->ageend != "All"){
                $l = date('Y') - $request->agestart;
                $r = date('Y') - $request->ageend;
                $l = $l."-12-31";
                $r = $r."-01-01";
                $users = $users->whereBetween('date_of_birth', [$r,$l]);
            }elseif ($request->agestart != "All" && $request->ageend == "All"){
                $l = date('Y') - $request->agestart;
                $l = $l."-12-31";
                $r = date('Y')-70;
                $r = $r."-01-01";
                //return $l." ".$r;
                $users = $users->whereBetween('date_of_birth', [$r,$l]);
            }



            $rcandidates = $users->get();
            $lnp =[];
            for($i=0; $i < count($rcandidates); $i++){
                array_push($lnp, $rcandidates[$i]);
                $lnp[$i]['firstname'] = $rcandidates[$i]->user->firstname;
                $lnp[$i]['lastname'] = $rcandidates[$i]->user->lastname;
                $lnp[$i]['resume'] = $rcandidates[$i]->user->resume;
                $r = $rcandidates[$i]->user->result;


                $sendBack = [];

                foreach ($r as $d){
                    $category = $d->category->category;
                    $score = $d->score;
                    array_push($sendBack, $category." : ".$score);
                }
                $lnp[$i]['result'] = implode(',', $sendBack);
            }
            $ne =[];
            if($request->discipline != null && $request->discipline != ""){
                $arrayOfDisciplines = candidate_educationInfo::where('fieldOfStudy', 'LIKE', '%'.$request->discipline.'%' )->select('user_id', 'fieldOfStudy')->distinct()->get();
                for($i=0; $i < count($arrayOfDisciplines); $i++){
                    array_push($ne, $arrayOfDisciplines[$i]->user_id);
                }
            }
            $holdLevels = [];
            if($request->educationLevel != "All"){
                $arrayOfEduLevels = candidate_educationInfo::where('educationLevel', 'LIKE', '%'.$request->educationLevel.'%' )->select('user_id', 'educationLevel')->distinct()->get();
                for($i=0; $i < count($arrayOfEduLevels); $i++){
                    array_push($holdLevels, $arrayOfEduLevels[$i]->user_id);
                }
            }
            $new = [];
            for($i=0; $i < count($lnp); $i++){
                if(in_array($lnp[$i]['user_id'], $results)){
                    if($request->discipline != null && $request->discipline != ""){
                        if(in_array($lnp[$i]['user_id'], $ne)){
                            array_push($new, $lnp[$i]);
                        }
                    }else{
                        array_push($new, $lnp[$i]);
                    }
                }
            }

            if($request->educationLevel != "All"){
                $latest = [];
                for($i=0; $i < count($new); $i++){
                    if(in_array($new[$i]['user_id'], $holdLevels)){
                        array_push($latest, $new[$i]);
                    }
                }
                return response()->json(['rcandidates'=>$latest]);
            }

            return response()->json(['rcandidates'=>$new]);
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    function filterAll(Request $request){
        try{
            if($request->discipline != null && $request->discipline != ""){
                $dis = candidate_educationInfo::where('fieldOfStudy', 'LIKE', '%'.$request->discipline.'%')->first();
                $users = candidate_personalInfo::select('user_id', 'date_of_birth')->where('user_id', $dis->user_id);
            }else{
                $users = candidate_personalInfo::select('user_id', 'date_of_birth', 'regionOfResidence_id','countryOfResidence_id','stateOfResidence_id');
            }

            //$users = candidate_personalInfo::all();
            if($request->country != "All"){
                $users = $users->where('countryOfResidence_id', $request->country);
            }
            if($request->region != "All"){
                $users = $users->where('regionOfResidence_id', $request->region);
            }
            if($request->agestart != "All" && $request->ageend != "All"){
                $l = date('Y') - $request->agestart;
                $r = date('Y') - $request->ageend;
                $l = $l."-12-31";
                $r = $r."-01-01";
                $users = $users->whereBetween('date_of_birth', [$r,$l]);
            }elseif ($request->agestart != "All" && $request->ageend == "All"){
                $l = date('Y') - $request->agestart;
                $l = $l."-12-31";
                $r = date('Y')-70;
                $r = $r."-01-01";
                //return $l." ".$r;
                $users = $users->whereBetween('date_of_birth', [$r,$l]);
            }
            if($request->state != "All"){
                $users = $users->where('stateOfResidence_id', $request->state);
            }

//            $rcandidates =  $users->get();
            $users->get();
            $lnp =[];
            for($i=0; $i < count($users->get()); $i++){
                array_push($lnp, $users->get()[$i]);
                $lnp[$i]['firstname'] = $users->get()[$i]->user->firstname;
                $lnp[$i]['lastname'] = $users->get()[$i]->user->lastname;
                $lnp[$i]['resume'] = $users->get()[$i]->user->resume;

            }
            return response()->json(['rcandidates'=>$lnp]);
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    function  filterAccepted(Request $request){
        $lnp = [];
        if($request->job == "All" && $request->client == "All"){
            $rcandidates = employedCandidate::all();
            for($i=0; $i < count($rcandidates); $i++){
                array_push($lnp, $rcandidates[$i]->user->personalInfo);
                $lnp[$i]['firstname'] = $rcandidates[$i]->user->firstname;
                $lnp[$i]['lastname'] = $rcandidates[$i]->user->lastname;
                $lnp[$i]['resume'] = $rcandidates[$i]->user->resume;
                $r = $rcandidates[$i]->user->result;
                $sendBack = [];
                foreach ($r as $d){
                    $category = $d->category->category;
                    $score = $d->score;
                    array_push($sendBack, $category." : ".$score);
                }
                $lnp[$i]['result'] = implode(',', $sendBack);
            }

            return response()->json(['rcandidates'=>$lnp]);
        }else {
            if($request->job != "All"){
                $rcandidates = employedCandidate::where('jobOpening_id', $request->job);
                $rcandidates = $rcandidates->get();

                for($i=0; $i < count($rcandidates); $i++){
                    array_push($lnp, $rcandidates[$i]->user->personalInfo);
                    $lnp[$i]['firstname'] = $rcandidates[$i]->user->firstname;
                    $lnp[$i]['lastname'] = $rcandidates[$i]->user->lastname;
                    $lnp[$i]['resume'] = $rcandidates[$i]->user->resume;
                    $r = $rcandidates[$i]->user->result;
                    $sendBack = [];

                    foreach ($r as $d){
                        $category = $d->category->category;
                        $score = $d->score;
                        array_push($sendBack, $category." : ".$score);
                    }
                    $lnp[$i]['result'] = implode(',', $sendBack);
                }
                return response()->json(['rcandidates'=>$lnp]);
            }
            return response()->json(['rcandidates'=>[]]);
        }
    }

    function clientSwitchFilter(Request $request){
        $rcandidates =[];
        if($request->param == "Accepted"){
            $rcandidates = employedCandidate::where('jobOpening_id', $request->job)->get();
        }elseif($request->param == "Recommended"){
            $rcandidates = recommendedCandidate::where('jobOpening_id', $request->job)->get();
        }
        //return $rcandidates;
        $lnp = [];
        if($rcandidates){
            for($i=0; $i < count($rcandidates); $i++){
                array_push($lnp, $rcandidates[$i]->user->personalInfo);
                $lnp[$i]['firstname'] = $rcandidates[$i]->user->firstname;
                $lnp[$i]['lastname'] = $rcandidates[$i]->user->lastname;
                $lnp[$i]['resume'] = $rcandidates[$i]->user->resume;
                $r = $rcandidates[$i]->user->result;
                $sendBack = [];
                foreach ($r as $d){
                    $category = $d->category->category;
                    $score = $d->score;
                    array_push($sendBack, $category." : ".$score);
                }
                $lnp[$i]['result'] = implode(',', $sendBack);
            }

            return response()->json(['rcandidates'=>$lnp]);
        }else{
            return response()->json(['rcandidates'=>$lnp]);
        }

    }
}
