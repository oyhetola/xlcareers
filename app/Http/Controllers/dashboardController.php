<?php

namespace App\Http\Controllers;

use App\jobOpening;
use App\Mail\applicationReceived;
use App\Mail\subscriptionSuccess;
use App\Mail\successfulApplication;
use App\Mail\testSubmissionMail;
use App\Models\candidate;
use App\Models\candidate_educationInfo;
use App\Models\candidate_experienceInfo;
use App\Models\candidate_personalInfo;
use App\Models\country;
use App\Models\employedCandidate;
use App\Models\jobApplication;
use App\Models\jobApplicationModel;
use App\Models\objectiveQuestion;
use App\Models\questionCategory;
use App\Models\recommendedCandidate;
use App\Models\result;
use App\Models\state;
use App\subscriber;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class dashboardController extends Controller
{
    function index(){
        if(Auth::check()){
            if(Auth::user()->user_role == "candidate"){
                if(Cookie::get('applyJobUrl') != null){
                    return redirect(route('applyForJob', ['Knvp0lkVmQn'=>Cookie::get('applyJobUrl')]));
                }else{
                    $results = result::where('user_id', Auth::id())->distinct('category_id')->get();
                    return view('dashboard', ['testscores'=>$results]);
                }

            }elseif(Auth::user()->user_role == "client"){
                $jobs = jobOpening::where('client_id', Auth::id())->get();
                $jobss = count($jobs);
                if(count($jobs) > 0){
                    $totalEmployment  = employedCandidate::where('jobOpening_id', $jobs[0]->id)->get();
                    $totalEmployment = count($totalEmployment);
                }else{
                    $totalEmployment = 0;
                }



                return view('clients.index', ['jobs'=>$jobss, 'totalEmployment'=>$totalEmployment]);
            }elseif(Auth::user()->user_role == "examiner"){
                $candidates = User::where('user_role', 'candidate')->count();
                $tested = result::distinct()->count('user_id');
                $clients = User::where('user_role', 'client')->count();
                $signups = User::where('user_role', 'candidate')->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->count();
                $recommended = recommendedCandidate::whereBetween('created_at', [Carbon::now('Africa/Lagos')->startOfWeek(), Carbon::now('Africa/Lagos')->endOfWeek()])->count();
                $accepted = employedCandidate::whereBetween('created_at', [Carbon::now('Africa/Lagos')->startOfMonth(), Carbon::now('Africa/Lagos')->endOfMonth()])->count();
                return view ('examiner.index', ['candidates'=>$candidates, 'tested'=>$tested, 'clients'=>$clients, 'recommended'=>$recommended, 'signups'=>$signups, 'accepted'=>$accepted]);
            }elseif(Auth::user()->user_role == "demo"){
                if(Cookie::get('applyJobUrl') != null){
                    return redirect(route('applyForJob', ['Knvp0lkVmQn'=>Cookie::get('applyJobUrl')]));
                }else{
                    $results = result::where('user_id', Auth::id())->distinct('category_id')->get();
                    return view('demo', ['testscores'=>$results]);
                }
            }
        }else{
            return redirect()->route('viewLogin');
        }

    }

    function getCountries(){
        $countries = country::get();
        return response()->json(['countries'=>$countries], 200);
    }

    function getStates(){
        $states = state::get();
        return response()->json(['states'=>$states], 200);
    }

    function  educationDetails(){
        if (Auth::user()){

            return view('educationViewPage');
        }else{
            return view('login');
        }
    }

    function  experienceDetails(){
        if (Auth::user()){
            return view('experienceViewPage');
        }else{
            return view('login');
        }
    }


    function submitPersonalInfo(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                //'nextOfKin' => 'required',
                //'nextOfKinPhoneNum' => 'required',
                //'nextOfKinAddress' => 'required',
                'phoneNumber' => 'required|min:11',
                'country' => 'required',
                'dateOfBirth' => 'required',
                'countryOfResidence' => 'required',
                'regionOfResidence' => 'required',
                'address' => 'required',
            ]);

            if ($validator->fails()) {
                return $validator->errors();
            }

            $candidate = candidate_personalInfo::create([
                'user_id'=> Auth::id(),
                'phoneNumber' => $request->phoneNumber,
                'date_of_birth' => $request->dateOfBirth,
                'country_id' => $request->country,
                'stateOfOrigin_id' => $request->stateOfOrigin,
                'countryOfResidence_id' => $request->countryOfResidence,
                'regionOfResidence_id' => $request->regionOfResidence,
                'stateOfResidence_id' => $request->stateOfResidence,
                'address' => $request->address,
                'nextOfKin' => $request->nextOfKin,
                'nextOfKinPhoneNum' => $request->nextOfKinPhoneNum,
                'nextOfKinAddress' => $request->nextOfKinAddress,
            ]);

            if($candidate){
                $candidate->user->personalInfo_filled = 1;
                $candidate->user->update();
                return response()->json(['success'=>'success'], 200);
            }else{
                return response()->json(['error'=>'Error occurred while submitting your request try again']);
            }
        }catch (\Exception $e){
            return response()->json(['error'=>'Error occurred while submitting your request try again']);
        }
    }

    function submitEducationInfo(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'schoolName' => 'required',
                'schoolCountry' => 'required',
                'schoolStartMonth' => 'required',
                'schoolStartYear' => 'required',
                'fieldOfStudy' => 'required',
            ]);
            if ($validator->fails()) {
                return $validator->errors();
            }
            $candidate = candidate_educationInfo::create([
                'user_id'=> Auth::id(),
                'school_name' => $request->schoolName,
                'schoolCountry_id' => $request->schoolCountry,
                'schoolState_id' => $request->schoolState,
                'schoolStart_month' => $request->schoolStartMonth,
                'schoolEnd_month' => $request->schoolEndMonth,
                'schoolStart_year' => $request->schoolStartYear,
                'schoolEnd_year' => $request->schoolEndYear,
                'currentSchool_status' => $request->current,
                'fieldOfStudy' => $request->fieldOfStudy,
                'educationLevel' => $request->educationLevel,
            ]);
            if($candidate){
                $candidate->user->educationInfo_filled = 1;
                $candidate->user->update();
                return response()->json(['success'=>['success'=>'success', 'info'=>$candidate]], 200);
            }else{
                return response()->json(['error'=>'Error in submitting your request, try again']);
            }
        }catch (\Exception $e){
            return response()->json(['error'=>'Error in submitting your request, try again']);
        }

    }

    function submitExperienceInfo(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'companyName' => 'required',
                'jobTitle' => 'required',
                'workStartMonth' => 'required',
                'workStartYear' => 'required',
            ]);
            if ($validator->fails()) {
                return $validator->errors();
            }

            $candidate = candidate_experienceInfo::create([
                'user_id'=> Auth::id(),
                'company_name'=> $request->companyName,
                'job_title'=> $request->jobTitle,
                'currentWorkStatus'=> $request->current,
                'workStart_month'=> $request->workStartMonth,
                'workEnd_month'=> $request->workEndMonth,
                'workStart_year'=> $request->workStartYear,
                'workEnd_year'=> $request->workEndYear,
                'job_description'=> $request->jobDescription,
            ]);

            if($candidate){
                $candidate->user->experienceInfo_filled = 1;
                $candidate->user->update();
                return response()->json(['success'=>['success'=>'success', 'info'=>$candidate]], 200);
            }else{
                return response()->json(['error'=>'Error in submitting your request try again']);
            }
        }catch (\Exception $e){
            return response()->json(['error'=>'Error in submitting your request try again']);
        }

    }


    function  submitResume(Request $request){
        try{
            $user =  User::where('id', Auth::id())->first();
            if($request->hasFile('theResume')){
                $path = $request->file('theResume');
                $name = $path->getClientOriginalName();
                $ext = $path->getClientOriginalExtension();
                $filename = str_replace('.' . $ext, "", $name);
                if($ext == "pdf" || $ext == "doc" || $ext == "docx"){
                    if (\File::exists(public_path('resume/' . Auth::id() . '/' . $name))) {
                        \File::delete(public_path('resume/' . Auth::id() . '/' . $name));
                        /*$er = rand(0, 10);
                        $name = $filename . $er . '.' . $ext;*/
                        $request->file('theResume')->move('resume/' . Auth::id(), $name);
                    } else {
                        $request->file('theResume')->move('resume/' . Auth::id(), $name);
                    }
                }else{
                    return response()->json(['formatError'=>"Format not supported (.pdf* or .doc* or .docx* required)"]);
                }
                $user->resume = $name;
                $user->resume_status = 1;
                $user->save();
                return response()->json(['success'=>'File successfully uploaded.']);
            }else{
                return response()->json(['nofileError'=>"File needed"]);
            }
            //return $request;
        }catch (\Exception $e){
            return response()->json(['error'=>'Error occurred while submitting your request, please try again']);
        }
    }

    function loadCategories(){
        $categories = questionCategory::all();
        $newCategory = [];
        $temPArr = [];
        foreach ($categories as $category){
            if($category->category_status == 1){
                if($category->taken_by != null){
                    $temPArr = $category->taken_by;
                    if(in_array(Auth::id(), $temPArr)){

                    }else{
                        array_push($newCategory, $category);
                    }
                }else{
                    array_push($newCategory, $category);
                }
                
                //return $temPArr;
                //return $newCategory;
            }else{
                $op = [];
                if($category->assigned_to != null){
                    $temPArr = $category->assigned_to;
                    if(in_array(Auth::id(), $temPArr)){
                        if($category->taken_by != null){
                            $op = $category->taken_by;
                            if(in_array(Auth::id(), $op)){

                            }else{
                                array_push($newCategory, $category);
                            }
                        }else{
                            array_push($newCategory, $category);
                        }
                    }
                }
            }
        }
        // return $newCategory;
        $categories = $newCategory;

        //if(count($newCategory) > 0){
        //   return view('categories', ['categories'=>$categories]);
        //}else{
        return view('categories', ['categories'=>$categories]);
        // }
    }


    function loadQuestions(Request $request){
        try{
            $sub = questionCategory::where('id', $request->catidnty)->first();
            $objQuestions = objectiveQuestion::where(['category_id'=>$request->catidnty])->get();
            foreach ($objQuestions as $objQuestion){
                //$options = [];
                $options = explode(',', $objQuestion->options);
                if(in_array('A', $options) == false){
                    shuffle($options);
                }
                $objQuestion['option'] = $options;
            }
            //return $objQuestions;
            //$theoryQuestions = theoryQuestion::where(['year_id'=>$request->year, 'examType_id'=>$request->examtype, 'subject_id'=>$sub->id])->get();
            return view('takeTest')->with(['objQuestions'=>$objQuestions,  'category'=>$request->catidnty]);
        }catch (\Exception $exception){
            //return $exception->getMessage();
            abort(500);
        }
    }

    function submitAnswers(Request $request){
        try{
            $theAnswers = objectiveQuestion::where(['category_id'=>$request->categoryId])->get(['id','answer']);
            $marks = [];
            $corrections = [];

            for($i=0; $i< sizeof($request->objAnswers); $i++){
                if($theAnswers[$i]->answer == $request->objAnswers[$i]){
                    array_push($marks, 1);
                }else{
                    array_push($marks, 0);
                    if(!in_array($theAnswers[$i]->id, $corrections)){
                        array_push($corrections, $theAnswers[$i]->id);
                    }

                }
            }


            $score = array_sum($marks);
            $total = ($score/sizeof($theAnswers))*100;
            $result = result::create([
                'user_id'=>Auth::id(),
                'category_id'=> $request->categoryId,
                'score' => $total,
                'token' => Str::random(7)
            ]);
            if($total >= 75){
                $result->badge_received = 1;
            }
            if(count($corrections) > 0){
                $result->correction = $corrections;
            }
            $result->save();

            if($result->category->taken_by == null){
                $arraytosave = [];
                array_push($arraytosave, Auth::id());
                $result->category->taken_by = $arraytosave;
                $result->category->save();
            }else{
                $arraytosave = [];
                $arraytosave = $result->category->taken_by;
                if(!in_array(Auth::id(),$arraytosave)){
                    array_push($arraytosave, Auth::id());
                }
                $result->category->taken_by = $arraytosave;
                $result->category->save();
            }
            $examiner = User::where('user_role', 'examiner')->get();
            $sendTo = [];
            foreach ($examiner as $recipient){
                array_push($sendTo, $recipient->email);
            }
            Mail::to($sendTo)->later(Carbon::now()->addMinute(2), new testSubmissionMail(Auth::user(), $result->score));
            return ['token'=>$result];
        }catch (\Exception $e){
            return $e->getMessage();
        }

    }


   function  getResult($token){
        $result = result::where('token', $token)->first();
        return view('testFinal')->with(['result'=>$result]);
   }

   function myProfile(){
       if(Auth::check()){
           if(Auth::user()->user_role == "candidate"){
               return view('profile');
           }elseif (Auth::user()->user_role == "client"){
               return view('clients.profile');
           }elseif (Auth::user()->user_role == "admin"){
               return view('admin.profile');
           }elseif (Auth::user()->user_role == "examiner"){
               return view('examiner.profile');
           }

       }else{
           return redirect(route('viewLogin'));
       }
   }


    function  updateMe(Request $request){
        try{
            if($request != null){
                $user = User::where('id', Auth::id())->first();
                if($user){
                    $user->email = $request->email;
                    $user->firstname = $request->firstname;
                    $user->lastname = $request->lastname;
                    $user->username = $request->username;
                    $user->update();
                    \Session::put('successfulMessage', 'You updated your info.');
                    return redirect()->back();
                }
            }
        }catch (\Exception $e){
            \Session::put('errorMessage', 'Error in connection, Try again');
            return redirect()->back();
        }

    }

    function  updateMe2(Request $request){
        try{
            if($request != null){
                $user = User::where('id', Auth::id())->first();
                if($user){
                    $user->email = $request->email;
                    $user->firstname = $request->firstname;
                    $user->lastname = $request->lastname;
                    $user->username = $request->username;
                    $user->update();
                    return response()->json(['success'=>'Your Info has been successfully updated.']);
                }else{
                    return response()->json(['error'=>'An error occured while trying to update your info.']);
                }
            }
        }catch (\Exception $e){
            \Session::put('errorMessage', 'Error in connection, Try again');
            return redirect()->back();
        }

    }



    function changePassword(Request $request){
        try{
            if(Auth::user()){
                $user = User::findOrFail(Auth::id());
                if(Hash::check($request->oldpassword, $user->password))
                {
                    if($request->newpassword == $request->confirmnewpassword){
                        $user->password = bcrypt($request->newpassword);
                        $user->update();
                        $info = "Password Updated successfully";
                        \Session::put('successfulMessage', $info);
                        return redirect()->back();
                    }else{
                        $error = "Ensure your new password and confirm new password are the same.";
                        \Session::put('errorMessage', $error);
                        return redirect()->back();
                    }
                }else{
                    $error = "Your password does not match what we have in record";
                    \Session::put('errorMessage', $error);
                    return redirect()->back();
                }
            }
        }catch (\Exception $e){
            $error = "There was a an error in connection, Try again";
            \Session::put('errorMessage', $error);
            return redirect()->back();
        }
    }

    function  updateExperienceInfo(Request $request){
        try{
            $experience = candidate_experienceInfo::where('id', $request->id)->first();
            $experience->company_name = $request->companyName;
            $experience->job_title = $request->jobTitle;
            $experience->currentWorkStatus = $request->current;
            $experience->workStart_month = $request->workStartMonth;
            $experience->workEnd_month = $request->workEndMonth;
            $experience->workStart_year = $request->workStartYear;
            $experience->workEnd_year = $request->workEndYear;
            $experience->job_description = $request->jobDescription;
            $experience->update();
            return response()->json(['success'=>'Successfully updated']);
        }catch (\Exception $exception){
            return response()->json(['error'=>'Error detected.'.$exception->getMessage()]);
        }

    }

    function  updateEducationInfo(Request $request){
        try{

            $validator = Validator::make($request->all(), [
                'fieldOfStudy' => 'required',
            ]);
            if ($validator->fails()) {
                return $validator->errors();
            }
            $education = candidate_educationInfo::where('id', $request->id)->first();
            $education->school_name = $request->schoolName;
            $education->schoolCountry_id = $request->schoolCountry;
            $education->schoolState_id = $request->schoolState;
            $education->schoolStart_month = $request->schoolStartMonth;
            $education->schoolStart_year = $request->schoolStartYear;
            $education->schoolEnd_month = $request->schoolEndMonth;
            $education->schoolEnd_year = $request->schoolEndYear;
            $education->currentSchool_status = $request->current;
            $education->fieldOfStudy = $request->fieldOfStudy;
            $education->educationLevel = $request->educationLevel;
            $education->update();
            return response()->json(['success'=>'Successfully updated']);
        }catch (\Exception $exception){
            return response()->json(['error'=>'Error detected.']);
        }

    }

    function allUsers(){
        $users = User::where('user_role', 'admin')->orWhere('user_role', 'examiner')->where('delete_status', 0)->get();
        return view('admin.users', ['users'=>$users]);
    }

    function suspendUser(Request $request){
        try{
            $user = User::where('id', $request->id)->first();
            if($user->suspend == 0){
                $user->suspend = 1;
            }else{
                $user->suspend = 0;
            }
            $user->update();
            return response()->json(['success'=>'suspended'], 200);
        }catch (\Exception $e){
            return response()->json(['error'=>$e->getMessage()], 500);
        }
    }

    function deleteUser(Request $request){
        try{
            $user = User::where('id', $request->id)->first();
            $user->delete_status = 1;
            $user->update();
            return response()->json(['success'=>'deleted'], 200);
        }catch (\Exception $e){
            return response()->json(['error'=>$e->getMessage()], 500);
        }
    }


    function userSetings(Request $request){
        try{
            if(Auth::check()){
                return view('profileSettings');
            }else{
                return redirect(route('viewLogin'));
            }
            
        }catch(\Exception $e){
            return response()->json(['error'=>$e->getMessage()], 500);
        }
    }


    function  jobBoard(){
        $jobs = jobOpening::where('status', 0)->latest()->paginate(6);
        return view('jobBoard', ['jobs'=>$jobs]);
    }

    function applyForJob(Request $request){
//        if(Auth::check()){

            $job = jobOpening::where('job_id', $request->jobId)->first();
            /*$applied = jobApplicationModel::where('candidate_id', Auth::id())->where('jobOpening_id', $request->Knvp0lkVmQn)->first();
            if($applied){
                $applied = 1;
            }else{
                $applied = 0;
            }
            Cookie::queue(
                Cookie::forget('applyJobUrl')
            );*/
            if(Auth::check() && Auth::user()->user_role == "admin"){
                return view('actualJob', ['job'=>$job, 'jobId'=>$request->jobId, 'expired'=>false]);
            }else{
                if($job->status == false &&  $job->deadline > Carbon::now()){
                    return view('actualJob', ['job'=>$job, 'jobId'=>$request->jobId, 'expired'=>false]);
                }elseif($job->status == false && $job->deadline < Carbon::now()){
                    Session::put('errorMessage', 'The job is no longer available to apply to.');
                    return view('actualJob', ['job'=>$job, 'jobId'=>$request->jobId, 'expired'=>true]);
                }elseif($job->status == true &&  $job->deadline < Carbon::now()){
                    Session::put('errorMessage', 'The job is no longer available to apply to.');
                    return view('actualJob', ['job'=>$job, 'jobId'=>$request->jobId, 'expired'=>true]);
                }
            }




        /*}else{
            Cookie::queue('applyJobUrl', $request->Knvp0lkVmQn, 30);
            Session::put('errorMessage', 'You have to be logged in to continue. Please login or Sign up in if you do not already have an account.');
            return redirect(route('viewLogin'));
        }*/
    }

    function signifyInterest(Request $request){
        if(Auth::check()){
            //return $request->all();
            $apply = jobApplicationModel::create([
                'candidate_id' => Auth::id(),
                'jobOpening_id' => $request->job
            ]);
            if($apply){
                Mail::to(Auth::user()->email)->send(new successfulApplication(Auth::user()));
                return response()->json(['success'=> 'You have successfully applied for this job. Your application will be reviewed and we will get back to you.']);
            }else{
                return response()->json(['error'=> 'There was a problem along the line please try again']);
            }
        }else{
            Session::put('errorMessage', 'Please login again');
            return response()->json(['logout'=> 'Ooops! There was a difficulty trying to submit your request, please try again']);
        }
    }

    function myApplications(){
        if(Auth::check()){
            $applications = jobApplication::where('candidate_id', Auth::id())->latest()->paginate(5);
            return view('applications', ['applications'=>$applications]);
        }else{
            Session::put('errorMessage', 'You have to login again');
            return redirect(route('viewLogin'));
        }
    }

    function searchJobs(Request $request){
        if($request->searchKey == "" || $request->searchKey == null){
            $jobs = jobOpening::where('status', 0)->latest()->get();
        }else{
            $jobs = jobOpening::where('status', 0)->where('title', 'LIKE', '%'.$request->searchKey.'%')->latest()->get();
        }

        //return $jobs;
        /*for ($i=0; $i < count($jobs); $i++){
           // $r =
            $jobs[$i]['company'] = $jobs[$i]->user->firstname;
        }*/
        return $jobs;
    }

    function returnToSafety(){

        try{
            //$new =  str_replace(url('/'), '', url()->previous());
            return redirect()->back();
        }catch(\Exception $e){

        }

    }

    function submitapplication(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'firstname' => 'required',
                'lastname' => 'required',
                'email' => 'required|email',
                'phoneNumber' => 'required|min:11',
                'countryOfResidence' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['errors'=>$validator->errors()]);
            }

            if($request->hasFile('resume')){
                $file = $request->file('resume');
                $getNameExtesion = $file->getClientOriginalName();
                $fileExtesion = $file->getClientOriginalExtension();
                $name = str_replace('.' . $fileExtesion, "", $getNameExtesion);

                if ($fileExtesion == "doc"  || $fileExtesion == "docx" || $fileExtesion == "pdf"){
                    $exist = candidate::where('email', $request->email)->where('job_number', $request->jobid)->first();
                    if($exist){
                        return response()->json(['error'=>'You already applied for this job, Please apply to other positions available.']);
                    }
                    $newapplication = candidate::create([
                        'firstname' => $request->firstname,
                        'lastname' => $request->lastname,
                        'middlename' => $request->middlename ? $request->middlename : '',
                        'email' => $request->email,
                        'job_id' => $request->id,
                        'cover_letter' => $request->cover ? $request->cover : '',
                        'country_id' => $request->countryOfResidence,
                        'state_id' => $request->stateofresidence ? $request->stateofresidence :'',
                        'job_number' => $request->jobid
                    ]);
                    $number = candidate::where('job_number', $request->jobid)->count();
//                    $number = $number+1;
                    $file->move('resume/'.$request->jobid.'/'.$number, $getNameExtesion);
                    $newapplication->resume = $number.'/'.$getNameExtesion;
                    $newapplication->save();
                    Mail::to($request->email)->send(new applicationReceived($newapplication));
                    return response()->json(['success'=>1, 'message'=>'Your application has been received']);
                }else{
                    return response()->json(['error'=>"Please check file format"]);
                }

            }else{
                return response()->json(['error'=>"You missed out your cv"]);
            }
        }catch (\Exception $e){
//            return $e->getMessage();
            return response()->json(['error'=>"An unexpected error occureed try again."]);
        }

    }

    function applicationSubmitted(){
        return view('applicationSuccess');
    }

    function applicants(Request $request){
        $applicants = candidate::where('job_number', $request->jobid)->get();
        return view('admin.applicants', ['applicants'=>$applicants]);
    }

    function doSubscribe(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
            ]);

            if ($validator->fails()) {
                return response()->json(['errors'=>$validator->errors()]);
            }
            $subscribe  = subscriber::create([
                'email'=> $request->email
            ]);
            if($subscribe){
                Mail::to($request->email)->send(new subscriptionSuccess($subscribe));
                return response()->json(['success'=>'You successfully subscribed.']);
            }
        }catch (\Exception $error){
            return response()->json(['error'=>"An unexpected error occureed try again."]);
        }
    }

    function unSubscribe(Request $request){
        try{
            subscriber::where('id', $request->aSubUser)->delete();
            return view('unsubriptionsuccess');
        }catch (\Exception $e){
            abort(503);
        }

    }
}
