<?php

namespace App\Http\Controllers;

use App\Mail\confirmationMail;
use App\Mail\newClientAlert;
use App\Mail\notifyNewAdminUser;
use App\Mail\recoverPassword;
use App\Models\clientInfo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class registerController extends Controller
{
    Public function renderRegister(){
        return view('register');
    }

    public $successStatus = 200;
  
    function register(Request $request) {
        try{

            $validator = Validator::make($request->all(), [
                'firstname' => 'required',
                'lastname' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required',
                'Confirm_Password' => 'required|same:password',
            ])->validate();
            /*if ($validator->fails()) {
                return response()->json(['error'=> $validator->errors()], 401);
            }*/

            /*$validateData = $request->validate([
                'firstname' => 'required',
                'lastname' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required',
                'Confirm_Password' => 'required|same:password',
            ]);*/
            //return $validateData;

            $input= $request->all();
            $input['password'] = Hash::make($request->password);
            $input['user_role'] = 'candidate';
            $input['confirmationToken'] = Str::random(50);
            $user = User::create($input);
            //$success['token'] =  $user->createToken('TalentHub')->accessToken;
            if($user){
                Mail::to($user->email)->send(new confirmationMail($user));
//                Mail::to($user->email)->later(Carbon::now('Africa/Lagos')->addMinute(2), new confirmationMail($user));
                return response()->json(['success'=>$user], $this->successStatus);
            }else{
                return response()->json(['error'=>"oops we cannot create this account, try again"]);
            }

        }catch (\Exception $e){
            return $e->getMessage();
        }
    }


    function confirmAccount($token){
        $user = User::where('confirmationToken', $token)->first();
        if(Auth::loginUsingId($user->id)){
            $user->confirmationToken = "";
            $user->confirmation_status = 1;
            $user->update();
            Session::put('successfulMessage', 'Your Account has been confirmed');
            return redirect()->intended('dashboard');

        }else{
            Session::put('errorMessage', 'Your Account cannot be confirmed at the moment.');
            return redirect(route('viewLogin'));
        }
    }

    function registerClient(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'phoneNumber' => 'required',
                'company_name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required',
                'Confirm_Password' => 'required|same:password',
            ]);

            if ($validator->fails()) {
                return response()->json(['error'=> $validator->errors()]);
            }

            /*$input= $request->all();
            $input['firstname'] = $request->clientname;
            $input['email'] = $request->clientemail;
            $input['password'] = Hash::make($request->password);
            $input['user_role'] = 'client';
            $input['confirmationToken'] = Str::random(50);*/
            $user = User::create([
                'firstname' => $request->company_name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'user_role' => 'client',
                'confirmationToken' => Str::random(50),
            ]);
            $client =  clientInfo::create(['user_id'=>$user->id, 'phone_number'=>$request->phoneNumber]);
            //$success['token'] =  $user->createToken('TalentHub')->accessToken;
            $admin = User::where('user_role', "admin")->first();
            if($user){
                //Mail::to($user->email)->send(new confirmationMail($user));
                Mail::to($admin->email)->send(new newClientAlert($admin));
                return response()->json(['success'=>$user], $this->successStatus);
            }else{
                return response()->json(['error'=>"oops we cannot create this account, try again"]);
            }
        }catch (\Exception $e){
            return response()->json(['error'=>"oops we cannot create this account, try again".$e->getMessage()]);
        }

    }




    function addUser(Request $request){
        try{
            $user = User::create([
                'firstname'=>$request->fname,
                'lastname' => $request->lname,
                'email' => $request->email,
                'password' => bcrypt('12345678'),
                'user_role' => $request->role,
            ]);
            if($user){
                Mail::to($user->email)->cc(Auth::user()->email)->send(new notifyNewAdminUser($user));
            }
            return response()->json(['success'=>$user], 200);
        }catch (\Exception $e){
            return response()->json(['error'=>'Sth went wrong, try again.', 'status'=>500]);
        }
    }


    ///password recovery
    function  renderPasswordRecoverPage(){
        return view('recoverpassword');
    }

    function createNewPassword($id){
        try{
            return view('enterNewPassword', ['token'=>$id]);
        }catch (\Exception $e){
            abort('505');
        }

    }

    function recoverPass(Request $request){
        try{
            $user = User::where('email', $request->email)->first();
            if($user){
                $str = Str::random(50);
                $user->recoveryToken = $str;
                $user->save();
                $user['newString'] = $str;

                Mail::to($user->email)->send(new recoverPassword($user));
                Session::put('successfulMessage', 'A mail containing the next instructions has been sent you email please check your inbox');
                return redirect()->back();
            }else{
                Session::put('errorMessage', 'Your email does not exist in our record');
                return redirect()->back();
            }
        }catch (\Exception $e){
            Session::put('errorMessage', 'Snap! Sth went wrong try again');
            return redirect()->back();
        }

    }

    function changePassword(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'password' => 'required',
                'confirmNewPassword' => 'required|same:password',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors());
            }
                $user = User::where('recoveryToken', $request->token)->first();
                if($user){
                    $user->password = bcrypt($request->password);
                    $user->save();
                    Session::put('successfulMessage', 'You successfully changed your password you can now login');
                    return view('passwordChangeSuccessful');
                }else{
                    Session::put('errorMessage', 'We cannot validate you');
                    return redirect()->back();
                }
        }catch (\Exception $e){
            $error = "There was a an error in connection, Try again";
            \Session::put('errorMessage', $error);
            return redirect()->back();
        }
    }
}
