<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class loginController extends Controller
{
    Public $successStatus = 200;

    function logUserIn(){
        try{
            if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
                $user = Auth::user();
                if($user->user_role == "candidate" && $user->confirmation_status == 0){
                    return response()->json(['verify' => "verify your account", 'token'=>$user->confirmationToken]);
                }elseif($user->user_role == "candidate" && $user->confirmation_status == 1){
                    return response()->json(['success' => "logged in", "user"=>"candidate"], $this->successStatus);
                }elseif($user->user_role == "admin") {
                    if ($user->suspend == 1) {
                        return response()->json(['suspended' => "suspended", "user" => "admin"], $this->successStatus);
                    } else {
                        return response()->json(['success' => "logged in", "user" => "admin"], $this->successStatus);
                    }
                }
                //$success['token'] =  $user->createToken('TalentHub')-> accessToken;
            }else{
//                return response()->json(['error'=>'Unauthorised'], 401);
                  abort(401);
            }
        }catch(\Exception $e){
            return $e->getMessage();
        }
    }

    function  logout(){
        Auth::logout();
        return redirect(route('viewLogin'));
    }
}
