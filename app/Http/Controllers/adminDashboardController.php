<?php

namespace App\Http\Controllers;

use App\jobOpening;
use App\Mail\generalInfo;
use App\Mail\interviewInviteMail;
use App\Mail\rejectionMail;
use App\Models\approvedCandidate;
use App\Models\candidate;
use App\Models\candidate_educationInfo;
use App\Models\clientInfo;
use App\Models\employedCandidate;
use App\Models\inviteMail;
use App\Models\objectiveQuestion;
use App\Models\questionCategory;
use App\Models\recommendedCandidate;
use App\Models\region;
use App\Models\result;
use App\Models\state;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use \Maatwebsite\Excel\Facades\Excel;
use Mockery\Exception;

class adminDashboardController extends Controller
{
    function index(){
        try{
            if(Auth::check()){
                $jobs = jobOpening::count();
                $latestjobs = jobOpening::latest()->limit(3)->get();
                $applicants = [];
                foreach ($latestjobs as $latestjob){
                    $number = candidate::where('job_id', $latestjob->id)->count();
                    array_push($applicants, $number);
                }
                return view ('admin.index', ['latestjobs'=>$latestjobs, 'applicants'=>$applicants, 'jobs'=>$jobs]);

            }else{
                return redirect(route('logout'));
            }
        }catch (\Exception $e){
            return view('login');
        }
    }


    function addQuestion(){
        try{
            if(Auth::check()){
                $categories = questionCategory::get();
                return view('admin.addQuestion', ['categories'=>$categories]);
            }else{
                return redirect(route('logout'));
            }
        }catch(\Exception $e){
            abort(500);
        }
    }

    function addObjQuestion(Request $request){
        if(Auth::check()){
            if(Auth::user()->user_role == "admin" ||  Auth::user()->user_role == "examiner"){
                try{
                    $validator = Validator::make($request->all(), [
                        'category' => 'required',
                        'question' => 'required',
                        'options' => 'required',
                        'answer' => 'required'
                    ]);
                    if($validator->fails()){
//                return redirect()->back()->with(['errors'=>$validator->errors()]);
                        return "Validation error";
                    }

                    $question = objectiveQuestion::create([
                        'question' => $request->question,
                        'options' => $request->options,
                        'answer' => $request->answer,
                        'category_id' => $request->category,
                        'added_by' => Auth::user()->id
                    ]);

                    if($request->hasFile('image')){
                        $filename = $request->file('image')->getClientOriginalName();
                        $fileExtesion = $request->file('image')->getClientOriginalExtension();
                        $name = str_replace('.' . $fileExtesion, "", $filename);
                        if ($fileExtesion == "png"  || $fileExtesion == "gif" || $fileExtesion == "jpg" || $fileExtesion == "jpeg"){
                            if (\File::exists(public_path('questionImageStorage'. $filename))) {
                                //\File::delete(public_path('questionImageStorage' . $filename));
                                $er = rand(0, 1000);
                                $filename = $name . $er . '.' . $fileExtesion;
                                $request->file('image')->move('questionImageStorage', $filename);
                            }else {
                                $request->file('image')->move('questionImageStorage', $filename);
                            }
                            $question->question_image = $filename;
                            $question->update();
                        }
                    }
                    return "success";
                }catch (\Exception $exception){
                    return $exception->getMessage();
                }
            }else{
                return "not authorized";
            }
        }else{
            return "logout";
        }
    }


    function  uploadExcel(Request $request){
        if (Auth::check()){
            if (Auth::user()->user_role == "admin" || Auth::user()->user_role == "examiner") {
                $validator = Validator::make($request->all(), [
                    'category' => 'required',
                ]);
                if($validator->fails()){
                    return redirect()->back()->with(['errors'=>$validator->errors()]);
                }
                if(!$request->hasFile('sheet')){
                    $errorMessage = 'Spreadsheet is needed for this operation.';
                    \Session::put('errorMessage', $errorMessage);
                    return redirect()->back();
                }else{
                    $path  = $request->file('sheet');
                    $extension = $path->getClientOriginalExtension();
                    $name = $path->getClientOriginalName();
                    if($extension == "xlsx" || $extension == "xls" || $extension == "csv"){
                        $filedata = Excel::load($path, function ($reader) {
                        })->get();
                        if(!empty($filedata) && $filedata->count()){
                            foreach ($filedata as $filedatum){
                                objectiveQuestion::insert([
                                    'question'=> $filedatum->question,
                                    'options'=> $filedatum->options,
                                    'answer'=>$filedatum->answer,
                                    'category_id' => $request->category,
                                    'added_by'=> Auth::user()->id
                                ]);
                            }
                            $successfulMessage = 'Questions successfully uploaded.';
                            \Session::put('successfulMessage', $successfulMessage);
                            return redirect()->back();
                        }else{
                            $errorMessage = 'Your file is empty.';
                            \Session::put('errorMessage', $errorMessage);
                            return redirect()->back();
                        }
                    }else{
                        $errorMessage = 'Format not correct! File should be in:* .xls or .xlsx or .csv';
                        \Session::put('errorMessage', $errorMessage);
                        return redirect()->back();
                    }
                }

            } else {
                \Session::put('errorMessage', 'You are not authorized.');
                return redirect()->back();
            }
        }else{
            \Session::put('errorMessage', 'Timed Out! Please Login again.');
            return redirect(route('logout'));
        }
    }


    function  subnewcategory(Request $request){
        $validator = Validator::make($request->all(), [
            'catName' => 'required',
            'duration'=> 'required'
        ]);

        if($validator->fails()){
            return $validator->errors();
        }
        $category = questionCategory::create([
            'category' => $request->catName,
            'category_status'=>$request->catStatus,
            'duration'=>$request->duration
        ]);

        if($category){
            return response()->json(['success' => 1, 'id' => $category->id, 'category' => $category->category]);
        }else{
            return response()->json(['error' => 1]);
        }
    }

    function allCategories(){
        if (Auth::check()){
            $categories = questionCategory::all();
            if(Auth::user()->user_role == "admin" || Auth::user()->user_role == "examiner") {
                return view('admin.categories', ['categories'=>$categories]);
            }elseif(Auth::user()->user_role == "candidate"){
                $newCategory = [];
                $temPArr = [];
                foreach ($categories as $category){
                    if($category->category_status == 1){
                        if($category->taken_by != null){
                            $temPArr = $category->taken_by;
                            if(in_array(Auth::id(), $temPArr)){

                            }else{
                                //return $category;
                                array_push($newCategory, $category);
                            }
                        }
                        //return $temPArr;
                        //return $newCategory;
                    }else{
                        $op = [];
                        if($category->assigned_to != null){
                            $temPArr = $category->assigned_to;
                            if(in_array(Auth::id(), $temPArr)){
                                if($category->taken_by != null){
                                    $op = $category->taken_by;
                                    if(!in_array(Auth::id(), $op)){
                                        array_push($newCategory, $category);
                                    }
                                }
                            }
                        }
                    }
                }
                //return $newCategory;
                $categories = $newCategory;

                //if(count($newCategory) > 0){
                 //   return view('categories', ['categories'=>$categories]);
                //}else{
                    return view('categories', ['categories'=>$categories]);
               // }

            } else {
                \Session::put('errorMessage', 'Not Authorized.');
                return redirect()->back();
            }
        }else{
            \Session::put('errorMessage', 'Timed Out! Please Login again.');
            return redirect(route('logout'));
        }

    }

    function editCategory(Request $request){
        try{
            //return $request;
            $validator = Validator::make($request->all(), [
                'categoryName' => 'required',
                'duration'=> 'required'
            ]);

            if($validator->fails()){
                return $validator->errors();
            }
            $category = questionCategory::where('id', $request->id)->first();
            $category->duration = $request->duration;
            $category->category_status = $request->categoryStatus;
            $category->category = $request->categoryName;
            $category->save();
            return response()->json(['success' => 1, 'id' => $category->id, 'category' => $category->category]);
        }catch (\Exception $e){
            return response()->json(['error' => 1]);
        }
    }

    function getQuestions(Request $request, $id){
        if (Auth::check()){
            if(Auth::user()->user_role == "admin" || Auth::user()->user_role == "examiner") {
                $questions = objectiveQuestion::where('category_id', $id)->get();
                return view('admin.questions', ['questions'=>$questions]);
            }else{
                \Session::put('errorMessage', 'Not Authorized.');
                return redirect()->back();
            }
        }else{
            \Session::put('errorMessage', 'Timed Out! Please Login again.');
            return redirect(route('logout'));
        }
    }

    function editQuestion(Request $request){
        if(Auth::check()){
            if(Auth::user()->user_role == "admin" || Auth::user()->user_role == "examiner"){
                try{
                    //return $request->all();
                    $validator = Validator::make($request->all(), [
                        'theId' =>'required',
                        'quest' =>'required',
                        'opt' =>'required',
                    ]);

                    if($validator->fails()){
                        return response()->json(['errors'=>$validator->errors()]);
                    }else{
                        $question = objectiveQuestion::where('id', $request->theId)->first();
                        $question->question = $request->quest;
                        $question->options = $request->opt;
                        $question->answer = $request->ans;
                        $question->added_by = Auth::user()->id;
                        if($request->hasFile('qimage')){
                            $path = $request->file('qimage');
                            $name = $path->getClientOriginalName();
                            $ext = $path->getClientOriginalExtension();
                            $filename = str_replace('.' . $ext, "", $name);
                            if($ext == "jpg" || $ext == "jpeg" || $ext == "png" || $ext == "gif"){
                                if (\File::exists(public_path('questionImageStorage' . $name))) {
                                    //\File::delete(public_path('questionImageStorage' . $name));
                                    $er = rand(0, 1000);
                                    $name = $filename . $er . '.' . $ext;
                                    $request->file('qimage')->move('questionImageStorage', $name);
                                } else {
                                    $request->file('qimage')->move('questionImageStorage', $name);
                                }
                            }else{
                                return "format not supported";
                            }
                            $question->question_image = $name;
                        }
                        $question->update();

                        return response()->json(['questions'=>[$question], 'status'=>1], 200);
                    }
                }catch (\Exception $e){
                    return $e->getMessage();
                }
            }else{
                \Session::put('errorMessage', 'You are not authorized');
                return "Not authorized";
            }
        }else{
            \Session::put('errorMessage', 'You are not authorized');
            return "logout";
        }


    }

    function deleteQuestion(Request $request){
        try{
            objectiveQuestion::where('id', $request->id)->delete();
            return 'success';
        }catch(\Exception $e){
            return $e->getMessage();
        }

    }

    function fetchCandidates(){

    }

    function returnCandidatesView(){
        $regions = region::all();
        return view('admin.filterStudents', ['regions'=>$regions]);
    }

    function  acceptedCandidates(){
        $clients = clientInfo::all();
        return view('admin.accepted', ['clients'=>$clients]);
    }

    function testedCandidates(){
        $candidates = result::distinct()->select('user_id')->get();
        for($i=0; $i<count($candidates); $i++){
            $tests = [];
            $res = result::where('user_id', $candidates[$i]->user_id)->get();
            $combinedString = "";
            foreach ($res as $re){
                $category = $re->category->category;
                $result= $re->score;
                $combinedString = $category." : ".$result;
                array_push($tests, $combinedString);
            }

            $candidates[$i]['result'] = implode(',', $tests);
        }
        //return $tests;
        $categories = questionCategory::where('category_status', 0)->get();
        $clients = clientInfo::all();
        $regions = region::all();
//        return $candidates;
        return view('admin.tested', ['categories'=>$categories, 'candidates'=>$candidates, 'clients'=>$clients, 'regions'=>$regions]);
    }

    function recommendedCandidates(){
        //recommendedCandidate::
        $clients = clientInfo::all();
        return view('admin.recommended', ['clients'=>$clients]);
    }

    function appliedCandidates(){
        //recommendedCandidate::
        $clients = clientInfo::all();
        return view('admin.appliedCandidates', ['clients'=>$clients]);
    }

    function approvedCandidates(){
        $clients = clientInfo::all();
        return view('admin.approved', ['clients'=>$clients]);
    }

    function jobOpenings(){
        $openings = jobOpening::latest()->get();
        return view('admin.openings', ['openings'=>$openings]);
    }

    function submitJobOpening(Request $request){
        try{
//            return $request->all();
            $jobid = Str::random(6);
            $jobopening = jobOpening::create([
                'job_id'=> $jobid,
                'title' => $request->jobTitle,
                'deadline' => $request->deadline,
                'summary' => $request->summary,
                'description' => $request->jobDescription,
            ]);
            if($jobopening){
                return response()->json(['success'=>'Job Successfully added']);
            }else{
                return response()->json(['error'=>'Unable to create job, Try again']);
            }
        }catch(\Exception $e){
            return response()->json(['error'=>'Unable to create job, Try again']);
        }
    }

    function suspendClient(Request $request){
        try{
            $user = User::where('id', $request->id)->first();
            if($user->client->active_status == 0){
                $user->client->active_status = 1;
            }else{
                $user->client->active_status = 0;
            }
            $user->client->update();
            return response()->json(['success'=>'suspended'], 200);
        }catch (\Exception $e){
            return response()->json(['error'=>$e->getMessage()], 500);
        }
    }

    function approveClient(Request $request){
        try{
            $user = User::where('id', $request->id)->first();
            if($user->client->approval_status == 0){
                $user->client->approval_status = 1;
            }else{
                $user->client->approval_status = 0;
            }
            $user->client->update();
            return response()->json(['success'=>'approved'], 200);
        }catch (\Exception $e){
            return response()->json(['error'=>"Sth went wrong"], 503);
        }
    }


    function findDiscipline(Request $request){
        try
        {
            //$users = User::where('email', 'LIKE', '%' . $request->searchKey . '%')->get(['id', 'email']);
            $items = candidate_educationInfo::select('fieldOfStudy')->where('fieldOfStudy', 'LIKE', '%' . $request->searchKey . '%')->groupBy('fieldOfStudy')->get(['id', 'fieldOfStudy']);
            $searchResults = $items;
            return ['searchResults'=>$searchResults];
        }
        catch (\Exception $e)
        {
            return $e->getMessage();
        }
    }

    function viewCandidateProfile($id){
        if(Auth::check()){
            try{
                $candidate = User::where('id', $id)->first();
                if(Auth::user()->user_role == "admin" || Auth::user()->user_role == "examiner"){
                    return view('admin.candidateProfileView', ['candidate'=>$candidate]);
                }elseif(Auth::user()->user_role == "client"){
                    $candidate->profile_views = $candidate->profile_views + 1;
                    $candidate->save();
                    return view('clients.viewCandidateProfile', ['candidate'=>$candidate]);
                }

            }catch(\Exception $e){
                Session::put('errorMessage', 'There was problem fetching the data');
                return redirect()->back();
                return response()->json(['error'=>$e->getMessage()], 500);
            }
        }else{
           return redirect(route('viewLogin')) ;
        }

    }

    function assignTest(Request $request){
        try{
            $holdArray = [];
            $category = questionCategory::where('id', $request->course)->first();
            if($category->assigned_to == null){
                $category->assigned_to = $request->students;
                //return $request->students;
                $category->save();
                return response()->json(['success'=>'You successfully assigned the test to the selected candidates(s)']);
            }else{
                $holdArray = $category->assigned_to;
                foreach ($request->students as $student){
                    if(!in_array($student, $holdArray)) {
                        array_push($holdArray, $student);
                    }
                }
                //return $holdArray;
                $category->assigned_to = $holdArray;
                $category->save();
                return response()->json(['success'=>'You successfully assigned the test to the selected candidates(s)']);
            }
        }catch(\Exception $e){
            return response()->json(['error'=>$e->getMessage()], 500);
        }
    }

    function  getOpenings(Request $request){
        try{
            $jobOpenings = jobOpening::where('client_id', $request->id)->get();
            return response()->json(['jobOpenings'=>$jobOpenings]);
        }catch(\Exception $e){
            return response()->json(['error'=>$e->getMessage()], 500);
        }
    }

    function recommendToAdmin(Request $request){
        try{
            //return count($request->students);
            foreach ($request->students as $student){
                $res = recommendedCandidate::where('user_id', $request->student)->where('jobOpening_id', $request->job)->first();
                if($res == null || $res = []){
                    $recommend = recommendedCandidate::create([
                        'user_id' => $student,
                        'jobOpening_id' => $request->job
                    ]);
                }

            }
//            if($recommend){
                return response()->json(['success'=> 'Candidates have been recommended for approval']);
//            }
        }catch(\Exception $e){
            return response()->json(['error'=>$e->getMessage()], 500);
        }
    }

    function approveForClient(Request $request){
        try{
            //return count($request->students);
            foreach ($request->students as $student){
                $res = approvedCandidate::where('user_id', $request->student)->where('jobOpening_id', $request->job)->first();
                if($res == null || $res = []){
                    $recommend = approvedCandidate::create([
                        'user_id' => $student,
                        'jobOpening_id' => $request->job
                    ]);
                }
            }

//            if($recommend){
            return response()->json(['success'=> 'Candidates have been approved for employment. The client can now access their information.']);
//            }
        }catch(\Exception $e){
            return response()->json(['error'=>$e->getMessage()], 500);
        }
    }

    function acceptForJob(Request $request){
        try{
            //return count($request->students);
            foreach ($request->students as $student){
                $res = employedCandidate::where('candidate_id', $request->student)->where('jobOpening_id', $request->job)->first();
                if($res == null || $res = []){
                    $accepted = employedCandidate::create([
                        'candidate_id' => $student,
                        'jobOpening_id' => $request->job
                    ]);
                }
            }

//            if($recommend){
            return response()->json(['success'=> 'Selected Candidates have been marked accepted for the client ']);
//            .$accepted->jobOpening->client->user->firstname
//            }
        }catch(\Exception $e){
            return response()->json(['error'=>$e->getMessage()], 500);
        }
    }

    function sendMail(Request $request){
        try{
            //return count($request->students);
            foreach ($request->students as $student){
                $res = User::where('id', $student)->first();
                if($res != null || $res != []){
                    //return $res->email;
                    Mail::to($res->email)->send(new generalInfo($res->firstname, $request->message, $request->subject));
                }
            }

//            if($recommend){
            return response()->json(['success'=> 'Message sent successfully.']);
//            .$accepted->jobOpening->client->user->firstname
//            }
        }catch(\Exception $e){
            return response()->json(['error'=>$e->getMessage()], 513);
        }
    }

    function closeApplication(Request $request){
        try{
            $job = jobOpening::find($request->id);
            $job->status = 1;
            $job->update();
            return response()->json(['success'=>'Application Closed']);
        }catch (\Exception $error){
            return response()->json(['error'=>'error']);
        }
    }

    function  deleteOpening(Request $request){
        try{
            $job = jobOpening::find($request->id);
            $job->delete();
            return response()->json(['success'=>'Application Deleted']);
        }catch (\Exception $error){
            return response()->json(['error'=>'error']);
        }
    }

    function sendInviteMail(Request $request){
        try{
            //return $request;
            Mail::to($request->recipient)->send(new interviewInviteMail($request->message, $request->title));
            if (Mail::failures()) {
                return response()->json(['error'=>'Sorry! Please try again latter']);
            }
            $candidate = candidate::where('job_id', $request->jobId)->where('email', $request->recipient)->first();
            $candidate->invite_status = 1;
            $candidate->update();
            inviteMail::create([
                'recipient' => $request->recipient,
                'subject' => $request->title,
                'message' => $request->message,
                'sender' => Auth::user()->email,
                'job_id' => $request->jobId
            ]);
            return response()->json(['success'=>'You successfully sent the mail to the candidate.']);
        }catch (\Exception $e){
            //return $e->getMessage();
            return response()->json(['error'=>'There was a problem sending the message. Please try again.']);
        }
    }

    function loadinviteMails(){
        $mails = inviteMail::latest()->get();
        return view('admin.inviteMailsSent', ['mails'=>$mails]);
    }

    function sendRejectionMail(){
        try{
            $check = candidate::where('job_id', \request('jobId'))->where('invite_status', 1)->first();
            if($check){
                $candidates = candidate::where('job_id', \request('jobId'))->where('invite_status', 0)->get();
                //return $candidates;
                foreach ($candidates as $candidate){
                    Mail::to($candidate->email)->queue(new rejectionMail($candidate));
                    /*if (Mail::failures()) {
                        return response()->json(['error'=>'Sorry! Please try again later']);
                    }*/
                    $candidate->rejectMail_sent = 1;
                    $candidate->update();
                }
                return response()->json(['success'=>'You successfully sent the mail to the candidates affected.']);
            }else{
                return response()->json(['error'=>'You can only reject if you have sent invitation mails to at least one person.']);
            }
        }catch (\Exception $e){
            return response()->json(['error'=>'Sorry! Please try again later']);
        }
    }


}
