<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jobOpening extends Model
{
    protected $fillable = ['title', 'job_id', 'deadline', 'status', 'summary', 'description'];

    public function applicant(){
        return $this->hasMany('App\Models\candidate', 'job_id', 'id');
    }
}
