<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from themepixels.me/slim1.1/template/page-signup2.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 02 Jun 2019 13:04:28 GMT -->
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{csrf_token()}}">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Slim">
    <meta name="twitter:description" content="XL Outsourcing Limited | Talent Hub">
    <meta name="twitter:image" content="../../slim/img/slim-social.html">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/slim">
    <meta property="og:title" content="Slim">
    <meta property="og:description" content="XL Outsourcing Limited | Talent Hub.">

    <meta property="og:image" content="../../slim/img/slim-social.html">
    <meta property="og:image:secure_url" content="../../slim/img/slim-social.html">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Telent Hub, XL Outsourcing Limited">
    <meta name="author" content="ThemePixels">

    <title>XL Outsourcing Limited -XLTalentHub</title>

    <!-- Vendor css -->
    <link href="../lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="../lib/Ionicons/css/ionicons.css" rel="stylesheet">

    <!-- Slim CSS -->
    <link rel="stylesheet" href="../css/slim.css">

</head>
<body>

<div class="d-md-flex flex-row-reverse">
    <div class="signin-right">

        <div class="signin-box signup">
            <h3 class="signin-title-primary">Get Started!</h3>
            <h5 class="signin-title-secondary lh-4">It's free to signup and only takes a minute.</h5>
            <form id="theform">
                <div class="row row-xs mg-b-10">
                    <div class="col-sm"><input type="text" class="form-control" name="firstname" placeholder="Firstname"></div>
                    <div class="col-sm mg-t-10 mg-sm-t-0"><input type="text" name="lastname" class="form-control" placeholder="Lastname"></div>
                </div><!-- row -->

                <div class="row row-xs mg-b-10">
                    <div class="col-sm"><input type="email" class="form-control" placeholder="Email" name="email"></div>
                    <div class="col-sm mg-t-10 mg-sm-t-0"><input type="password" name="password" class="form-control" placeholder="Password"><i class="fa fa-eye"></i></div>
                </div><!-- row -->
                <div class="row row-xs mg-b-10">
                    <div class="col-sm"><input type="password" name="Confirm_Password" class="form-control" placeholder="Confirm Password"></div>
                </div>

                <button class="btn btn-primary btn-block btn-signin" id="signUp">Sign Up</button>

            </form>


            {{--<div class="signup-separator"><span>or signup using</span></div>--}}

            {{--<button class="btn btn-facebook btn-block">Sign Up Using Facebook</button>
            <button class="btn btn-twitter btn-block">Sign Up Using Twitter</button>--}}

            <p class="mg-t-40 mg-b-0">Already have an account? <a href="{{route('viewLogin')}}">Sign In</a> |  <a href="{{url('/')}}">Home</a></p>
            <p class="mg-t-30"><a href="{{route('jobBoard')}}">View Jobs</a></p>
        </div><!-- signin-box -->

    </div><!-- signin-right -->
    <div class="signin-left">
        <div class="signin-box">
            <h2 class="slim-logo"><a href="">XLTalentHub<span>.</span></a></h2>

            <p>We are excited to launch our new  platform XLTalentHub. XLTalentHub help job seekers to set up their profiles online and get profiled by interested companies when their skills are needed.</p>

            <p>Try talent hub today and get your new dream job(s) or hire the best candidates/Talents for roles your organization is looking to fill.</p>

            <p><a href="#" data-target="#clientReg" data-toggle="modal" class="btn btn-outline-secondary pd-x-25">Hire a Talent</a></p>

            <p class="tx-12">&copy; Copyright XL Outsourcing Limited {{date('Y')}}. All Rights Reserved.</p>
        </div>
    </div><!-- signin-left -->
    @include('includes.alerts')
    <div id="clientReg" class="modal fade" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content bd-0 bg-transparent rounded overflow-hidden">
                <div class="modal-body pd-0">
                    <div class="row no-gutters">
                        <div class="col-lg-6 bg-primary">
                            <div class="pd-40">
                                <h1 class="tx-white mg-b-20">XLTalentHub</h1>
                                <p class="tx-white op-7 mg-b-30">
                                    Welcome! Begin your first step to hiring the best minds with us. Please register your interest on the form to your right.
                                    we have over the years done this and have achieved nothing but the best for our clients. Join the train of successful employers today!
                                </p>
                                <p class="tx-white">
                                    {{--<span class="tx-uppercase tx-medium d-block mg-b-15">Our Address:</span>--}}
                                    {{--<span class="op-7">Plott Samuel Manuwa Street, Victoria Island, Lagos</span>--}}
                                </p>
                            </div>
                        </div><!-- col-6 -->
                        <div class="col-lg-6 bg-white">
                            <div class="pd-y-30 pd-xl-x-30">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <div class="pd-x-30 pd-y-10">
                                    <h3 class="tx-gray-800 tx-normal mg-b-5">Client Registration</h3>
                                    <p>Create and account to start hiring</p>
                                    <br>
                                    <form id="clienform">
                                        <div class="form-group">
                                            <label>Contact Email</label>
                                            <input type="email" name="clientemail" class="form-control pd-y-12" placeholder="Email">
                                        </div><!-- form-group -->
                                        <div class="form-group">
                                            <label>Company Name</label>
                                            <input type="text" name="clientname" class="form-control pd-y-12" placeholder="Company Name">
                                        </div>
                                        <div class="form-group">
                                            <label>Contact Phone Number</label>
                                            <input type="text" name="phoneNumber" class="form-control pd-y-12" placeholder="Enter your phone Number">
                                        </div><!-- form-group -->
                                        <div class="form-group">
                                            <label>Create Password</label>
                                            <input type="password" name="passwordd" class="form-control pd-y-12" placeholder="Create a password">
                                        </div>
                                        <div class="form-group mg-b-20">
                                            <label>Confirm Password</label>
                                            <input type="password" name="cpasswordd" class="form-control pd-y-12" placeholder="Confirm Password">
                                        </div>
                                        <button class="btn btn-primary pd-y-12 btn-block" id="registerClient">Register</button>
                                    </form>
                                    {{--<div class="mg-t-30 mg-b-20">Don't have an account yet? <a href="#">Sign Up</a></div>--}}
                                </div>
                            </div><!-- pd-20 -->
                        </div><!-- col-6 -->
                    </div><!-- row -->
                </div><!-- modal-body -->
            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div>
</div><!-- d-flex -->

<script src="{{asset('lib/jquery/js/jquery.js')}}"></script>
<script src="{{asset('lib/popper.js/js/popper.js')}}"></script>
<script src="{{asset('lib/bootstrap/js/bootstrap.js')}}"></script>

<script src="{{asset('js/slim.js')}}"></script>

<script>
    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')}
    });

    $(document).on('click', '#signUp', function (e) {
        e.preventDefault();
        let ths =$(this);
        let firstname = $('input[name=firstname]').val();
        let lastname = $('input[name=lastname]').val();
        let email = $('input[name=email]').val();
        let password = $('input[name=password]').val();
        let Confirm_Password = $('input[name=Confirm_Password]').val();
        $(this).attr('disabled', true);
        $(this).text('...processing');
        $.post("{{route('signup')}}", {firstname:firstname, lastname:lastname, email:email, password:password, Confirm_Password:Confirm_Password}, function(data) {
            ths.attr('disabled', false);
            ths.text('Sign Up');
            console.log(data);
            if(data.success){
                $('#success').modal('toggle');
                let msg = "<p><b>A confirmation email has been sent to the email address you provided, please verify your email address.</b></p>";
                $('#successmsg').html(msg);
                $('#theform')[0].reset();
            }
            else if(data.error){
                $('#error').modal('toggle');
                let msg =  "<p>Ensure the following:</p><li>Password and Confirm password must be the same</li>";
                msg += "<li>Check if your email is correctly given</li>";
                $('#message').html(msg);
            }
            else if(data == "The given data was invalid."){
                $('#error').modal('toggle');
                let msg =  "<p>Ensure the following:</p><li>Password and Confirm password must be the same</li>";
                msg += "<li>Check if your email is correctly given or has not been already registered.</li>";
                $('#message').html(msg);
            }else if(data == "Connection to xlafricagroup.com:465 Timed Out"){
                $('#success').modal('toggle');
                let msg = "<p><b>Login to continue <a href='{{route('viewLogin')}}'>Login</a></b></p>";
                $('#successmsg').html(msg);
                $('#theform')[0].reset();
            }else{
                $('#success').modal('toggle');
                let msg = "<p><b>Login to continue <a href='{{route('viewLogin')}}'>Login</a></b></p>";
                $('#successmsg').html(msg);
                $('#theform')[0].reset();
            }
        });
    });

    $(document).on('click', '#registerClient', function (e) {
        e.preventDefault();
        let ths =$(this);
        let email = $('input[name=clientemail]').val();
        let company_name = $('#clienform').find('input[name=clientname]').val();
        let phoneNumber = $('input[name=phoneNumber]').val();
        let password = $('input[name=passwordd]').val();
        let Confirm_Password = $('input[name=cpasswordd]').val();
        $(this).attr('disabled', true);
        $(this).text('...processing');
        $.post('{{route("registerClient")}}', {company_name:company_name,  email:email, password:password, Confirm_Password:Confirm_Password, phoneNumber:phoneNumber}, function(data) {
            ths.attr('disabled', false);
            ths.text('Register');
            console.log(data);
            if(data.success){
                $('#success').modal('toggle');
                let msg = "<p><b>A confirmation email has been sent to the email address you provided, please verify your email address.</b></p>";
                //msg += "<p><a class='btn btn-warning' href='/sign-in'></a></p>";
                $('#successmsg').html(msg);
                $('#clienform')[0].reset();
            }else if(data.error.email){
                $('#error').modal('toggle');
                let msg =  "<li>"+data.error.email+"</li>";
                // msg += "<li>Check if your email is correctly given</li>";
                $('#message').html(msg);
            }
            else if(data.error){
                $('#error').modal('toggle');
                let msg =  "<p>Ensure the following:</p><li>Password and Confirm password must be the same</li>";
                msg += "<li>Check if your email is correctly given</li>";
                $('#message').html(msg);
            }
            else if(data == "The given data was invalid."){
                $('#error').modal('toggle');
                let msg =  "<p>Ensure the following:</p><li>Password and Confirm password must be the same</li>";
                msg += "<li>Check if your email is correctly given</li>";
                $('#message').html(msg);
            }else{
                $('#error').modal('toggle');
                let msg =  "<p>Ensure the following:</p><li>Password and Confirm password must be the same</li>";
                msg += "<li>Check if your email is correctly given</li>";
                $('#message').html(msg);
            }
        });
    });

    $('input[name=Confirm_Password]').on('keypress', function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            let ths =$(this);
            let firstname = $('input[name=firstname]').val();
            let lastname = $('input[name=lastname]').val();
            let email = $('input[name=email]').val();
            let password = $('input[name=password]').val();
            let Confirm_Password = $('input[name=Confirm_Password]').val();
            $(this).attr('disabled', true);
            $(this).text('...processing');
            $.post("{{route('signup')}}", {firstname:firstname, lastname:lastname, email:email, password:password, Confirm_Password:Confirm_Password}, function(data) {
                ths.attr('disabled', false);
                ths.text('Sign Up');
                console.log(data);
                if(data.success){
                    $('#success').modal('toggle');
                    let msg = "<p><b>A confirmation email has been sent to the email address you provided, please verify your email address.</b></p>";
                    $('#successmsg').html(msg);
                    $('#theform')[0].reset();
                }
                else if(data.error){
                    $('#error').modal('toggle');
                    let msg =  "<p>Ensure the following:</p><li>Password and Confirm password must be the same</li>";
                    msg += "<li>Check if your email is correctly given</li>";
                    $('#message').html(msg);
                }
                else if(data == "The given data was invalid."){
                    $('#error').modal('toggle');
                    let msg =  "<p>Ensure the following:</p><li>Password and Confirm password must be the same</li>";
                    msg += "<li>Check if your email is correctly given or has not been already registered.</li>";
                    $('#message').html(msg);
                }else if(data == "Connection to xlafricagroup.com:465 Timed Out"){
                    $('#success').modal('toggle');
                    let msg = "<p><b>Login to continue <a href='{{route('viewLogin')}}'>Login</a></b></p>";
                    $('#successmsg').html(msg);
                    $('#theform')[0].reset();
                }else{
                    $('#success').modal('toggle');
                    let msg = "<p><b>Login to continue <a href='{{route('viewLogin')}}'>Login</a></b></p>";
                    $('#successmsg').html(msg);
                    $('#theform')[0].reset();
                }
            });
        }
    });
</script>

</body>

<!-- Mirrored from themepixels.me/slim1.1/template/page-signup2.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 02 Jun 2019 13:04:28 GMT -->
</html>
