@extends('Layouts.examinerDashboard')
@section('content')
    <div class="slim-mainpanel">
        {{--<div class="box box-block bg-white col-md-6">
            <div class="box-block b-b">

            </div>
        </div>--}}
        <div class="container pd-t-50">
            <p><h4> Settings -<small>Profile Settings</small></h4></p>
            @include('includes.messages')
            <div id="msg"></div>
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="box bg-white col-md-12">
                        <p><h4> <small>Personal</small></h4></p>
                        <form action="{{route('updateProfile')}}" method="post">
                            {{csrf_field()}}
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Company Name</label>
                                    <input type="text" class="form-control" name="username" value="{{Auth::user()->firstname}}">
                                </div>

                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" class="form-control" name="email" value="{{Auth::user()->email}}">
                                </div>

                                {{--<div class="form-group">
                                    <label>Company Name</label>
                                    <input type="text" class="form-control">
                                </div>--}}
                                <div class="form-group">
                                    <div><button class="btn btn-success pull-right mt-2 mb-2" type="submit">Update</button></div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>


                <div class="col-md-6">
                    <div class="box bg-white col-md-12">
                        <p><h4> <small>Change Password</small></h4></p>
                        <div class="card-body">
                            <form action="{{route('changePassword')}}" method="post">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label>Old Password</label>
                                    <input type="text" class="form-control" name="oldpassword">
                                </div>

                                <div class="form-group">
                                    <label>New Password</label>
                                    <input type="text" class="form-control" name="newpassword">
                                </div>

                                <div class="form-group">
                                    <label>Confirm New Password</label>
                                    <input type="text" class="form-control" name="confirmnewpassword">
                                </div>
                                <div class="form-group">
                                    <div><button class="btn btn-success pull-right mt-2 mb-2">Update Password</button></div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>

            </div>
        </div>



    </div>
@endsection
