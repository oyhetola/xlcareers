@extends('Layouts.examinerDashboard')
@section('content')
    <div class="mt-2 container">@include('includes.messages')</div>
    <div class="slim-mainpanel">
        <div class="container pd-t-50">
            <div class="row">
                <div class="col-lg-6">
                    <h3 class="tx-inverse mg-b-15">Hi, {{Auth::user()->firstname}}!</h3>
                    <p class="mg-b-40">.</p>

                    <h6 class="slim-card-title mg-b-15">Your Stats</h6>
                    <div class="row no-gutters">
                        <div class="col-sm-6">
                            <div class="card card-earning-summary">
                                <h6>Total Candidates</h6>
                                <h1>{{$candidates}}</h1>
                                <span>Total Candidates</span>
                            </div><!-- card -->
                        </div><!-- col-6 -->
                        <div class="col-sm-6">
                            <div class="card card-earning-summary mg-sm-l--1 bd-t-0 bd-sm-t">
                                <h6>Total Tested Candidates</h6>
                                <h1>{{$tested}}</h1>
                                <span>Total Shortlisted</span>
                            </div><!-- card -->
                        </div><!-- col-6 -->
                        {{--<div class="col-sm-4">
                            <div class="card card-earning-summary mg-sm-l--1 bd-t-0 bd-sm-t">
                                <h6>Total  clients</h6>
                                <h1>{{$clients}}</h1>
                                <span>Verified Clients</span>
                            </div><!-- card -->
                        </div><!-- col-6 -->--}}
                    </div><!-- row -->
                </div><!-- col-6 -->
                <div class="col-lg-6 mg-t-20 mg-sm-t-30 mg-lg-t-0">
                    <ul class="nav nav-activity-profile mg-t-20">
                        <li><a href="{{route('addQuestion')}}" class="nav-link"><i class="icon ion-ios-redo tx-purple"></i> Upload Questions</a></li>
                        <li class="nav-item"><a href="{{route('returnCandidatesView')}}" class="nav-link"><i class="icon ion-image tx-primary"></i> Filter </a></li>
                        {{--<li class="nav-item"><a href="{{route('experienceDetails')}}" class="nav-link"><i class="icon ion-document-text tx-success"></i> Experience </a></li>--}}
                    </ul>
                    <div class="mg-t-15">
                        <div class="card card-dash-headline card-sales">
                            <h6 class="slim-card-title">TRENDS</h6>
                            <h6 class="visitor-operating-label"></h6>
                            <div class="row">
                                <div class="col">
                                    <label class="tx-12">Candidate Sign Ups this week</label>
                                    <p>{{$signups}}</p>
                                </div><!-- col -->
                                <div class="col">
                                    <label class="tx-12">Recommended this week</label>
                                    <p>{{$recommended}}</p>
                                </div><!-- col -->
                                <div class="col">
                                    <label class="tx-12">Accepted Candidates This Month</label>
                                    <p>{{$tested}}</p>
                                </div><!-- col -->
                            </div><!-- row -->

                            <div class="progress mg-b-5">
                                <div class="progress-bar bg-success wd-75p" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>

                        </div><!-- card -->
                    </div>
                </div>
            {{--<div class="col-lg-6 mg-t-20 mg-sm-t-30 mg-lg-t-0">
                @if(Auth::user()->resume_status == 0)
                    <form class="md-form" id="resumeForm" method="post">
                        <h3>Upload your resume(.pdf*, .docx*)</h3>
                        <div class="file-field">
                            <div class="btn btn-primary btn-sm float-left">
                                <span>UPLOAD RESUME</span>
                                <input type="file" name="theResume">
                            </div>
                            <div>
                                <button type="submit" id="uploadResume" class="btn btn-success">Upload</button>
                            </div>
                            --}}{{--<div class="file-path-wrapper">
                                <input class="file-path validate" type="text" placeholder="Upload your file">
                            </div>--}}{{--
                        </div>
                    </form>
                @endif
                <div>
                    <a href="" class="btn btn-info">Take a test</a>
                </div>
            </div>--}}<!-- col-6 -->
            </div><!-- row -->
            <div class="clearfix"></div>


        </div><!-- slim-mainpanel -->
    </div>
    @include('includes.modals.modals')
    @include('includes.alerts')


@endsection
@section('script')
    <script>
        //alert();

        $.ajaxSetup({
            headers:
                {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });
    </script>
    <script src="{{asset('js/')}}"></script>
@endsection