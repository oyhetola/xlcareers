@extends('Layouts.dashboardLayout')
@section('content')
    <div class="mt-2 container">@include('includes.messages')</div>
    <div class="slim-mainpanel">
        <div class="container pd-t-50">
            @if(count($categories) > 0)
            <p class="font-lg-20" style="font-size:25px">Make sure to have strong a internet connection. Your time starts once you click on the test you want to take.</p>
            <ul>
                <li>To enjoy the best experience, please take the test on a PC.</li>
                <li>Do not refresh the else you loose your previous answers.</li>
                <li>Do not copy the question to another platform, your score will reduce by 1 for every copy.</li>
            </ul>
            @foreach($categories as $category)
                <div class="row">
                    <div class="col-md-3 mg-t-20 mg-md-t-0">
                        <a href="{{route('loadQuestions', ['catidnty'=>$category->id, 'catgry' => $category->category])}}">

                            <div class="card card-body bg-primary tx-white bd-0 cat" data-id="{{$category->id}}" data-category="{{$category->category}}">
                                <p class="card-text">{{$category->category}}</p>
                            </div><!-- card -->
                        </a>
                    </div>
                </div>

            @endforeach
             @else
                <p class="text-center text-lg-center">Hello, it appears you have taken all the tests assigned to you</p>
            @endif
            <div class="text-center"><button id="back" class="btn btn-success">Back</button></div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $.ajaxSetup({
            headers:
                {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });

        /*$(document).on('click', '.cat', function () {
            let ths = $(this);
            let id = $(this).data('id');
            let category = $(this).data('category');
            $.ajax({
                url: '{{route('loadQuestions')}}',
                type: 'POST',
                data: {'id':id, category:category},
                cache: false,
                //contentType: false,
                //processData: false,
                beforeSend: function () {
                    $(ths).attr("disabled", "disabled");
                    //$('#editQn').css("opacity", ".5");
                },
                success: function (response) {
                    $(ths).attr("disabled", false);
                }
            });
        });*/


        $(document).on("click","#back",function (){
            window.history.back();
        });
    </script>

@endsection