<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<div class='box' style="margin: 0 auto; width: 600px;  top: 200px; left: 50%; transform: -50%, 50%; border: 2px solid #b24a13;">
    <div style="width:600px; height:60px; background-color:#b23415; color:white; font-family: Montserrat; padding-top:12px; text-align:center;"><h3>Confirm Your TalentHub Account.</h3></div>
    <div style="text-align:center; align-content: center"><img src="" style="width: 400px; height:150px; padding-top: 15px;" alt="XL Outsourcing Limited">
        <h2 style=" font-family: Helvetica Neue, Arial, Helvetica, sans-serif;">Account Confirmation</h2>
    </div>
    <div style="font-family: Montserrat; text-align: left;  padding: 35px 30px; color: black">
        <p style="font-family: MontserratBlack; padding: 0 10px;">Hello, {{$name}}</p>
    <p>{!! $message !!}</p>
       
        <br>
        <i><p><strong>Cheers!</strong></p>
            <strong>XL Outsourcing Limited.</strong>
        </i>
    </div>
</div>
<!-- <button class="btn btn-primary" style="background-color: #20B2AA;
padding: 15px 3px; width: 150px; color: white; border: 0;">Verify Account</button>
</div> -->
</body>
</html>