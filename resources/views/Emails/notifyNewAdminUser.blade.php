<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--<link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>--}}
</head>

<body>
<div class='box' style="margin: 0 auto; width: 600px;  top: 200px; left: 50%; transform: -50%, 50%; border: 2px solid #b21806;">
    <div style="width:600px; height:60px; background-color:#b21806; color:white; font-family: Georgia, serif; padding-top:12px; text-align:center;"><h3></h3></div>
    <div style="text-align:center; align-content: center">
        {{--<img src="" style="width: 400px; height:250px; padding-top: 15px;">--}}
        <h2 style=" font-family: Georgia, serif;">Account Creation Notification</h2>
    </div>
    {{--{{$message->embed(public_path().'/img/logos/nutrifair.jpg')}}--}}
    <div style="font-family: cursive; text-align: left;  padding: 35px 30px;">
        <p style="font-family: cursive; padding: 0 15px;">Hi {{$name}},   </p>
        <p> The admin on <b>XLTalentHub</b> created and account for you. Here are your login details</p>
        <p>Your official email : {{$email}}  is your username</p>
        <p>Your default password is: <b>12345678</b> (You can change this from settings in your dashboard)</p>
        <br>
        <a href="https://talenthub.xlafricagroup.com" class="btn btn-primary" style="background-color: #b24a23;
padding: 15px 3px; width: 150px; color: white; border: 0;">Get started here</a>


        <br>
        <i><p><strong>Automated Mail</strong></p>
            <strong>XLTalentHub .</strong><br>
            Plot 883, Samuel Manuwa Street, Off Bishop Aboyade Close, VI, Lagos.
        </i>
    </div>

</div>

<!-- <button class="btn btn-primary" style="background-color: #20B2AA;
padding: 15px 3px; width: 150px; color: white; border: 0;">Verify Account</button>
</div> -->
</body>
</html>