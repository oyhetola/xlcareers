<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--<link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>--}}
</head>

<body>
{{--<div class='box' >
    <table style="margin: 0 auto; width: 600px;  top: 200px; left: 50%; transform: -50%, 50%; border: 2px solid #b22d1e;">
        <tbody>
        <tr style="width:600px; height:60px; background-color:#b22d1e; color:white; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; padding-top:12px; text-align:center;">
            <h3>XL OUTSOURCING LTD</h3>
        </tr>
        <tr style="font-family: cursive; text-align: left;  padding: 35px 30px;">
            <p style="font-family: 'Courier New'; padding: 0 15px;">Dear {{$recipient}},</p>
            <p>
                {{$body}}
            </p>
        </tr>
        </tbody>
    </table>
</div>--}}
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td style="padding: 10px 0 30px 0;">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc; border-collapse: collapse;">
                <tr>
                    <td align="center" bgcolor="#b22d1e" style="padding: 40px 0 30px 0; color: #f7fbfe; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
                        <img src="{{asset('img/logo2.png')}}" alt="XL Africa Group"  style="display: block; color: white"/>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
                                    {{--<b>{{$title}}</b>--}}
                                </td>
                            </tr>
                            <tr><p>Hello {{$name}},</p></tr>
                            <tr>
                                <td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
                                    <p>We are glad to let you  know that your application on <a href="https://xlafricagroup.com">XL AFRICA GROUP</a> to the job with ID: <strong>{{strtoupper($jobid)}}</strong>, has been received.
                                        This application is being reviewed and we'll sure get back to you within 2 weeks. </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#ee4c50" style="padding: 30px 30px 30px 30px;">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;" width="75%">
                                    &reg; XL Africa Group {{date('Y')}}<br/>
                                    <a href="#" style="color: #ffffff;"><font color="#ffffff"></font></a>  XL Africa Group.
                                </td>
                                <td align="right" width="25%">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td style="font-family: Arial, sans-serif; font-size: 12px; font-weight: bold;">
                                                <a href="#" style="color: #ffffff;">
                                                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/210284/tw.gif" alt="Twitter" width="38" height="38" style="display: block;" border="0" />
                                                </a>
                                            </td>
                                            <td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
                                            <td style="font-family: Arial, sans-serif; font-size: 12px; font-weight: bold;">
                                                <a href="#" style="color: #ffffff;">
                                                    <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/210284/fb.gif" alt="Facebook" width="38" height="38" style="display: block;" border="0" />
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>