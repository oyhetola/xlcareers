@if(count($errors)>0)
    <div class="alert alert-danger">
        <i class="pull-right fa fa-close"></i>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
    <div class="clearfix"></div>
@endif

@if(Session::has('errorMessage'))
    <div class="alert alert-danger">
        {{Session::get('errorMessage')}}
        <i class="pull-right ti-close"></i>
        <i class="material-icons float-right fa fa-close"></i>
    </div>
    {{Session::forget('errorMessage')}}

@endif

@if(Session::has('successfulMessage'))

    <div class="alert alert-success">
        {!! Session::get('successfulMessage') !!}
        <i class="pull-right fa fa-close"></i>
        <i class="float-right fa fa-close"></i>
    </div>
    {{Session::forget('successfulMessage')}}

@endif


<script src="{{asset('lib/jquery/js/jquery.js')}}"></script>
<script>
    $(document).on('click', '.fa-close', function () {
        $(this).parent().remove();
    });
</script>