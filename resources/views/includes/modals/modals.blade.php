
<div id="personalInfoModal" class="modal fade" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content tx-size-sm">
            <div class="modal-header pd-x-20">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Personal Information</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body pd-20">
                <p class="mg-b-20 mg-sm-b-40">Help employers find you easily by filling the following info.</p>
                <div class="row mb-5 personalInfo">
                    <label class="col-sm-4 form-control-label section-title"><span class="tx-danger"></span>PERSONAL INFORMATION</label>
                    <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                        <div class="form-group" >
                            <label>Your Phone Number</label>
                            <input type="email" name="phone-number" class="form-control" placeholder="Enter Phone Number">
                        </div>
                        <div class="form-group" >
                            <label>Date of Birth</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="icon ion-calendar tx-16 lh-0 op-6"></i>
                                    </div>
                                </div>
                                <input type="text" id="dateOfBirth" name="dateOfBirth" class="form-control fc-datepicker" data-provide='datepicker' data-date-container='#personalInfoModal' placeholder="YYYY/MM/DD" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Country of Origin</label>
                            <select name="country" class="form-control">
                                <option></option>
                            </select>
                        </div>
                        <div class="form-group" >
                            <label>State of Origin</label>
                            <select name="stateoforigin" class="form-control">
                                <option></option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Country of Residence</label>
                            <select name="countryOfResidence" class="form-control">
                                <option></option>
                            </select>
                        </div>
                        <div class="form-group regn">
                            <label>Region of Residence</label>
                            <select name="regionOfResidence" class="form-control">
                                <option selected disabled>Select Region</option>
                                <option value="1">South-West</option>
                                <option value="2">North-Central</option>
                                <option value="3">South-East</option>
                                <option value="4">South-South</option>
                                <option value="5">North-East</option>
                                <option value="6">North-West</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>State of Residence</label>
                            <select name="stateofresidence" class="form-control">
                                <option></option>
                            </select>
                        </div>
                        <div class="form-group" >
                            <label>Residential Address</label>
                            <textarea  name="address" rows="4" class="form-control" placeholder="Enter your address"></textarea>
                        </div>
                        {{--<div class="form-group">
                            <label>Name of Next of kin</label>
                            <input type="text" name="nextOfKin" class="form-control" placeholder="Enter firstname">
                        </div>
                        <div class="form-group">
                            <label>Address of next of kin</label>
                            <textarea rows="3" name="nextOfKinAddress" class="form-control" placeholder="Enter next of kin Address"></textarea>
                        </div>
                        <div class="form-group" >
                            <label>Phone Number of Next of Kin</label>
                            <input type="text" name="nextOfKinPhoneNum" class="form-control" placeholder="Enter Phone Number">
                        </div>--}}
                    </div>
                </div>
            </div><!-- modal-body -->
            <div class="modal-footer">
                <button type="button" id="submitPersonalInfo" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div><!-- modal-dialog -->
</div>


<div id="educationInfoModal" class="modal fade" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content tx-size-sm">
            <div class="modal-header pd-x-20">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Education</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body pd-20">
                <p class="mg-b-20 mg-sm-b-40">Help employers find you easily by filling the following info.</p>
                <div class="row mt-5 education">
                    <label class="col-sm-4 form-control-label section-title"><span class="tx-danger"></span>Education</label>
                    <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                        <div id="educationDetails">
                            <div class="singleEducationDetails">
                                <div class="form-group">
                                    <label>School</label>
                                    <input type="text" name="nameOfSchool" class="form-control" placeholder="Enter name of school">
                                </div>
                                <div class="form-group" >
                                    <label>Country</label>
                                    <select name="schoolCountry" class="form-control">
                                        <option></option>
                                    </select>
                                </div>
                                <div class="form-group" >
                                    <label>State</label>
                                    <select name="schoolState" class="form-control">
                                        <option></option>
                                    </select>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group" >
                                            <label>From</label>
                                            <select name="schoolStartMonth" class="form-control">
                                                <option></option>
                                            </select>
                                            <select name="schoolStartYear" class="form-control">
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group" >
                                            <label>To</label>
                                            <select name="schoolEndMonth" class="form-control">
                                                <option></option>
                                            </select>
                                            <select name="schoolEndYear" class="form-control">
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group" >
                                        <label></label>
                                        <input name="currentStudyInstitution" type="checkbox">I currently study here
                                    </div>
                                </div>
                                <div class="form-group" >
                                    <label>Field of Study</label>
                                    <input type="text" name="studyField" placeholder="e.g. Sciences, Arts, Engineering, Pharmacy, " class="form-control">
                                </div>
                                <div class="form-group mb-4">
                                    <label>Level of Education</label>
                                    <select name="educationLevel" class="form-control">
                                        <option selected disabled>Select</option>
                                        <option>Primary School Leaving Certificate</option>
                                        <option>Secondary School Leaving Certificate</option>
                                        <option>Ordinary National Diploma (OND)</option>
                                        <option>Higher National Diploma (HND)</option>
                                        <option>Bachelors Degree (BSC, BA)</option>
                                        <option>Graduate Diploma</option>
                                        <option>Masters Degree (MSc.)</option>
                                        <option>Doctorate Degree (PhD.)</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        {{--<div class="row"><p class="float-right addEducationButton"><i class="fa fa-plus"></i> Add new Education</p></div>--}}
                    </div>
                </div>
            </div><!-- modal-body -->
            <div class="modal-footer">
                <button type="button" id="submitEducationInfo" class="btn btn-primary">Add</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div><!-- modal-dialog -->
</div>


<div id="experienceInfoModal" class="modal fade" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content tx-size-sm">
            <div class="modal-header pd-x-20">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Your Experience</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body pd-20">
                <p class="mg-b-20 mg-sm-b-40">Help employers find you easily by filling the following info.</p>
                <div class="row mt-5 experience">
                    <label class="col-sm-4 form-control-label section-title"><span class="tx-danger"></span>EXPERIENCE</label>
                    <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                        <div id="addExperience">
                            <div class="singleJobExperience">
                                <div class="form-group">
                                    <label>Company Name</label>
                                    <input type="text" name="companyName" class="form-control" placeholder="Company Name">
                                </div>
                                <div class="form-group">
                                    <label>Job Title</label>
                                    <input type="text" name="jobTitle" class="form-control" placeholder="Role">
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group" >
                                            <label></label>
                                            <input type="checkbox" name="currentWorkPlace">I currently work here
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group" >
                                            <label>From</label>
                                            <select name="workStartMonth" class="form-control">
                                                <option></option>
                                            </select>
                                            <select name="workStartYear" class="form-control">
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group" >
                                            <label>To</label>
                                            <select name="workEndMonth" class="form-control">
                                                <option></option>
                                            </select>
                                            <select name="workEndYear" class="form-control">
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label>Description</label>
                                    <textarea rows="5" name="jobDescription" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                        {{--<div class="row"><p class="float-right addExperienceButton"><i class="fa fa-plus"></i> Add new job Experience</p></div>--}}
                    </div>
                </div>
            </div><!-- modal-body -->
            <div class="modal-footer">
                <button type="button" id="submitExperienceInfo" class="btn btn-primary">Add</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div><!-- modal-dialog -->
</div>

