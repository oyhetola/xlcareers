<div id="editPersonal" class="modal fade" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content tx-size-sm">
            <div class="modal-header pd-x-20">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Edit Personal Info</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body pd-20">
                <p class="mg-b-20 mg-sm-b-40"></p>
                <div class="row mt-5">
                    {{--<label class="col-sm-4 form-control-label section-title"><span class="tx-danger"></span>EXPERIENCE</label>--}}
                    <div class="col-sm-12 mg-t-10 mg-sm-t-0">
                        <div id="">
                            <div class="userUpdate">
                                <div class="form-group">
                                    <label>Firstname</label>
                                    <input type="text" name="cfirstname" class="form-control" value="{{Auth::user()->firstname}}" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label>Lastname</label>
                                    <input type="text" name="clastname" class="form-control" value="{{Auth::user()->lastname}}" placeholder="">
                                </div>
                                <div class="form-group mb-4">
                                    <label>Email</label>
                                    <input type="text" name="cemail" value="{{Auth::user()->email}}" class="form-control">
                                </div>
                                <div class="form-group mb-4">
                                    <label>Username</label>
                                    <input type="text" name="cusername" value="{{Auth::user()->username}}" class="form-control">
                                </div>

                            </div>
                        </div>
                        {{--<div class="row"><p class="float-right addExperienceButton"><i class="fa fa-plus"></i> Add new job Experience</p></div>--}}
                    </div>
                </div>
            </div><!-- modal-body -->
            <div class="modal-footer">
                <button type="button" id="updateUserPersonalInfo" class="btn btn-primary btn-sm">Update</button>
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div><!-- modal-dialog -->
</div>

<div id="editExperience" class="modal fade" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content tx-size-sm">
            <div class="modal-header pd-x-20">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Edit Work Experience</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body pd-20">
                <p class="mg-b-20 mg-sm-b-40"></p>
                <div class="row mt-5">
                    {{--<label class="col-sm-4 form-control-label section-title"><span class="tx-danger"></span>EXPERIENCE</label>--}}
                    <div class="col-sm-12 mg-t-10 mg-sm-t-0">
                        <div id="">
                            <div class="updateExp">
                                <input type="hidden" name="theId">
                                <div class="form-group">
                                    <label>Company Name</label>
                                    <input type="text" name="companyName" class="form-control" placeholder="Company Name">
                                </div>
                                <div class="form-group">
                                    <label>Job Title</label>
                                    <input type="text" name="jobTitle" class="form-control" placeholder="Role">
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group" >
                                            <label></label>
                                            <input type="checkbox" name="currentWorkPlace">I currently work here
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group" >
                                            <label>From</label>
                                            <select name="workStartMonth" class="form-control">
                                                <option></option>
                                            </select>
                                            <select name="workStartYear" class="form-control">
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group" >
                                            <label>To</label>
                                            <select name="workEndMonth" class="form-control">
                                                <option></option>
                                            </select>
                                            <select name="workEndYear" class="form-control">
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label>Description</label>
                                    <textarea rows="5" name="jobDescription" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                        {{--<div class="row"><p class="float-right addExperienceButton"><i class="fa fa-plus"></i> Add new job Experience</p></div>--}}
                    </div>
                </div>
            </div><!-- modal-body -->
            <div class="modal-footer">
                <button type="button" id="updateUserExperience" class="btn btn-primary btn-sm">Update</button>
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div><!-- modal-dialog -->
</div>


<div id="editEducation" class="modal fade" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content tx-size-sm">
            <div class="modal-header pd-x-20">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Edit Education Info</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body pd-20">
                <p class="mg-b-20 mg-sm-b-40"></p>
                <div class="row mt-5 experience">
                    {{--<label class="col-sm-4 form-control-label section-title"><span class="tx-danger"></span>EXPERIENCE</label>--}}
                    <div class="col-sm-12 mg-t-10 mg-sm-t-0">
                        <div id="">
                            <div class="u">
                                <div class="updatedu">
                                    <input type="hidden" name="eduId">
                                    <div class="form-group">
                                        <label>School</label>
                                        <input type="text" name="nameOfSchool" class="form-control" placeholder="Enter name of school">
                                    </div>
                                    <div class="form-group" >
                                        <label>Country</label>
                                        <select name="schoolCountry" class="form-control">
                                            <option></option>
                                        </select>
                                    </div>
                                    <div class="form-group" >
                                        <label>State</label>
                                        <select name="schoolState" class="form-control">
                                            <option></option>
                                        </select>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group" >
                                                <label>From</label>
                                                <select name="schoolStartMonth" class="form-control">
                                                    <option></option>
                                                </select>
                                                <select name="schoolStartYear" class="form-control">
                                                    <option></option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group" >
                                                <label>To</label>
                                                <select name="schoolEndMonth" class="form-control">
                                                    <option></option>
                                                </select>
                                                <select name="schoolEndYear" class="form-control">
                                                    <option></option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group" >
                                            <label></label>
                                            <input name="currentStudyInstitution" type="checkbox">I currently study here
                                        </div>
                                    </div>
                                    <div class="form-group" >
                                        <label>Field of Study</label>
                                        <input type="text" name="studyField" placeholder="e.g. Sciences, Arts, Engineering, Pharmacy, " class="form-control">
                                    </div>
                                    <div class="form-group mb-4">
                                        <label>Level of Education</label>
                                        <select name="educationLevel" class="form-control">
                                            <option selected disabled>Select</option>
                                            <option>Primary School Leaving Certificate</option>
                                            <option>College Certificate</option>
                                            <option>Diploma</option>
                                            <option>Degree</option>
                                            <option>Graduate Diploma</option>
                                            <option>Second Degree</option>
                                            <option>Doctorate Degree</option>
                                        </select>
                                    </div>

                            </div>
                        </div>
                        {{--<div class="row"><p class="float-right addExperienceButton"><i class="fa fa-plus"></i> Add new job Experience</p></div>--}}
                    </div>
                </div>
            </div><!-- modal-body -->
            <div class="modal-footer">
                <button type="button" id="updateEducationInfo" class="btn btn-primary btn-sm">Update</button>
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div><!-- modal-dialog -->
    </div>
</div>