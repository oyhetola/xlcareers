<div class="form-group col-md-6">
    <input type="text" class="form-control theoption" placeholder="Option1" id="option1">
</div>
<div class="form-group col-md-6">
    <input type="text" class="form-control theoption" placeholder="Option2" id="option2">
</div>
<div class="form-group col-md-6">
    <div class="input-group">
    <input type="text" class="form-control theoption" placeholder="Option3" id="option3">
    <div class="input-group-append removeoption"><i class="input-group-text"></i></div>
    </div>
</div>
<div class="form-group col-md-6">
    <div class="input-group">
    <input type="text" class="form-control theoption" placeholder="Option4" id="option4">
    <div class="input-group-append removeoption"><i class="input-group-text"></i></div>
    </div>
</div>
<div id="moreoptions" class=""></div>
<div class="col-md-12 offset-md-4">
    <button class="btn btn-default text-center addmore" type="button"><i class="ti-plus"></i> </button>
</div>
<div class="clearfix"></div>
