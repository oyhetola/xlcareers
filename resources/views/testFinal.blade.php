@extends('Layouts.dashboardLayout')
@section('content')
    <div class="mt-2 container">@include('includes.messages')</div>
    <div id="msg"></div>
    <div class="slim-mainpanel">
        <div class="section-wrapper">
            <div class="">
                <div class="card card-body">
                    <p class="text-center" style="text-align: center; font-size: larger; color: green">You successfully submitted the test</p>
                    @if($result)
                        <p style="text-align: center;"><b>{{$result->score}}%</b></p>
                        @if($result->badge_received == 1 && $result->score >= 80)
                            <div class="col-md-12" style="text-align: center"><img class="img-responsive" src="{{asset('/badges/cup.png')}}" height="300px" width="300px"></div>
                            <div style="text-align: center; font-weight: bold; color: #2b542c" class="mt-1 mb-1">Badge Received! Welldone </div>
                        @endif()
                        <div style="text-align: center" class="mt-2">
                            <a style="align-self: center" class="btn btn-success" href="{{route('dashboard')}}"><i class="ti-dashboard"></i> Dashboard</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection