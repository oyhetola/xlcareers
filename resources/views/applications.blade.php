@extends('Layouts.dashboardLayout')

@section('content')
    <div class="mt-2 container">@include('includes.messages')</div>
    <div id="msg"></div>
    <div class="slim-mainpanel">
        <div class="container pd-t-50">
            <div class="row">
                <div class="col-lg-6">
                    {{--<h3 class="tx-inverse mg-b-15">Hi, {{Auth::user()->firstname}}!</h3>--}}
                    {{--<p class="mg-b-40">.</p>--}}

                    <h2 class="header-skin-body">My Applications</h2>
                    {{--<div class="row no-gutters">
                        <div class="col-sm-6">
                            <div class="card card-earning-summary">
                                <h6>Profile Views</h6>
                                <h1>0</h1>
                                <span>Profile Views</span>
                            </div><!-- card -->
                        </div><!-- col-6 -->
                        <div class="col-sm-6">
                            <div class="card card-earning-summary mg-sm-l--1 bd-t-0 bd-sm-t">
                                <h6>Rating</h6>
                                <h1>...</h1>
                                <span>Rating</span>
                            </div><!-- card -->
                        </div><!-- col-6 -->
                    </div><!-- row -->--}}
                </div><!-- col-6 -->
                <div class="col-lg-6 offset-lg-3 mg-t-20 mg-sm-t-30 mg-lg-t-0">
                    {{--<ul class="nav nav-activity-profile mg-t-20">
                        @if(\Illuminate\Support\Facades\Auth::user()->personalInfo_filled == 0)
                            <li class="pinfo" data-toggle="modal" data-target="#personalInfoModal" ><a href="javascript:void(0)" class="nav-link"><i class="icon ion-ios-redo tx-purple"></i> Personal Info</a></li>
                        @endif
                        @if(\Illuminate\Support\Facades\Auth::user()->educationInfo_filled == 0)
                            <li class="nav-item einfo"><a href="{{route('educationDetails')}}" class="nav-link"><i class="icon ion-image tx-primary"></i> Education </a></li>
                        @endif
                        @if(\Illuminate\Support\Facades\Auth::user()->experienceInfo_filled == 0)
                            <li class="nav-item exinfo"><a href="{{route('experienceDetails')}}" class="nav-link"><i class="icon ion-document-text tx-success"></i> Experience </a></li>
                        @endif
                    </ul>--}}

                    @if(count($applications) > 0)
                        @foreach($applications as $application)
                        <div class="mg-t-15">
                            <div class="card card-body">
                                <h6 class="slim-card-title">{{$application->jobOpening->title}}</h6>
                                <b class="mg-t-30" style="color: black"><i>Description</i></b>
                                <p>{{$application->jobOpening->description}}</p>
                                <b class="float-right">Status: {{($application->application_status == 0 ? 'Pending' : 'In view')}}</b>
                            </div><!-- card -->
                        </div>
                        @endforeach
                        <div>{{ $applications->links() }}</div>
                    @else
                        <p>You do not have any application history at the time.</p>
                    @endif

                </div>
            </div><!-- row -->
            <div class="clearfix"></div>
        {{--<div class="row">
            <div class="col-md-12 section-wrapper mg-t-20">
                <label class="section-title">FILL YOUR INFORMATION.</label>
                <p class="mg-b-20 mg-sm-b-40">Help employers find you easily by filling the following info.</p>
                <div class="row mb-5 personalInfo">
                    <label class="col-sm-4 form-control-label section-title"><span class="tx-danger"></span>PERSONAL INFORMATION</label>
                    <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                        <div class="form-group">
                            <label>Firstname</label>
                            <input type="text" name="firstname" class="form-control" placeholder="Enter firstname">
                        </div>
                        --}}{{--<div class="form-group">
                            <label>Middlename</label>
                            <input type="text" name="middlename" class="form-control" placeholder="Enter Middlename">
                        </div>--}}{{--
                        <div class="form-group">
                            <label>Lastname</label>
                            <input type="text" name="lastname" class="form-control" placeholder="Enter Lastname">
                        </div>
                        <div class="form-group" >
                            <label>Email</label>
                            <input type="email" name="email" class="form-control" placeholder="Enter Email">
                        </div>
                        <div class="form-group" >
                            <label>Phone Number</label>
                            <input type="email" name="phone-number" class="form-control" placeholder="Enter Phone Number">
                        </div>
                        <div class="form-group">
                            <label>Country of Origin</label>
                            <select name="country" class="form-control">
                                <option></option>
                            </select>
                        </div>
                        <div class="form-group" >
                            <label>State of Origin</label>
                            <select name="stateoforigin" class="form-control">
                                <option></option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Country of Residence</label>
                            <select name="countryOfResidence" class="form-control">
                                <option></option>
                            </select>
                        </div>
                        <div class="form-group" >
                            <label>State of Residence</label>
                            <select name="stateofresidence" class="form-control">
                                <option></option>
                            </select>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row mt-5 education">
                    <label class="col-sm-4 form-control-label section-title"><span class="tx-danger"></span>Education</label>
                    <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                        <div id="educationDetails">
                            <div class="singleEducationDetails">
                                <div class="form-group">
                                    <label>School</label>
                                    <input type="text" name="nameOfSchool" class="form-control" placeholder="Enter name of school">
                                </div>
                                <div class="form-group" >
                                    <label>Country</label>
                                    <select name="schoolCountry" class="form-control">
                                        <option></option>
                                    </select>
                                </div>
                                <div class="form-group" >
                                    <label>State</label>
                                    <select name="schoolState" class="form-control">
                                        <option></option>
                                    </select>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group" >
                                            <label>From</label>
                                            <select name="schoolStartMonth" class="form-control">
                                                <option></option>
                                            </select>
                                            <select name="schoolStartYear" class="form-control">
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group" >
                                            <label>To</label>
                                            <select name="schoolEndMonth" class="form-control">
                                                <option></option>
                                            </select>
                                            <select name="schoolEndYear" class="form-control">
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group" >
                                        <label></label>
                                        <input name="currentStudyInstitution" type="checkbox">I currently study here
                                    </div>
                                </div>
                                <div class="form-group" >
                                    <label>Field of Study</label>
                                    <input type="text" name="studyField" placeholder="e.g. Sciences, Arts, Engineering, Pharmacy, " class="form-control">
                                </div>
                                <div class="form-group mb-4">
                                    <label>Level of Education</label>
                                    <select name="educationLevel" class="form-control">
                                        <option selected disabled>Select</option>
                                        <option>School Leaving Certificate</option>
                                        <option>Diploma</option>
                                        <option>Degree</option>
                                        <option>Graduate Diploma</option>
                                        <option>Second Degree</option>
                                        <option>Doctorate Degree</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row"><p class="float-right addEducationButton"><i class="fa fa-plus"></i> Add new Education</p></div>
                    </div>
                </div>
                <hr>
                <div class="row mt-5 experience">
                    <label class="col-sm-4 form-control-label section-title"><span class="tx-danger"></span>EXPERIENCE</label>
                    <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                        <div id="addExperience">
                            <div class="singleJobExperience">
                                <div class="form-group">
                                    <label>Company Name</label>
                                    <input type="text" name="companyName" class="form-control" placeholder="Company Name">
                                </div>
                                <div class="form-group">
                                    <label>Job Title</label>
                                    <input type="text" name="jobTitle" class="form-control" placeholder="Role">
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group" >
                                            <label></label>
                                            <input type="checkbox" name="currentWorkPlace">I currently work here
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group" >
                                            <label>From</label>
                                            <select name="workStartMonth" class="form-control">
                                                <option></option>
                                            </select>
                                            <select name="workStartYear" class="form-control">
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group" >
                                            <label>To</label>
                                            <select name="workEndMonth" class="form-control">
                                                <option></option>
                                            </select>
                                            <select name="workEndYear" class="form-control">
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label>Description</label>
                                    <textarea rows="5" name="jobDescription" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row"><p class="float-right addExperienceButton"><i class="fa fa-plus"></i> Add new job Experience</p></div>
                    </div>
                </div>
                <hr>
                --}}{{--<div class="row mt-5 document">
                    <label class="col-sm-4 form-control-label section-title"><span class="tx-danger"></span>UPLOAD RESUME</label>
                    <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                        <div>
                            <div class="form-group">
                                <label>Upload from your device</label>
                                <input type="file">
                            </div>
                        </div>
                    </div>
                </div>--}}{{--

        </div>

            <div class="mt-4 mb-4">
                <div class="row">
                    <div class="col-md-10"></div>
                    <div class="col-md-2">
                        <div class="form-layout-footer mg-t-30">
                            <button class="btn btn-primary bd-0" id="submitIt">Submit Form</button>
                        </div>
                    </div>
                </div>
            </div>
        --}}{{--<div class="row">
            <div class="section-wrapper mg-t-20">
                <label class="section-title">Personal Information</label>
                <p class="mg-b-20 mg-sm-b-40">A bordered form group wrapper with a label on top of each form control.</p>

                <div class="form-layout form-layout-2">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-control-label">Firstname: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="firstname" value="John Paul" placeholder="Enter firstname">
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-4 mg-t--1 mg-md-t-0">
                            <div class="form-group mg-md-l--1">
                                <label class="form-control-label">Lastname: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="lastname" value="McDoe" placeholder="Enter lastname">
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-4 mg-t--1 mg-md-t-0">
                            <div class="form-group mg-md-l--1">
                                <label class="form-control-label">Email address: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="email" value="johnpaul@yourdomain.com" placeholder="Enter email address">
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-8">
                            <div class="form-group bd-t-0-force">
                                <label class="form-control-label">Mail address: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="address" value="Market St., San Francisco" placeholder="Enter address">
                            </div>
                        </div><!-- col-8 -->
                        <div class="col-md-4">
                            <div class="form-group mg-md-l--1 bd-t-0-force">
                                <label class="form-control-label mg-b-0-force">Country: <span class="tx-danger">*</span></label>
                                <select id="select2-a" class="form-control select2-hidden-accessible" data-placeholder="Choose country" tabindex="-1" aria-hidden="true">
                                    <option label="Choose country"></option>
                                    <option value="USA" selected="">United States of America</option>
                                    <option value="UK">United Kingdom</option>
                                    <option value="China">China</option>
                                    <option value="Japan">Japan</option>
                                </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 302px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-select2-a-container"><span class="select2-selection__rendered" id="select2-select2-a-container" title="United States of America">United States of America</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                            </div>
                        </div><!-- col-4 -->
                    </div><!-- row -->
                    <div class="form-layout-footer bd pd-20 bd-t-0">
                        <button class="btn btn-primary bd-0">Submit Form</button>
                        <button class="btn btn-secondary bd-0">Cancel</button>
                    </div><!-- form-group -->
                </div><!-- form-layout -->
            </div>
        </div>--}}{{--
    </div>--}}<!-- container -->

        </div><!-- slim-mainpanel -->
    </div>
    @include('includes.modals.modals')
    @include('includes.alerts')


@endsection
@section('script')
    <script>
        //alert();
        generateYears();
        populateMonth();
        populateCountry();
        populateState($('select[name=stateoforigin]'));
        populateState($('select[name=stateofresidence]'));
        populateState($('select[name=schoolState]'));

        $.ajaxSetup({
            headers:
                {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });


        $('#applyToJob').on('click', function () {
            let job = $(this).data('jobid');
            $.ajax({
                type: 'POST',
                url: "{{route('signifyInterest')}}",
                data: {job: job},
                //cache: false,
                beforeSend: function (){
                    $('#applyToJob').attr("disabled", "disabled");
                    $('.card').css("opacity", ".5");
                },
                success:function (data) {
                    console.log(data);
                    $('.card').css("opacity", "");
                    //ths.attr('disabled', false);
                    if(data.error){
                        $('#error').modal('toggle');
                        let msg = "<p>"+data.error+".</p>";
                        $('#message').html(msg);
                    }else if(data.success){
                        //$('.selectMultiple').prop('checked', '');
                        $('#success').modal('toggle');
                        let msg = "<p>"+data.success+"</p>";
                        $('#successmsg').html(msg);
                        //setTimeout(function (){ location.reload(); }, 1200);
                    }else if(data.logout){
                        location.href = "/sign-in"
                    }
                },
                error: function (error){
                    console.log(error);
                    $('#error').modal('toggle');
                    let msg = "<p>"+error.error+".</p>";
                    $('#message').html(msg);
                    //$('#recommendToAdmin').attr('disabled', false);
                    $('.card').css("opacity", "");
                }
            });
        });

    </script>
    {{--<script src="{{asset('js/formSubmission.js')}}"></script>--}}
@endsection