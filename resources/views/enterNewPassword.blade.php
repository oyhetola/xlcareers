<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from themepixels.me/slim1.1/template/page-signup.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 02 Jun 2019 13:04:28 GMT -->
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Slim">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="../../slim/img/slim-social.html">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/slim">
    <meta property="og:title" content="Slim">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="../../slim/img/slim-social.html">
    <meta property="og:image:secure_url" content="../../slim/img/slim-social.html">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>XL Outsourcing Limited - Talenthub</title>

    <!-- Vendor css -->
    <link href="../lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="../lib/Ionicons/css/ionicons.css" rel="stylesheet">

    <!-- Slim CSS -->
    <link rel="stylesheet" href="../css/slim.css">

</head>
<body>

<div class="signin-wrapper">

    <div class="signin-box signup">
        <h2 class="slim-logo"><a href="">XLTalenthub<span>.</span></a></h2>
        <h3 class="signin-title-primary">Create new password</h3>
        <h5 class="signin-title-secondary lh-4"></h5>
        @include('includes.messages')
        {{--<div class="row row-xs mg-b-10">--}}
        <form action="{{route('changeThePassword')}}" method="post">
            {{csrf_field()}}
            <input type="hidden" name="token" value="{{$token}}">
            <div class="row row-xs mg-b-10">
                <label>New Password</label>
                <input type="password" name="password" class="form-control" placeholder="New Password">
            </div>
            <div class="row row-xs mg-t-10 mg-sm-t-0 mg-b-10">
                <label>Confirm New Password</label>
                <input type="password" name="confirmNewPassword" class="form-control" placeholder="Confirm new password">
            </div>
            {{--</div><!-- row -->--}}

            <button class="btn btn-primary btn-block btn-signin">Create New Password</button>

        </form>

    </div><!-- signin-box -->

</div><!-- signin-wrapper -->

<script src="../lib/jquery/js/jquery.js"></script>
<script src="../lib/popper.js/js/popper.js"></script>
<script src="../lib/bootstrap/js/bootstrap.js"></script>

<script src="../js/slim.js"></script>

</body>

<!-- Mirrored from themepixels.me/slim1.1/template/page-signup.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 02 Jun 2019 13:04:28 GMT -->
</html>
