@extends('Layouts.dashboardLayout')
@section('content')
    <div class="mt-2 container">@include('includes.messages')</div>
    <div id="msg"></div>
    <div class="slim-mainpanel">
        <div class="container pd-t-50">
            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                    {{--<li class="breadcrumb-item"><a href="#">Pages</a></li>--}}
                    <li class="breadcrumb-item active" aria-current="page">Profile Page</li>
                </ol>
                <h6 class="slim-pagetitle">My Profile</h6>
            </div>

            <div class="row row-sm">
                <div class="col-lg-8">
                    <div class="card card-profile">
                        <div class="card-body">
                            <div class="media">
                                <img src="../img/user-image.png" alt="">
                                <div class="media-body">
                                    <h3 class="card-profile-name">{{Auth::user()->firstname}} {{Auth::user()->lastname}}</h3>
                                    {{--<p class="card-profile-position">Executive Director at <a href="#">ThemePixels, Inc.</a></p>--}}
                                    {{--<p>San Francisco, California</p>--}}

                                    <p class="mg-b-0">About me<a href="#"></a></p>
                                </div><!-- media-body -->
                            </div><!-- media -->
                        </div><!-- card-body -->
                        <div class="card-footer">
                            {{--<a href="#" class="card-profile-direct">http://thmpxls.me/profile?id=katherine</a>--}}
                            <div>
                                <a><i class="fa fa-user-circle"></i> </a>
                                <a data-toggle="modal" data-target="#editPersonal">Profile Settings</a>
                            </div>
                        </div><!-- card-footer -->
                    </div><!-- card -->

                    {{--<ul class="nav nav-activity-profile mg-t-20">
                        <li class="nav-item"><a href="#" class="nav-link"><i class="icon ion-ios-redo tx-purple"></i> Share an update</a></li>
                        <li class="nav-item"><a href="#" class="nav-link"><i class="icon ion-image tx-primary"></i> Publish photo</a></li>
                        <li class="nav-item"><a href="#" class="nav-link"><i class="icon ion-document-text tx-success"></i> Add an article</a></li>
                    </ul>--}}<!-- nav -->



                    <div class="card card-experience mg-t-20">
                        <div class="card-body">
                            <div class="slim-card-title">Work Experience</div>
                            <div class="takemediaExp">
                            @if(count(Auth::user()->experience) > 0)
                            @foreach(Auth::user()->experience as $experience)
                            <div class="media" id="exp{{$experience->id}}">
                                <div class="experience-logo">
                                    <i class="fa fa-briefcase"></i>
                                </div><!-- experience-logo -->

                                <div class="media-body">
                                    <h6 class="position-name">{{$experience->job_title}}</h6>
                                    <p class="position-company">{{$experience->company_name}}.</p>
                                    <p class="position-year">{{$experience->workStart_month}} {{$experience->workStart_year}} - {{($experience->currentWorkStatus == 1 ? '- present': $experience->workEnd_month .'  '.$experience->workEnd_year)}}&nbsp;-&nbsp;
                                        <a class="editexperiencepop" data-toggle="modal" data-target="#editExperience" data-id="{{$experience->id}}" data-title="{{$experience->job_title}}" data-company="{{$experience->company_name}}"
                                        data-workstartmonth="{{$experience->workStart_month}}" data-workendmonth="{{$experience->workEnd_month}}" data-workstartyear="{{$experience->workStart_year}}"
                                        data-workendyear="{{$experience->workEnd_year}}" data-description="{{$experience->description}}" data-current="{{$experience->currentWorkStatus}}">Edit</a>
                                    </p>
                                </div><!-- media-body -->

                            </div><!-- media -->
                            @endforeach
                            @endif
                            </div>
                        </div><!-- card-body -->
                        <div class="card-footer">
                            <a href="#">{{--Show more<span class="d-none d-sm-inline"> experiences (4)</span>--}} <i class="fa fa-angle-down"></i></a>
                            <a data-toggle="modal" data-target="#experienceInfoModal" href="javascript:void(0)">Add new</a>
                        </div><!-- card-footer -->
                    </div><!-- card -->

                    <div class="card card-experience mg-t-20">
                        <div class="card-body">
                            <div class="slim-card-title">Education</div>
                            <div class="takemediaEdu">
                            @if(count(Auth::user()->education) > 0)
                            @foreach(Auth::user()->education as $education)
                            <div class="media" id="edu{{$education->id}}">
                                <div class="experience-logo">
                                    <i class="fa fa-briefcase"></i>
                                </div><!-- experience-logo -->
                                <div class="media-body">
                                    <h6 class="position-name">{{$education->fieldOfStudy}}</h6>
                                    <p class="position-company">{{$education->school_name}}</p>
                                    <p class="position-year">{{$education->schoolStart_month}} {{$education->schoolStart_year}} - {{$education->schoolEnd_month}} {{$education->schoolEnd_year}}&nbsp;-&nbsp;
                                        <a class="editEducationpop" data-toggle="modal" data-target="#editEducation" data-school="{{$education->school_name}}"
                                        data-schoolstartmonth="{{$education->schoolStart_month}}" data-schoolstartyear="{{$education->schoolStart_year}}"
                                        data-schoolendmonth="{{$education->schoolEnd_month}}" data-schoolendyear="{{$education->schoolEnd_year}}" data-id="{{$education->id}}"
                                        data-country="{{$education->schoolCountry_id}}" data-state="{{$education->schoolState_id}}" data-field="{{$education->fieldOfStudy}}"
                                        data-educationlevel="{{$education->educationLevel}}">Edit</a>
                                    </p>
                                </div><!-- media-body -->
                            </div><!-- media -->
                            @endforeach
                            @endif
                            </div>
                        </div><!-- card-body -->
                        <div class="card-footer">
                            <a href="#"> <i class="fa fa-angle-down"></i></a>
                            <a data-toggle="modal" data-target="#educationInfoModal" href="javascript:void(0)">Add new</a>
                        </div><!-- card-footer -->
                    </div><!-- card -->

                    {{--<div class="card card-recommendation mg-t-20">
                        <div class="card-body pd-25">
                            <div class="slim-card-title">Recommendations</div>
                            <div class="media align-items-center mg-y-25">
                                <img src="../img/img3.jpg" class="wd-40 rounded-circle" alt="">
                                <div class="media-body mg-l-15">
                                    <h6 class="tx-14 mg-b-2"><a href="#">Rolando Paloso</a></h6>
                                    <p class="mg-b-0">Head Architect</p>
                                </div><!-- media-body -->
                                <span class="tx-12">Nov 20, 2017</span>
                            </div><!-- media -->

                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                            <p class="mg-b-0">Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.</p>
                        </div><!-- card-body -->

                        <div class="card-footer pd-y-12 pd-x-25 bd-t bd-gray-300">
                            <a href="#">More recommendations (2) <i class="fa fa-angle-down"></i></a>
                        </div><!-- card-footer -->
                    </div>--}}
                </div><!-- col-8 -->

                <div class="col-lg-4 mg-t-20 mg-lg-t-0">
                    <div class="card card-connection">
                        <div class="row row-xs">
                            <div class="col-4 tx-primary">..0</div>
                            <div class="col-8">people viewed your profile in the past 90 days</div>
                        </div><!-- row -->
                        <hr>

                    </div><!-- card -->


                    <div class="card pd-25 mg-t-20">
                        <div class="slim-card-title">Contact</div>

                        <div class="media-list mg-t-25">
                            {{--<div class="media">
                                <div><i class="icon ion-link tx-24 lh-0"></i></div>
                                <div class="media-body mg-l-15 mg-t-4">
                                    <h6 class="tx-14 tx-gray-700">Websites</h6>
                                    <a href="#" class="d-block">http://themepixels.me</a>
                                    <a href="#" class="d-block">http://themeforest.net</a>
                                </div><!-- media-body -->
                            </div>--}}<!-- media -->
                            <div class="media">
                                <div><i class="icon ion-ios-telephone-outline tx-24 lh-0"></i></div>
                                <div class="media-body mg-l-15 mg-t-4">
                                    <h6 class="tx-14 tx-gray-700">Phone Number</h6>
                                    <span class="d-block">{{(Auth::user()->personalInfo) ? Auth::user()->personalInfo->phoneNumber : ''}}</span>
                                </div><!-- media-body -->
                            </div><!-- media -->
                            <div class="media mg-t-25">
                                <div><i class="icon ion-ios-email-outline tx-24 lh-0"></i></div>
                                <div class="media-body mg-l-15 mg-t-4">
                                    <h6 class="tx-14 tx-gray-700">Email Address</h6>
                                    <span class="d-block dispEmail">{{(Auth::user() ? Auth::user()->email : '')}}</span>
                                </div><!-- media-body -->
                            </div><!-- media -->
                        </div><!-- media-list -->
                    </div><!-- card -->
                </div><!-- col-4 -->
            </div>
        </div>
        @include('includes.modals.modals')
    </div>
    @include('includes.modals.editCandidateModals')
    @include('includes.alerts')
@endsection
@section('script')
    <script>
        $.ajaxSetup({
            headers:
                {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });
    </script>
    <script src="{{asset('js/updateUserInfo.js')}}"></script>
    <script src="{{asset('js/candidateInfo.js')}}"></script>
    <script src="{{asset('js/formSubmission.js')}}"></script>
    <script>
        generateYears();
        populateMonth();
        populateCountry();
        populateState($('select[name=stateoforigin]'));
        populateState($('select[name=stateofresidence]'));
        populateState($('select[name=schoolState]'));
        
        $('.editexperiencepop').on('click', function () {
            let ths = $(this)
            $('.updateExp').find('input[name=theId]').val($(this).data('id'));
            $('.updateExp').find('input[name=companyName]').val($(this).data('company'));
            $('.updateExp').find('input[name=jobTitle]').val($(this).data('title'));
            $('.updateExp').find('input[name=jobDescription]').val($(this).data('description'));

            if($(this).data('current') == 1){
                $('.updateExp').find('select[name=workEndYear] option').filter('[value=select]')
                    .prop('selected', true);
                $('.updateExp').find('select[name=workEndMonth] option').filter('[value=select]')
                    .prop('selected', true);
                $('.updateExp').find('input[name=currentWorkPlace]').prop('checked', true);
                $('.updateExp').find('select[name=workEndMonth]').attr('disabled', true);
                $('.updateExp').find('select[name=workEndYear]').attr('disabled', true);

            }else{
                $('.updateExp').find('input[name=currentWorkPlace]').prop('checked', false);
                $('.updateExp').find('select[name=workEndMonth]').attr('disabled', false);
                $('.updateExp').find('select[name=workEndYear]').attr('disabled', false);
                $('.updateExp').find('select[name=workEndYear] option').each(function () {
                    if($(this).text() == ths.data('workendyear')){
                        $(this).attr('selected', 'selected');
                    }
                });
                $('.updateExp').find('select[name=workEndMonth] option').each(function () {
                    if($(this).text() == ths.data('workendmonth')){
                        $(this).attr('selected', 'selected');
                    }
                });
            }

            $('.updateExp').find('select[name=workStartMonth] option').each(function () {
                 if($(this).text() == ths.data('workstartmonth')){
                     $(this).attr('selected', 'selected');
                 }
             });

            $('.updateExp').find('select[name=workStartYear] option').each(function () {
                if($(this).text() == ths.data('workstartyear')){
                    $(this).attr('selected', 'selected');
                }
            });

        });

        $('.editEducationpop').on('click', function () {
            let ths = $(this)
            $('input[name=eduId]').val($(this).data('id'));
            $('.updatedu').find('input[name=nameOfSchool]').val($(this).data('school'));
            $('.updatedu').find('input[name=studyField]').val($(this).data('field'));


            if($(this).data('current') == 1){
                $('.updatedu').find('select[name=schoolEndYear] option').filter('[value=select]')
                    .prop('selected', true);
                $('.updatedu').find('select[name=schoolEndMonth] option').filter('[value=select]')
                    .prop('selected', true);
                $('.updatedu').find('input[name=currentStudyInstitution]').prop('checked', true);
                $('.updatedu').find('select[name=schoolEndMonth]').attr('disabled', true);
                $('.updatedu').find('select[name=schoolEndYear]').attr('disabled', true);

            }else{
                $('.updatedu').find('input[name=currentStudyInstitution]').prop('checked', false);
                $('.updatedu').find('select[name=schoolEndMonth]').attr('disabled', false);
                $('.updatedu').find('select[name=schoolEndYear]').attr('disabled', false);
                $('.updatedu').find('select[name=schoolEndYear] option').each(function () {
                    if($(this).text() == ths.data('schoolendyear')){
                        $(this).attr('selected', 'selected');
                    }
                });
                $('.updatedu').find('select[name=schoolEndMonth] option').each(function () {
                    if($(this).text() == ths.data('schoolendmonth')){
                        $(this).attr('selected', 'selected');
                    }
                });
            }

            $('.updatedu').find('select[name=schoolStartMonth] option').each(function () {
                if($(this).text() == ths.data('schoolstartmonth')){
                    $(this).attr('selected', 'selected');
                }
            });

            $('.updatedu').find('select[name=schoolStartYear] option').each(function () {
                if($(this).text() == ths.data('schoolstartyear')){
                    $(this).attr('selected', 'selected');
                }
            });

            $('.updatedu').find('select[name=schoolCountry] option').each(function () {
                if($(this).val() == ths.data('country')){
                    $(this).attr('selected', 'selected');
                }
            });

            $('.updatedu').find('select[name=schoolState] option').each(function () {
                if($(this).val() == ths.data('state')){
                    $(this).attr('selected', 'selected');
                }
            });

            $('.updatedu').find('select[name=educationLevel] option').each(function () {
                if($(this).text() == ths.data('educationlevel')){
                    $(this).attr('selected', 'selected');
                }
            });
        });
    </script>
@endsection