@extends('Layouts.actualJobLayout')
@section('content')
    <div class="mt-2 container">@include('includes.messages')</div>
    <div id="msg"></div>
    <div class="slim-mainpanel">
        <div class="container pd-t-50">
            <div class="row mt-3 mb-4">
                <button class="btn btn-danger" style="margin-left: 90px" onclick="back()">Back</button>
            </div>
            @if(!$expired)
            <div class="row">
                <div class="col-lg-10 offset-lg-1 card card-body bg-danger">
                    {{--<h3 class="tx-inverse mg-b-15">Hi, {{Auth::user()->firstname}}!</h3>--}}
                    {{--<p class="mg-b-40">.</p>--}}

                    {{--<h2 class="header-skin-body">{{$job->title}} at {{$job->user->firstname}}</h2>--}}
                    <h2 class="header-skin-body text-white">{{$job->title}} - {{strtoupper($job->job_id)}} </h2>
                    <button id="upapply" class="btn btn-outline-amber" style="float: right !important; color: white!important;">Apply</button>
                    {{--<div class="row no-gutters">
                        <div class="col-sm-6">
                            <div class="card card-earning-summary">
                                <h6>Profile Views</h6>
                                <h1>0</h1>
                                <span>Profile Views</span>
                            </div><!-- card -->
                        </div><!-- col-6 -->
                        <div class="col-sm-6">
                            <div class="card card-earning-summary mg-sm-l--1 bd-t-0 bd-sm-t">
                                <h6>Rating</h6>
                                <h1>...</h1>
                                <span>Rating</span>
                            </div><!-- card -->
                        </div><!-- col-6 -->
                    </div><!-- row -->--}}
                </div><!-- col-6 -->
                <div class="col-lg-10 offset-lg-1 mg-t-20 mg-sm-t-30 mg-lg-t-0">
                    {{--<ul class="nav nav-activity-profile mg-t-20">
                        @if(\Illuminate\Support\Facades\Auth::user()->personalInfo_filled == 0)
                            <li class="pinfo" data-toggle="modal" data-target="#personalInfoModal" ><a href="javascript:void(0)" class="nav-link"><i class="icon ion-ios-redo tx-purple"></i> Personal Info</a></li>
                        @endif
                        @if(\Illuminate\Support\Facades\Auth::user()->educationInfo_filled == 0)
                            <li class="nav-item einfo"><a href="{{route('educationDetails')}}" class="nav-link"><i class="icon ion-image tx-primary"></i> Education </a></li>
                        @endif
                        @if(\Illuminate\Support\Facades\Auth::user()->experienceInfo_filled == 0)
                            <li class="nav-item exinfo"><a href="{{route('experienceDetails')}}" class="nav-link"><i class="icon ion-document-text tx-success"></i> Experience </a></li>
                        @endif
                    </ul>--}}

                    @if($job)
                            <div class="mg-t-15">
                                <div class="card card-body">
                                    <h6 class="slim-card-title">{{$job->title}}</h6>
                                    <b class="mg-t-30" style="color: black"><i>Description</i></b>
                                    <div class="mg-4">{!!  $job->description !!}</div>
                                    <p style="color: black">Deadline: {{date('jS F, Y', strtotime($job->deadline))}}</p>
                                    <div><p class="float-left mg-t-10">Posted on {{date('jS F, Y', strtotime($job->created_at))}}</p>
                                        {{--@if($applyStatus == 0)--}}
                                            <button data-jobId="{{$job->id}}" class="btn btn-danger float-right" id="apply" {{(date('Y-m-d') > $job->deadline ? 'disabled' : '')}}>{{(date('Y-m-d') > $job->deadline ? 'No longer open' : 'Apply to Job')}}</button></div>
                                        {{--@else--}}
                                            {{--<div class="alert alert-info float-right">You already applied for this job. | Status:{{ ($job->acceptance_status == 0 ? 'pending' : 'In-view')}}</div>--}}
                                        {{--@endif--}}
                                </div><!-- card -->
                            </div>
                    @endif

                </div>
            </div><!-- row -->
            <div class="clearfix"></div>
            @if(date('Y-m-d') < $job->deadline)
            <div class="row" id="applytoJob">
            <div class="col-lg-10 offset-lg-1 section-wrapper mg-t-30">
                <label class="section-title">FILL YOUR INFORMATION.</label>
                <p class="mg-b-20 mg-sm-b-40 text-danger">Please note the asterisked fields are COMPULSORY to fill .</p>
                <form enctype="multipart/form-data" method="post" id="newapplication">
                <div class="row mb-5 personalInfo">
                    <label class="col-sm-4 form-control-label section-title"><span class="tx-danger"></span>PERSONAL INFORMATION</label>
                    <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                        <input type="hidden" name="id" value="{{$job->id}}">
                        <input type="hidden" name="jobid" value="{{$jobId}}">
                        <div class="form-group">
                            <label>Firstname<em style="color: red">*</em></label>
                            <input type="text" name="firstname" class="form-control" placeholder="Enter firstname">
                        </div>
                        <div class="form-group">
                            <label>Middlename</label>
                            <input type="text" name="middlename" class="form-control" placeholder="Enter Middlename">
                        </div>
                        <div class="form-group">
                            <label>Lastname<em style="color: red">*</em></label>
                            <input type="text" name="lastname" class="form-control" placeholder="Enter Lastname">
                        </div>
                        <div class="form-group" >
                            <label>Email<em style="color: red">*</em></label>
                            <input type="email" name="email" class="form-control" placeholder="Enter Email">
                        </div>
                        <div class="form-group" >
                            <label>Phone Number<em style="color: red">*</em></label>
                            <input type="text" name="phoneNumber" class="form-control" placeholder="Enter Phone Number">
                        </div>
                        <div class="form-group">
                            <label>Country<em style="color: red">*</em></label>
                            <select name="countryOfResidence" class="form-control">
                                <option></option>
                            </select>
                        </div>
                        <div class="form-group" >
                            <label>State<em style="color: red">*</em></label>
                            <select name="stateofresidence" class="form-control">
                                <option></option>
                            </select>
                        </div>
                    </div>
                </div>
                <hr>
                {{--<div class="row mt-5 education">
                    <label class="col-sm-4 form-control-label section-title"><span class="tx-danger"></span>Education</label>
                    <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                        <div id="educationDetails">
                            <div class="singleEducationDetails">
                                <div class="form-group">
                                    <label>School</label>
                                    <input type="text" name="nameOfSchool" class="form-control" placeholder="Enter name of school">
                                </div>
                                <div class="form-group" >
                                    <label>Country</label>
                                    <select name="schoolCountry" class="form-control">
                                        <option></option>
                                    </select>
                                </div>
                                <div class="form-group" >
                                    <label>State</label>
                                    <select name="schoolState" class="form-control">
                                        <option></option>
                                    </select>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group" >
                                            <label>From</label>
                                            <select name="schoolStartMonth" class="form-control">
                                                <option></option>
                                            </select>
                                            <select name="schoolStartYear" class="form-control">
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group" >
                                            <label>To</label>
                                            <select name="schoolEndMonth" class="form-control">
                                                <option></option>
                                            </select>
                                            <select name="schoolEndYear" class="form-control">
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group" >
                                        <label></label>
                                        <input name="currentStudyInstitution" type="checkbox">I currently study here
                                    </div>
                                </div>
                                <div class="form-group" >
                                    <label>Field of Study</label>
                                    <input type="text" name="studyField" placeholder="e.g. Sciences, Arts, Engineering, Pharmacy, " class="form-control">
                                </div>
                                <div class="form-group mb-4">
                                    <label>Level of Education</label>
                                    <select name="educationLevel" class="form-control">
                                        <option selected disabled>Select</option>
                                        <option>School Leaving Certificate</option>
                                        <option>Diploma</option>
                                        <option>Degree</option>
                                        <option>Graduate Diploma</option>
                                        <option>Second Degree</option>
                                        <option>Doctorate Degree</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row"><p class="float-right addEducationButton"><i class="fa fa-plus"></i> Add new Education</p></div>
                    </div>
                </div>
                <hr>--}}
                {{--<div class="row mt-5 experience">
                    <label class="col-sm-4 form-control-label section-title"><span class="tx-danger"></span>EXPERIENCE</label>
                    <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                        <div id="addExperience">
                            <div class="singleJobExperience">
                                <div class="form-group">
                                    <label>Company Name</label>
                                    <input type="text" name="companyName" class="form-control" placeholder="Company Name">
                                </div>
                                <div class="form-group">
                                    <label>Job Title</label>
                                    <input type="text" name="jobTitle" class="form-control" placeholder="Role">
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group" >
                                            <label></label>
                                            <input type="checkbox" name="currentWorkPlace">I currently work here
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group" >
                                            <label>From</label>
                                            <select name="workStartMonth" class="form-control">
                                                <option></option>
                                            </select>
                                            <select name="workStartYear" class="form-control">
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group" >
                                            <label>To</label>
                                            <select name="workEndMonth" class="form-control">
                                                <option></option>
                                            </select>
                                            <select name="workEndYear" class="form-control">
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mb-4">
                                    <label>Description</label>
                                    <textarea rows="5" name="jobDescription" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row"><p class="float-right addExperienceButton"><i class="fa fa-plus"></i> Add new job Experience</p></div>
                    </div>
                </div>--}}
                <div class="row mt-5 document">
                    <label class="col-sm-4 form-control-label section-title"><span class="tx-danger"></span>UPLOAD RESUME</label>
                    <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                        <div>
                            <div class="form-group">
                                <label>Cover Statement</label>
                                <textarea rows="6" class="form-control" name="cover"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Upload from your device<em style="color: red">*</em> (.doc*, .pdf*, .docx*)</label>
                                <input type="file" name="resume">
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="row mt-4 mb-4">
                        {{--<div class="col-md-4">--}}
                        <label class="col-sm-4 form-control-label section-title"><span class="tx-danger"></span></label>
                        <div class="col-sm-6 mg-t-10 mg-sm-t-0">
                            <button class="btn btn-primary bd-0 pull-right" style="float: right !important;" type="submit" id="submitIt">Submit Application</button>
                        </div>
                    </div>
            </form>

        </div>


        {{--<div class="row">
            <div class="section-wrapper mg-t-20">
                <label class="section-title">Personal Information</label>
                <p class="mg-b-20 mg-sm-b-40">A bordered form group wrapper with a label on top of each form control.</p>

                <div class="form-layout form-layout-2">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-control-label">Firstname: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="firstname" value="John Paul" placeholder="Enter firstname">
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-4 mg-t--1 mg-md-t-0">
                            <div class="form-group mg-md-l--1">
                                <label class="form-control-label">Lastname: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="lastname" value="McDoe" placeholder="Enter lastname">
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-4 mg-t--1 mg-md-t-0">
                            <div class="form-group mg-md-l--1">
                                <label class="form-control-label">Email address: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="email" value="johnpaul@yourdomain.com" placeholder="Enter email address">
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-md-8">
                            <div class="form-group bd-t-0-force">
                                <label class="form-control-label">Mail address: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="address" value="Market St., San Francisco" placeholder="Enter address">
                            </div>
                        </div><!-- col-8 -->
                        <div class="col-md-4">
                            <div class="form-group mg-md-l--1 bd-t-0-force">
                                <label class="form-control-label mg-b-0-force">Country: <span class="tx-danger">*</span></label>
                                <select id="select2-a" class="form-control select2-hidden-accessible" data-placeholder="Choose country" tabindex="-1" aria-hidden="true">
                                    <option label="Choose country"></option>
                                    <option value="USA" selected="">United States of America</option>
                                    <option value="UK">United Kingdom</option>
                                    <option value="China">China</option>
                                    <option value="Japan">Japan</option>
                                </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 302px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-select2-a-container"><span class="select2-selection__rendered" id="select2-select2-a-container" title="United States of America">United States of America</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                            </div>
                        </div><!-- col-4 -->
                    </div><!-- row -->
                    <div class="form-layout-footer bd pd-20 bd-t-0">
                        <button class="btn btn-primary bd-0">Submit Form</button>
                        <button class="btn btn-secondary bd-0">Cancel</button>
                    </div><!-- form-group -->
                </div><!-- form-layout -->
            </div>
        </div>--}}{{--
    </div>--}}<!-- container -->

        </div><!-- slim-mainpanel -->
            @endif
            @endif
        </div>
    </div>
    @include('includes.modals.modals')
    @include('includes.alerts')


@endsection
@section('script')
    <script>
        //alert();
        generateYears();
        populateMonth();
        populateCountry();
        populateState($('select[name=stateoforigin]'));
        populateState($('select[name=stateofresidence]'));
        populateState($('select[name=schoolState]'));

        $.ajaxSetup({
            headers:
                {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });

        function back() {
            window.history.back();
        }

        $('#apply').on('click', function () {
            $('html,body').animate({
                scrollTop: $("#applytoJob").offset().top
            }, 'slow');
        });


        $('#upapply').on('click', function () {
            $('html,body').animate({
                scrollTop: $("#applytoJob").offset().top
            }, 'slow');
        });


        /*$('#applyToJob').on('click', function () {
            let job = $(this).data('jobid');
            $.ajax({
                type: 'POST',
                url: "{{route('signifyInterest')}}",
                data: {job: job},
                //cache: false,
                beforeSend: function (){
                    $('#applyToJob').attr("disabled", "disabled");
                    $('.card').css("opacity", ".5");
                },
                success:function (data) {
                    console.log(data);
                    $('.card').css("opacity", "");
                    //ths.attr('disabled', false);
                    if(data.error){
                        $('#error').modal('toggle');
                        let msg = "<p>"+data.error+".</p>";
                        $('#message').html(msg);
                    }else if(data.success){
                        //$('.selectMultiple').prop('checked', '');
                        $('#success').modal('toggle');
                        let msg = "<p>"+data.success+"</p>";
                        $('#successmsg').html(msg);
                        //setTimeout(function (){ location.reload(); }, 1200);
                    }else if(data.logout){
                        location.href = "/sign-in"
                    }
                },
                error: function (error){
                    console.log(error);
                    $('#error').modal('toggle');
                    let msg = "<p>"+error.error+".</p>";
                    $('#message').html(msg);
                    //$('#recommendToAdmin').attr('disabled', false);
                    $('.card').css("opacity", "");
                }
            });
        });*/

    </script>
    {{--<script src="{{asset('js/formSubmission.js')}}"></script>--}}
    <script>

        $('form#newapplication').on('submit', function (e) {
            e.preventDefault();
            let firstname = $('.personalInfo').find('input[name=firstname]').val();
            let lastname = $('.personalInfo').find('input[name=lastname]').val();
            let email = $('.personalInfo').find('input[name=email]').val();
            let phoneNumber = $('.personalInfo').find('input[name=phoneNumber]').val();
//        let country = $('.personalInfo').find('select[name=country]').val();
//        let stateOfOrigin = $('.personalInfo').find('select[name=stateoforigin]').val();
            let countryOfResidence = $('.personalInfo').find('select[name=countryOfResidence]').val();
            let stateOfResidence = $('.personalInfo').find('select[name=stateofresidence]').val();

            let cover = $('textarea[name=cover]').val();


            if (firstname == null || lastname == null || email == "" || phoneNumber == null  || stateOfResidence == null
                || countryOfResidence == null) {
                return alert('Oppps! one or more asterisked field is not filled in the personal information section');
            }

            if ($('input[name=resume]')[0].files.length <=  0) {
                return alert('Please attach your resume.');
            }

            let formData = new FormData(this);

            $.ajax({
                url: '{{route("submitapplication")}}',
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    console.log(formData);
                    $('#submitIt').attr("disabled", "disabled");
                    $('#submitIt').text("...Processing");
                    $('#newapplication').css("opacity", ".5");
                },
                success: function (response) {
                    console.log(response);
                    $('#submitIt').text("Submit Application");
                    //$('#msgg').empty();
                    if (response.success) {
                        $('#success').modal('toggle');
                        let msg = "<p>"+response.message+"</p>";
                        $('#submitIt').removeAttr("disabled");
                        $('#newapplication').css("opacity", ".5");
                        document.getElementById('newapplication').reset();
                        //$(window).scrollTop(0);
                        location.href = "{{route('applicationSubmitted')}}";
                    }else if(response.errors){
                        $('#error').modal('toggle');
                        let msg = "<p>Please fill compulsory fields.</p>";
                        $('#message').html(msg);
                        $('#submitIt').removeAttr("disabled");
                        $('#newapplication').css("opacity", "");
                        //$(window).scrollTop(0);
                    } else if(response.error){
                        $('#error').modal('toggle');
                        let msg = "<p>"+response.error+".</p>";
                        $('#message').html(msg);
                        $('#submitIt').removeAttr("disabled");
                        $('#newapplication').css("opacity", "");
                        //$(window).scrollTop(0);
                    }else {
                        $('#msgg').empty();
                        $('#msgg').append('<div class="alert alert-danger">There was difficulty submitting, try again. </div>');
                        $('#submitIt').removeAttr("disabled");
                        $('#newapplication').css("opacity", "");
                        //$(window).scrollTop(0);
                    }
                },
                error : function (error) {
                    $('#msgg').empty();
                    $('#msgg').append('<div class="alert alert-danger">There was difficulty submitting, try again. </div>');
                    $('#submitIt').removeAttr("disabled");
                    $('#newapplication').css("opacity", "");
                }
            });
        });

    </script>
@endsection