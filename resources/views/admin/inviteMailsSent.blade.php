@extends('Layouts.adminDashboardLayout')
@section('content')
    <div class="mt-2 container">@include('includes.messages')</div>
    <div class="slim-mainpanel">
        <div class="container pd-t-50">
            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Openings</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Mails</li>
                </ol>
                <h6 class="slim-pagetitle">Sent Invitations Mails</h6>
            </div>
            {{--<div class="card mg-b-60">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2">
                            <label>Country</label>
                            <select class="form-control">
                                <option>All</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label>Region</label>
                            <select class="form-control">
                                <option>All</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label>Age From</label>
                            <select class="form-control">
                                <option>All</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label>Age To</label>
                            <select class="form-control">
                                <option>All</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md text-center"><button class="btn btn-success">Filter</button></div>
                    </div>
                </div>
            </div>--}}
            {{--<div class="row">
                <button class="btn btn-info newjob" data-toggle="modal" data-target="#addjobOpenings">Create an Opening</button>
            </div>--}}
            <div class="row">
                @php  $counter = 0 @endphp
                <table id="loadSudents" class="table display responsive table-responsive" role="grid" aria-describedby="datatable1_info" style="width:100%">
                    <thead>
                    <tr role="row">
                        <th>Title</th>
                        <th>Message</th>
                        <th>Sender</th>
                        <th>Recipient</th>
                        <th>Job</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($mails as $mail)
                        @php $counter = $counter+1 @endphp
                        <tr role="row">
                            <td>{{$mail->title}}</td>
                            <td>{!! $mail->message !!}</td>
                            <td>{{$mail->sender}}</td>
                            <td>{{$mail->recipient}}</td>
                            <td>{{$mail->job->job_tile}}</td>
                            <td> </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="col-md">
                    {{--<button class="btn btn-success">Approve</button> --}}
                </div>
            </div>
        </div>
    </div>


    @include('includes.alerts')
@endsection
@section('script')
    <script>

        $.ajaxSetup({
            headers:
                {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });

        let table = $('#loadSudents').DataTable({
            responsive: true,
            "scrollX": true,
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ items/page',
            }
        });

        /*$('.newjob').on('click',function(){
         var id = $(this).data('id');
         $('#theClientId').val(id);
         });*/

        tinymce.init({
            selector: '#jobDescription'
        });


    </script>
@endsection