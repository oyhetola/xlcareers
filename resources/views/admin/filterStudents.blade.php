@extends('Layouts.adminDashboardLayout')
@section('content')
    <div class="mt-2 container">@include('includes.messages')</div>
    <div class="slim-mainpanel">
        <div class="container pd-t-50">
            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">All Candidates</li>
                </ol>
                <h6 class="slim-pagetitle">All Candidates</h6>
            </div>
                <div class="card mg-b-60">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-2">
                                <label>Country</label>
                                <select class="form-control" name="country">
                                    <option>All</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label>Region</label>
                                <select class="form-control" name="region">
                                    <option>All</option>
                                    @foreach($regions as $region)
                                        <option value="{{$region->id}}">{{$region->region}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label>Age From</label>
                                <select class="form-control" name="ageFrom">
                                    <option>All</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label>Age To</label>
                                <select class="form-control" name="ageTo">
                                    <option>All</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label>State</label>
                                <select class="form-control" name="state">
                                    <option>All</option>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label>Discipline</label>
                                <input class="form-control" name="discipline" id="search">
                                <div id="loadResult" style="position: absolute; z-index: 100;"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md text-center"><button class="btn btn-success" id="filterCandidates">Filter</button></div>
                        </div>
                    </div>
                </div>

            <div class="row">
                <table id="loadStudents" class="table display responsive nowrap dataTable no-footer dtr-inline" role="grid" aria-describedby="datatable1_info" style="width: 1026px;">
                    <thead>
                    <tr role="row">
                        <th>First name</th>
                        <th>Last name</th>
                        <th>Age</th>
                        <th>Action</th>
                        <th>Multiple</th>
                    </tr>
                    </thead>
                    <tbody>
                        {{--<tr role="row">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><button class="btn btn-sm btn-info">View Profile</button> <button class="btn btn-sm btn-info">CV</button> <button class="btn btn-sm btn-info">Send Message</button></td>
                            <td><input type="checkbox" class="selectMultiple"></td>
                        </tr>--}}
                    </tbody>
                </table>
            </div>
            <div class="row"><button id="" class="btn btn-info sendmsg" data-toggle="modal" data-target="#sendMessage">Send Message</button></div>
        </div>
    </div>
    @include('includes.alerts')
@endsection
@section('script')

    <script src="{{asset('js/filterFetch.js')}}"></script>
    <script>
        $.ajaxSetup({
            headers:
                {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });


        $('#loadSudents').DataTable({
            responsive: true,
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ items/page',
            }
        });

        $('select[name=country]').on('change', function () {
            if($('select[name=country] option:selected').text() == "Nigeria"){
                populateState($('select[name=state]'));
            }else{
                $('select[name=state]').html('<option>All</option>');
            }
        });


        $('#search').on('keyup',function() {

            $('#searchres').empty();
            if ($(this).val() != '') {
                var searchKey = $(this).val();
                $.post('{{route("findDiscipline")}}', {searchKey: searchKey}, function (data) {
                    console.log(data);
                    if(data != "Please log out"){
                        if(data.searchResults.length !== 0){
                            $('#loadResult').empty();
//                            $('#loadResult').append('<p><b>Search Results</b></p>');
                            for(var i = 0; i<data.searchResults.length; i++){
                                $('#loadResult').append('<div class="actualResult" style="background: #fcfcfc; padding: 3px 5px; border-bottom: 1px solid grey">'+data.searchResults[i].fieldOfStudy+'</div>')
                            }
                        }else{
                            $('#loadResult').empty();
                            //$('#searchres').empty();
                            $('#loadResult').append('<li class="k s">No result(s) found</li>');
                        }
                    }else{
                        location.href = '/';
                    }
                });
            }else{
                //exitSearch();
            }
        });


        $(document).on('click', '.actualResult', function () {
            let res = $(this).text();
            $('#search').val(res);
            $('#search').text(res);
            $('#loadResult').empty();
        });

        $('#search').focusout(function () {
            setTimeout(function () {
                $('#loadResult').empty();
            }, 1000)
        });
    </script>
@endsection