@extends('Layouts.adminDashboardLayout')
@section('content')
    <div class="mt-2 container">@include('includes.messages')</div>
    <div id="msg"></div>
    <div class="slim-mainpanel">
        <div class="container pd-t-50">
            <div><button class="btn btn-sm btn-amber pull-right" id="goback">Back</button></div>
            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    @if(Auth::user()->user_role== "admin")
                    <li class="breadcrumb-item"><a href="{{route('adminDashboard')}}">Dashboard</a></li>
                    @else
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                    @endif
                    {{--<li class="breadcrumb-item"><a href="#">Pages</a></li>--}}
                    <li class="breadcrumb-item active" aria-current="page">Profile Page</li>
                </ol>
                <h6 class="slim-pagetitle">{{$candidate->firstname}} {{$candidate->lastname}}'s Profile</h6>
            </div>

            <div class="row row-sm">
                <div class="col-lg-8">
                    <div class="card card-profile">
                        <div class="card-body">
                            <div class="media">
                                <img src="../img/user-image.png" alt="">
                                <div class="media-body">
                                    <h3 class="card-profile-name">{{$candidate->firstname}} {{$candidate->lastname}}</h3>
                                    {{--<p class="card-profile-position">Executive Director at <a href="#">ThemePixels, Inc.</a></p>--}}
                                    {{--<p>San Francisco, California</p>--}}

                                    <p class="mg-b-0">About {{$candidate->firstname}}<a href="#"></a></p>
                                </div><!-- media-body -->
                            </div><!-- media -->
                        </div><!-- card-body -->
                        {{--<div class="card-footer">
                            --}}{{--<a href="#" class="card-profile-direct">http://thmpxls.me/profile?id=katherine</a>--}}{{--
                            <div>
                                <a href="#">Edit Profile</a>
                                <a href="#">Profile Settings</a>
                            </div>
                        </div><!-- card-footer -->--}}
                    </div><!-- card -->

                {{--<ul class="nav nav-activity-profile mg-t-20">
                    <li class="nav-item"><a href="#" class="nav-link"><i class="icon ion-ios-redo tx-purple"></i> Share an update</a></li>
                    <li class="nav-item"><a href="#" class="nav-link"><i class="icon ion-image tx-primary"></i> Publish photo</a></li>
                    <li class="nav-item"><a href="#" class="nav-link"><i class="icon ion-document-text tx-success"></i> Add an article</a></li>
                </ul>--}}<!-- nav -->



                    <div class="card card-experience mg-t-20">
                        <div class="card-body">
                            <div class="slim-card-title">Work Experience</div>
                            @foreach($candidate->experience as $experience)
                                <div class="media">
                                    <div class="experience-logo">
                                        <i class="icon ion-briefcase"></i>
                                    </div><!-- experience-logo -->

                                    <div class="media-body">
                                        <h6 class="position-name">{{$experience->job_title}}</h6>
                                        <p class="position-company">{{$experience->company_name}}.</p>
                                        <p class="position-year">{{$experience->workStart_month}} {{$experience->workStart_year}} - {{($experience->currentWorkStatus == 1 ? '- present': $experience->workEnd_month .'  '.$experience->workEnd_year)}}&nbsp;-&nbsp;
                                            {{--<a href="#">Edit</a>--}}
                                        </p>
                                    </div><!-- media-body -->

                                </div><!-- media -->
                            @endforeach
                        </div><!-- card-body -->
                        {{--<div class="card-footer">
                            <a href="#">--}}{{--Show more<span class="d-none d-sm-inline"> experiences (4)</span>--}}{{-- <i class="fa fa-angle-down"></i></a>
                            <a href="#">Add new</a>
                        </div><!-- card-footer -->--}}
                    </div><!-- card -->

                    <div class="card card-experience mg-t-20">
                        <div class="card-body">
                            <div class="slim-card-title">Education</div>
                            @foreach($candidate->education as $education)
                                <div class="media">
                                    <div class="experience-logo">
                                        <i class="icon ion-briefcase"></i>
                                    </div><!-- experience-logo -->
                                    <div class="media-body">
                                        <h6 class="position-name">{{$education->fieldOfStudy}}</h6>
                                        <p class="position-company">{{$education->school_name}}</p>
                                        <p class="position-year">{{$education->schoolStart_month}} {{$education->schoolStart_year}} - {{$education->schoolEnd_month}} {{$education->schoolEnd_year}}&nbsp;-&nbsp;
                                            {{--<a href="#">Edit</a>--}}
                                        </p>
                                    </div><!-- media-body -->
                                </div><!-- media -->
                            @endforeach
                        </div><!-- card-body -->
                       {{-- <div class="card-footer">
                            <a href="#"> <i class="fa fa-angle-down"></i></a>
                            <a href="#">Add new</a>
                        </div><!-- card-footer -->--}}
                    </div><!-- card -->

                    {{--<div class="card card-recommendation mg-t-20">
                        <div class="card-body pd-25">
                            <div class="slim-card-title">Recommendations</div>
                            <div class="media align-items-center mg-y-25">
                                <img src="../img/img3.jpg" class="wd-40 rounded-circle" alt="">
                                <div class="media-body mg-l-15">
                                    <h6 class="tx-14 mg-b-2"><a href="#">Rolando Paloso</a></h6>
                                    <p class="mg-b-0">Head Architect</p>
                                </div><!-- media-body -->
                                <span class="tx-12">Nov 20, 2017</span>
                            </div><!-- media -->

                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                            <p class="mg-b-0">Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.</p>
                        </div><!-- card-body -->

                        <div class="card-footer pd-y-12 pd-x-25 bd-t bd-gray-300">
                            <a href="#">More recommendations (2) <i class="fa fa-angle-down"></i></a>
                        </div><!-- card-footer -->
                    </div>--}}
                </div><!-- col-8 -->

                <div class="col-lg-4 mg-t-20 mg-lg-t-0">
                    <div class="card card-connection">
                        <div class="row row-xs">
                            {{--<div class="col-4 tx-primary">129</div>--}}
                            {{--<div class="col-8"></div>--}}
                            <div class="media-body mg-l-15 mg-t-4">
                                <h6 class="tx-14 tx-gray-700">Next of kin</h6>
                                <span class="d-block">{{$candidate->personalInfo->nextOfKin}}</span>
                            </div>
                            <div class="media-body mg-l-15 mg-t-4">
                                <h6 class="tx-14 tx-gray-700">Next of kin Phone Number</h6>
                                <span class="d-block">{{$candidate->personalInfo->nextOfKinPhoneNum}}</span>
                            </div>
                            <div class="media-body mg-l-15 mg-t-4">
                                <h6 class="tx-14 tx-gray-700">Next of kin Address</h6>
                                <span class="d-block">{{$candidate->personalInfo->nextOfKinAddress}}</span>
                            </div>
                        </div><!-- row -->
                        <hr>

                    </div><!-- card -->


                    <div class="card pd-25 mg-t-20">
                        <div class="slim-card-title">Contact</div>

                        <div class="media-list mg-t-25">
                        {{--<div class="media">
                            <div><i class="icon ion-link tx-24 lh-0"></i></div>
                            <div class="media-body mg-l-15 mg-t-4">
                                <h6 class="tx-14 tx-gray-700">Websites</h6>
                                <a href="#" class="d-block">http://themepixels.me</a>
                                <a href="#" class="d-block">http://themeforest.net</a>
                            </div><!-- media-body -->
                        </div>--}}<!-- media -->
                            <div class="media">
                                <div><i class="icon ion-ios-telephone-outline tx-24 lh-0"></i></div>
                                <div class="media-body mg-l-15 mg-t-4">
                                    <h6 class="tx-14 tx-gray-700">Phone Number</h6>
                                    <span class="d-block">{{$candidate->personalInfo->phoneNumber}}</span>
                                </div><!-- media-body -->
                            </div><!-- media -->
                            <div class="media mg-t-25">
                                <div><i class="icon ion-ios-email-outline tx-24 lh-0"></i></div>
                                <div class="media-body mg-l-15 mg-t-4">
                                    <h6 class="tx-14 tx-gray-700">Email Address</h6>
                                    <span class="d-block">{{$candidate->email}}</span>
                                </div><!-- media-body -->
                            </div><!-- media -->
                            <div class="media mg-t-25">
                                <div><i class="icon ion-ios-email-outline tx-24 lh-0"></i></div>
                                <div class="media-body mg-l-15 mg-t-4">
                                    <h6 class="tx-14 tx-gray-700">Residencial Address</h6>
                                    <span class="d-block">{{$candidate->personalInfo->address}}</span>
                                </div><!-- media-body -->
                            </div><!-- media -->
                        </div><!-- media-list -->
                    </div><!-- card -->
                </div><!-- col-4 -->
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $('#goback').click(function () {
            window.history.back();
        });
    </script>
@endsection