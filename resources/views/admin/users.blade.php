@extends('Layouts.adminDashboardLayout')
@section('content')
    <div class="slim-mainpanel">
        <div class="container">
            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashbaord</a></li>
                    <li class="breadcrumb-item active" aria-current="page">All Users</li>
                </ol>
                <h6 class="slim-pagetitle">All Users</h6>
            </div><!-- slim-pageheader -->

            <div class="section-wrapper">
                <label class="section-title">Users</label>
                {{--<p class="mg-b-20 mg-sm-b-40">Projects Have tasks contained in them.  The tasks are tracked </p>--}}
                <p><button class="btn btn-secondary" data-toggle="modal" data-target="#newUser">Add a new User</button> </p>

                <div class="table-wrapper">
                    <table id="datatable1" class="table display responsive nowrap">
                        <thead>
                        <tr>
                            <th class="wd-15p-force">Name</th>
                            <th class="wd-10p-force">Email</th>
                            <th class="wd-10p-force">Status</th>
                            <th class="wd-15p-force">Role</th>
                            <th class="wd-30p-force">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{$user->firstname}} {{$user->lastname}}</td>
                                <td>{{$user->email}}</td>
                                <td>@if($user->suspend == 1) Suspended @else Active @endif</td>
                                <td>{{$user->user_role}}</td>
                                <td>
                                    @if(Auth::id() != $user->id)
                                    <button href="#" class="btn btn-sm suspendUser {{($user->suspend == 0 ? 'btn-warning' : 'btn-success')}}" data-id="{{$user->id}}" title="{{($user->suspend == 0 ? 'Suspend' : 'Activate')}}" data-placement="top" data-toggle="tooltip">@if($user->suspend == 0)<i class="fas fa-ban"></i> Suspend @else <i class="fas fa-key"></i> Activate @endif </button> <buttton  class="btn btn-danger btn-sm deleteUser" title="Delete User" data-toggle="tooltip" data-placement="top" data-id="{{$user->id}}"><i class="fas fa-trash" ></i> Delete</buttton>
                                    @endif
                                </td>

                            </tr>
                        @endforeach
                        {{-- <buttton class="btn btn-info btn-sm">Mark as Completed</buttton>--}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{--Create task modal--}}
    <div id="newUser" class="modal fade" aria-hidden="true" style="">
        <div class="modal-dialog modal-dialog-vertical-center" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Add a new user</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body pd-25">
                    <h5 class="lh-3 mg-b-20"><a href="#" class="tx-inverse hover-primary">  <div id="taskName"></div></a></h5>
                    <div class="form-group">
                        <label>Firstname</label>
                        <input class="form-control" type="text" name="firstname" placeholder="Firstname">
                    </div>
                    <div class="form-group">
                        <label>Lastname</label>
                        <input class="form-control" type="text" name="lastname" placeholder="Lastame">
                    </div>
                    <div class="form-group">
                        <label>Official Email</label>
                        <input class="form-control" type="email" name="email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label>Select User Role</label>
                        <select class="form-control" name="role">
                            <option value="examiner">Examiner</option>
                            <option value="admin">Admin</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="addUser" class="btn btn-primary">Add User</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div>
    {{--create task modal end--}}
    @include('includes.alerts')
@endsection
@section('script')
    <script>
        $.ajaxSetup({
            headers:{'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')}
        });

        $('#datatable1').DataTable({
            responsive: true,
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ items/page',
            }
        });

        $('.suspendUser').on('click', function () {
            var id = $(this).data('id');
            if(confirm('Are you sure you want to perform this action on the user?')){
                $.ajax({
                    url: '{{route('suspendUser')}}',
                    method: 'PUT',
                    data: {id:id},
                    success: function(data) {
                        console.log(data);
                        if(data.success){
                            $('#success').modal('toggle');
                            let msg = "<p>You successfully suspended the user.</p>";
                            $('#successmsg').html(msg);
                            setTimeout(function(){
                                location.reload();
                            }, 800);
                        }else{
                            $('#errors').modal('toggle');
                            let msg = "<p>There was an errors creating performing this action.</p>";
                            $('#message').html(msg);
                        }
                    },
                    error: function (data) {
                        console.log(data);
                        $('#errors').modal('toggle');
                        let msg = "<p><b> Ooops! Something went wrong.</b></p>";
                        $('#message').html(msg);
                    }
                });
            }

        });

        $('.deleteUser').on('click', function () {
            var id = $(this).data('id');
            if (confirm('Are you sure you want to delete this user?')){
                $.ajax({
                    url: '{{route('deleteUser')}}',
                    method: 'PUT',
                    data: {id:id},
                    success: function(data) {
                        console.log(data);
                        if(data.success){
                            $('#success').modal('toggle');
                            let msg = "<p>You successfully deleted the user.</p>";
                            $('#successmsg').html(msg);
                            setTimeout(function(){
                                location.reload();
                            }, 800);
                        }else{
                            $('#errors').modal('toggle');
                            let msg = "<p>There was an errors creating performing this action.</p>";
                            $('#message').html(msg);
                        }
                    },
                    error: function (data) {
                        console.log(data);
                        $('#errors').modal('toggle');
                        let msg = "<p><b> Ooops! Something went wrong.</b></p>";
                        $('#message').html(msg);
                    }
                });
            }
        });

        $('#addUser').on('click', function () {
            let ths = $(this);
            let fname =$('input[name=firstname]').val();
            let lname = $('input[name=lastname]').val();
            let email = $('input[name=email]').val();
            let role = $('select[name=role] option:selected').val();
                ths.attr('disabled', true);
                $('#newUser').css('opacity', '.7');
                $.ajax({
                    url: '{{route('addUser')}}',
                    method: 'POST',
                    data: {fname:fname, lname:lname, email:email, role:role},
                    success: function(data) {
                        ths.attr('disabled', false);
                        $('#newUser').css('opacity', '');
                        $('input').val('');
                        console.log(data);
                        if(data.success){
                            $('#success').modal('toggle');
                            let msg = "<p>You successfully added d"+data.success.firstname+".</p>";
                            $('#successmsg').html(msg);
                            setTimeout(function(){
                                location.reload();
                            }, 800);
                        }else{
                            $('#errors').modal('toggle');
                            let msg = "<p>There was an errors creating performing this action.</p>";
                            $('#message').html(msg);
                        }
                    },
                    error: function (data) {
                        ths.attr('disbled', false);
                        console.log(data);
                        $('#errors').modal('toggle');
                        let msg = "<p><b> Ooops! Something went wrong.</b></p>";
                        $('#message').html(msg);
                    }
                });

        });
    </script>
@endsection
