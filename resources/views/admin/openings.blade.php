@extends('Layouts.adminDashboardLayout')
@section('content')
    <div class="mt-2 container">@include('includes.messages')</div>
    <div class="slim-mainpanel">
        <div class="container pd-t-50">
            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Clients</li>
                </ol>
                <h6 class="slim-pagetitle">Job Openings</h6>
            </div>
            {{--<div class="card mg-b-60">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2">
                            <label>Country</label>
                            <select class="form-control">
                                <option>All</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label>Region</label>
                            <select class="form-control">
                                <option>All</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label>Age From</label>
                            <select class="form-control">
                                <option>All</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label>Age To</label>
                            <select class="form-control">
                                <option>All</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md text-center"><button class="btn btn-success">Filter</button></div>
                    </div>
                </div>
            </div>--}}
            <div class="row">
                <button class="btn btn-info newjob" data-toggle="modal" data-target="#addjobOpenings">Create an Opening</button>
            </div>
            <div class="row">
                @php  $counter = 0 @endphp
                <table id="loadSudents" class="table display responsive table-responsive" role="grid" aria-describedby="datatable1_info" style="width:100%">
                    <thead>
                    <tr role="row">
                        <th>Title</th>
                        <th>Date</th>
                        <th>Job ID</th>
                        <th>End Date</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($openings as $opening)
                        @php $counter = $counter+1 @endphp
                        <tr role="row">
                            <td>{{$opening->title}}</td>
                            <td>{{$opening->created_at}}</td>
                            <td>{{strtoupper($opening->job_id)}}</td>
                            <td>{{$opening->deadline}}</td>
                            <td>@if($opening->status == 0) Active @else  Closed @endif</td>
                            <td> @if($opening->status == 0)<button class="btn btn-sm btn-info cls"  data-id="{{$opening->id}}">Close</button>@endif <a class="btn btn-sm btn-success" href="/apply?jobId={{$opening->job_id}}">View</a> <a class="btn btn-sm btn-primary" href="{{route('applicants', ['jobid'=>$opening->job_id])}}">Applicants</a> <input id="link{{$counter}}" type="text" class="url d-none" value="{{route('applyForJob', ['jobId'=>$opening->job_id])}}"> <button class="btn btn-sm btn-primary share" data-id="{{$counter}}"><i class="fa fa-copy"></i>Share</button><button data-id="{{$opening->id}}" class="btn btn-danger btn-sm del"><i class="fa fa-trash"></i></button></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="col-md">
                    {{--<button class="btn btn-success">Approve</button> --}}
                </div>
            </div>
        </div>
    </div>

    <div id="addjobOpenings" class="modal fade" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content tx-size-sm">
                <div class="modal-header pd-x-20">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Create an Opening </h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body pd-20">
                    <div class="row mt-2 mg-l-2">
                        <form method="post" id="jobopen" enctype="multipart/form-data">
                            {{--<input type="hidden" name="theClientId" id="theClientId">--}}
                           {{-- <div class="form-group">
                                <label>Company Name</label>
                                <input type="text" name="companyName" class="form-control" placeholder="Company Name">
                            </div>--}}
                            <div class="form-group">
                                <label>Job Title</label>
                                <input type="text" name="jobTitle" class="form-control" placeholder="Title">
                            </div>
                            <div class="form-group" >
                                <label>Deadline to conclude process</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="icon ion-calendar tx-16 lh-0 op-6"></i>
                                        </div>
                                    </div>
                                    <input type="text" id="deadline" name="deadline" class="form-control fc-datepicker" data-provide='datepicker' data-date-container='#addjobOpenings' placeholder="YYYY/MM/DD" >
                                </div>
                            </div>
                            <div class="form-group mb-4">
                                <label>A short summary of the role</label>
                                <textarea rows="3" name="summary" id="summary" class="form-control"></textarea>
                            </div>
                            <div class="form-group mb-4">
                                <label>Full Description of role</label>
                                <textarea rows="5" name="jobDescription" id="jobDescription" class="form-control"></textarea>
                            </div>
                        </form>
                    </div>
                </div><!-- modal-body -->
                <div class="modal-footer">
                    <button type="submit" id="submitJobOpening" class="btn btn-primary">Add</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div>
    @include('includes.alerts')
@endsection
@section('script')
    <script>

        $.ajaxSetup({
            headers:
                {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });

        let table = $('#loadSudents').DataTable({
            responsive: true,
            "scrollX": true,
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ items/page',
            }
        });

        /*$('.newjob').on('click',function(){
            var id = $(this).data('id');
            $('#theClientId').val(id);
        });*/

        tinymce.init({
            selector: '#jobDescription'
        });

        $('#submitJobOpening').on('click', function () {
            let ths = $(this);
            //e.preventDefault();
            //ths.d
//            let theClientId = $('input[name=theClientId]').val();
            let jobTitle = $('input[name=jobTitle]').val();
            let deadline = $('input[name=deadline]').val();
            tinyMCE.triggerSave();
            let jobDescription = $('textarea[name=jobDescription]').val();
            let summary = $('textarea[name=summary]').val();
            let formData = {jobTitle: jobTitle, deadline: deadline, jobDescription: jobDescription};
            console.log(formData);
            $.ajax({
                type: 'POST',
                url: "/submitJobOpening",
                data: {jobTitle: jobTitle, deadline: deadline, summary:summary, jobDescription: jobDescription},
                //cache: false,
                beforeSend: function (){
                    $('#submitJobOpening').attr("disabled", "disabled");
                    $('#jobopen').css("opacity", ".5");
                },
                success:function (data) {
                    console.log(data);
                    $('#submitJobOpening').removeAttr("disabled");
                    $('#jobopen').css("opacity", "");
                    //ths.attr('disabled', false);
                    if(data.error){
                        $('#error').modal('toggle');
                        let msg = "<p>"+data.error+".</p>";
                        $('#message').html(msg);
                    }else if(data.success){
                        $('#success').modal('toggle');
                        let msg = "<p>"+data.success+"</p>";
                        $('#successmsg').html(msg);
                        $('#jobopen input').val('');
                        $('#jobopen textarea').val('');
                        tinymce.activeEditor.setContent('');
                        setTimeout(function (){ location.reload(); }, 1200);
                    }
                },
                error: function (error){
                    console.log(error);
                    ths.attr('disabled', false);
                }
            });
        });

        $('.approveClient').on('click', function () {
            var id = $(this).data('id');
            $.ajax({
                url: '{{route('approveClient')}}',
                method: 'PUT',
                data: {id:id},
                beforeSend: function (){
                    $('.approveClient').attr("disabled", "disabled");
                },
                success: function(data) {
                    console.log(data);
                    if(data.success){
                        $('.approveClient').attr("disabled", false);
                        $('#success').modal('toggle');
                        let msg = "<p>You successfully approved the client. Client can now have access to perform their actions.</p>";
                        $('#successmsg').html(msg);
                        setTimeout(function(){
                            location.reload();
                        }, 1000);
                    }else{
                        $('#errors').modal('toggle');
                        let msg = "<p>There was an errors creating performing this action.</p>";
                        $('#message').html(msg);
                    }
                },
                error: function (data) {
                    console.log(data);
                    $('#errors').modal('toggle');
                    let msg = "<p><b> Ooops! Something went wrong.</b></p>";
                    $('#message').html(msg);
                }
            });
        });


        $('.suspendUser').on('click', function () {
            var id = $(this).data('id');
            $.ajax({
                url: '{{route('approveClient')}}',
                method: 'PUT',
                data: {id:id},
                success: function(data) {
                    console.log(data);
                    if(data.success){
                        $('#success').modal('toggle');
                        let msg = "<p>You successfully suspended the client.</p>";
                        $('#successmsg').html(msg);
                        setTimeout(function(){
                            location.reload();
                        }, 800);
                    }else{
                        $('#errors').modal('toggle');
                        let msg = "<p>There was an errors creating performing this action.</p>";
                        $('#message').html(msg);
                    }
                },
                error: function (data) {
                    console.log(data);
                    $('#errors').modal('toggle');
                    let msg = "<p><b> Ooops! Something went wrong.</b></p>";
                    $('#message').html(msg);
                }
            });
        });

        $(document).on('click', '.share', function () {
            let id  = $(this).data('id');
//            alert(id);
            let str = '#link'+id;
            $(str).removeClass('d-none');
            let copyText = document.getElementById('link'+id).select();
            //copyText.setSelectionRange(0, 99999);
            document.execCommand('copy');
            $('#success').modal('toggle');
            $(str).addClass('d-none');
            let msg = "<p>Link now copied. You can share to any platform.</p>";
            $('#successmsg').html(msg);

        });

        $('.cls').on('click', function(){
           let theButtonClicked = $(this);
           let id = theButtonClicked.data('id');
           if(confirm('Are you sure you want to CLOSE this opening?')){
               $.post('{{route('closeApplication')}}', {id:id}, function (response) {
                   if(response.success){
                       theButtonClicked.parent().parent().find('td:nth-child(5)').text('Closed');
                       theButtonClicked.addClass('d-none');
                       $('#success').modal('toggle');
                       let msg = "<p>You successfully closed the application.</p>";
                       $('#successmsg').html(msg);
                   }else{
                       $('#errors').modal('toggle');
                       let msg = "<p><b> Ooops! Something went wrong. Try gain.</b></p>";
                       $('#message').html(msg);
                   }
               })
           }

        });

        $('.del').on('click', function(){
            let theButtonClicked = $(this);
            let id = theButtonClicked.data('id');
            $.post('{{route('deleteOpening')}}', {id:id}, function (response) {
                if(response.success){
                    table.row(theButtonClicked.parents('tr')).remove().draw();
                    $('#success').modal('toggle');
                    let msg = "<p>You successfully deleted the opening.</p>";
                    $('#successmsg').html(msg);
                }else{
                    $('#errors').modal('toggle');
                    let msg = "<p><b> Ooops! Something went wrong. Try again.</b></p>";
                    $('#message').html(msg);
                }
            })
        });
    </script>
@endsection