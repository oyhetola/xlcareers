@extends('Layouts.adminDashboardLayout')
@section('content')
    <div class="mt-2 container">@include('includes.messages')</div>
    <div class="slim-mainpanel">
        <div class="container pd-t-50">
            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Applicants</li>
                </ol>
                <h6 class="slim-pagetitle">Applicants</h6>
                @if(count($applicants) > 0)Applicants for {{$applicants[0]->jobOpening->title}} - {{$applicants[0]->job_number}}@endif
            </div>
            {{--<div class="card mg-b-60">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2">
                            <label>Country</label>
                            <select class="form-control">
                                <option>All</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label>Region</label>
                            <select class="form-control">
                                <option>All</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label>Age From</label>
                            <select class="form-control">
                                <option>All</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label>Age To</label>
                            <select class="form-control">
                                <option>All</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md text-center"><button class="btn btn-success">Filter</button></div>
                    </div>
                </div>
            </div>--}}
            <div class="row">
                <button class="btn btn-info" onclick="back()">Back</button>
                @if(count($applicants) > 0)<button class="btn btn-warning float-right" id="rmail" data-number="{{$applicants[0]->job_id}}" title="Send rejection mails to univited candidates">Send rejection mails</button>@endif
            </div>
            <div class="row">
                <table id="loadSudents" class="table display responsive table-responsive" role="grid" aria-describedby="datatable1_info" style="width: 1220px">
                    <thead>
                    <tr role="row">
                        <th>Country</th>
                        <th>State</th>
                        <th>Email</th>
                        <th>Date Applied</th>
                        <th>Name[First *Middle*  Last]</th>
                        <th>Resume</th>
                        <th>Cover note</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($applicants as $applicant)
                        <tr>
                            <td>{{$applicant->country->country}}</td>
                            <td>{{$applicant->state->state}}</td>
                            <td>{{$applicant->email}}</td>
                            <td>{{$applicant->created_at}}</td>
                            <td>{{ $applicant->firstname.' '.$applicant->middlename.' '.$applicant->lastname}}</td>
                            <td><a target="_blank" href="{{asset('resume/'.$applicant->job_number.'/'.$applicant->resume)}}"><i class="fa fa-file" style="font-size: 50px; color: #8FBCBB"></i></a></td>
                            <td>{{$applicant->cover_letter}}</td>
                            <td> @if (!$applicant->invite_status) <button class="btn btn-sm btn-success invite" data-target="#inviteCandidate" data-toggle="modal" data-email="{{$applicant->email}}" data-id="{{$applicant->job_id}}">Invite</button>@else Invited @endif </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="col-md">
                    {{--<button class="btn btn-success">Approve</button> --}}
                </div>
            </div>
        </div>
    </div>



    <div id="inviteCandidate" class="modal fade" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content tx-size-sm">
                <div class="modal-header pd-x-20">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Invite Candidate</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body pd-20">
                    <div class="row mt-2 mg-l-2">
                        <form method="post" id="jobopen" enctype="multipart/form-data">
                            {{--<input type="hidden" name="theClientId" id="theClientId">--}}
                            {{-- <div class="form-group">
                                 <label>Company Name</label>
                                 <input type="text" name="companyName" class="form-control" placeholder="Company Name">
                             </div>--}}
                            <div class="form-group">
                                <label>Recipient</label>
                                <input type="text" name="recipient" class="form-control" placeholder="Recipient">
                            </div>
                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" name="title" class="form-control" placeholder="Title">
                            </div>
                            <div class="form-group mb-4">
                                <label>Message</label>
                                <textarea rows="5" name="message" id="message" class="form-control"></textarea>
                            </div>
                        </form>
                    </div>
                </div><!-- modal-body -->
                <div class="modal-footer">
                    <button type="submit" id="sendToCandidate" class="btn btn-primary">Send</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div>
    @include('includes.alerts')
@endsection
@section('script')
    <script>

        $.ajaxSetup({
            headers:
                {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });

        $('#loadSudents').DataTable({
            responsive: true,
            "scrollX": true,
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ items/page',
            }
        });

        function back() {
            window.history.back();
        }

        let email= "";
        let  jobId = 0;


        /*$('.newjob').on('click',function(){
         var id = $(this).data('id');
         $('#theClientId').val(id);
         });*/

        tinymce.init({
            selector: '#message',
            height : "480",
            auto_focus: 'message'
        });

        $('.invite').on('click', function () {
            email = $(this).data('email');
            $('input[name=recipient]').val(email);
            jobId = $(this).data('id');
            let signame = "{{\Illuminate\Support\Facades\Auth::user()->firstname}} {{\Illuminate\Support\Facades\Auth::user()->lastname}}";
            let nm = $(this).closest('tr').find('td:nth-child(5)').text();
            tinyMCE.activeEditor.setContent('Dear '+nm+'<br><br><p><b>'+signame+'</b></p>\
        <p><b>HR Officer</b></p>\
        <p><b>XL Africa Group Limited.</b></p>\
        <p><b>Address: Plot 883, Samuel Manuwa Street,</b></p>\
        <p><b>Off Bishop Aboyade Cole Street, | Victoria Island, Lagos</b></p>\
        <p><b>Tel: 08159619576</b></p>\
        <p><b>fax: (234) 01-4616877</b></p><br>');
        });

        $(document).on('click', '#sendToCandidate', function () {
            let recipient = $('input[name=recipient]').val();
            let title = $('input[name=title]').val();
            let message = tinyMCE.get('message').getContent();

            /*if(message == null){
             message = $('textarea[name=messageBody]').val();
             }*/
            let ths = $(this);
            if(title == "" || title == null){
                return alert('Please Provide a message title.');
            }
            if (message) {
                $(this).prop('disabled', true);
                $(this).text('..Sending');
//                message.setProgressState(1);
                console.log({recipient: recipient, title: title, message: message, jobId:jobId});
                {{--{{route('submitMessage')}}--}}
                $.post("{{route('sendInviteMail')}}", {
                    recipient: recipient,
                    title: title,
                    message: message,
                    jobId:jobId
                }, function (data) {
                    console.log(data);
//                    message.setProgressState(0);
                    ths.prop('disabled', false);
                    ths.text('Send');
                    if (data.success) {
                        alert(data.success);
                        $('input[name=title]').val('');
                        $('textarea[name=messageBody]').val('');
                        location.reload();
                        //location.href = '';
                    } else if (data.error) {
                        alert(data.error);
                    } else {
                        alert('An error occured.');
                    }

                });
            } else {
                alert('There is no message body. Please compose your message.')
            }
        });

        $('#rmail').on('click', function(){
            let ths = $(this);
           if(confirm('Are you sure you want to send rejection mails to un-ivited candidates? It will be automatically sent')){
               let jobId = $(this).data('number');
               ths.prop('disabled', true);
               ths.text('Sending...');
               $.post("{{route('sendRejectionMail')}}", {
                   jobId:jobId
               }, function (data) {
                   console.log(data);
//                    message.setProgressState(0);
                   if (data.success) {
                       //location.href = '';
                       alert(data.success)
                   } else if (data.error) {
                       ths.prop('disabled', false);
                       ths.text('Send Rejection Mails');
                       alert(data.error);
                   } else {
                       ths.prop('disabled', false);
                       ths.text('Send Rejection Mails');
                       alert('An error occured.');
                   }
               });
           }
        });

    </script>
@endsection