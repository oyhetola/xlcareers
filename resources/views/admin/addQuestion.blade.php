@extends('Layouts.adminDashboardLayout')
@section('content')
    <div class="mt-2 container">@include('includes.messages')</div>
    <div id="msg"></div>
    <div class="slim-mainpanel">
        <div class="container mg-t-60">
            <div class="row">

                <div class="col-md-6 mb-2">
                    <div><button class="btn btn-info" data-toggle="modal" data-target="#addnewModal">Add new question category</button></div>
                    <div class="card card-body">
                        <h5 class="mb-1">Add Single Question</h5>
                        <form enctype="multipart/form-data" id="singleQ">
                            <div class="form-group">
                                <label>Select Question Category</label>
                                <select class="form-control" id="selectCategory" name="category">
                                    <option selected disabled>Select</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->category}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Question</label>
                                <textarea class="form-control" id="Question" name="question" placeholder="Type your question" rows="5"></textarea>
                            </div>
                            @include('includes.options')
                            <br>
                            <button class="btn btn-primary btn-sm resetOption" type="button">Reset Option</button>
                            <hr>
                            <div class="form-group answers mb-2 mt-2">
                                <label class="font-weight-bold">Answer</label>
                                <div class="col-md-12 takeoption mb-2"></div>
                            </div>
                            <div class="form-group">
                                <label>Question Image</label><small> Leave blank if question doesn't have image.</small>
                                <input type="file" class="form-control-file" id="exampleInputFile" name="image">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-danger btn-rounded pull-right"  id="addQuestion">Add</button>
                            </div>
                        </form>

                    </div>
                </div>
                <div class="col-md-6 mg-t-60">
                    <div class="card card-body">
                        <h5 class="mb-1">Upload Excel Instead</h5>
                        <form action="{{route('admin.uploadExcel')}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label>Select Question Category</label>
                                <select class="form-control" id="excelCat" name="category">
                                    <option selected disabled>Select</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->category}}</option>
                                    @endforeach
                                </select>
                            </div>



                            <div class="form-group">
                                <label for="exampleInputFile">Upload Excel File</label>
                                <input type="file" class="form-control-file" name="sheet" id="exampleInputFile" aria-describedby="fileHelp">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-danger btn-rounded pull-right">Upload</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>


        {{--modal for adding new category--}}
        <div class="modal fade" id="addnewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="addnewTitle">Add New Category</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" class="zmdi zmdi-close"></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="msg"></div>
                        <p id="contentArea">
                            <div class="form-group">
                                <input type="text" id="catName" name="catName" class="form-control" placeholder="Type Category name">
                            </div>



                        <div class="form-group">
                            <select class="">
                                <option>Minute</option>
                            </select>

                            <input name="duration" id="duration" type="number" class="">
                        </div>

                        <div class="form-group">
                            <select class="form-control" id="catStatus" name="catStatus">
                                <option value="1">Compulsory</option>
                                <option value="0">Not Compulsory</option>
                            </select>
                        </div>

                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-outline" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="submitNew">Add</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('includes.alerts')
@endsection
@section('script')
    <script>
        $.ajaxSetup({
            headers:
                {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });
        $(document).on('click', '.addmore', function(){
            if($('.theoption').length < 6){
                $('#moreoptions').append('<div class="form-group col-md-6"><div class="input-group">\
                <input type="text" class="form-control theoption" placeholder="Add Another option">\
                <div class="input-group-append removeoption"><i class="ti-minus input-group-text"></i></div>\
                </div></div>');
            }

        });

        $(document).on('click', '.removeoption', function(){
            $(this).parent().parent().remove();
        });

        /*$(document).on('click', '#addQuestion', function () {
         var examType = $('#inputExamType').val();
         var subject = $('#inputSubjects').val();
         var year =  $('#inputYear').val();
         var options = [];
         var question = $('#Question').val();
         $('.theoption').each(function(){
         let pushval = $(this).val();
         options.push(pushval);
         });
         var selectfile = document.getElementById('exampleInputFile');
         console.log(selectfile.files[0]);
         //var formData = new FormData($('#singleQ').submit(function(){}));
         var dataToSend = {examType:examType, subject:subject, year:year, option: options, question: question, formdata:formData}
         console.log(dataToSend);
         });*/

        $('form#singleQ').on('submit', function(e) {
            e.preventDefault();
            var options = [];
            $('.theoption').each(function(){
                let pushval = $(this).val();
                options.push(pushval);
            });
            var formData = new FormData(this);
            formData.append('options', options);
            console.log(formData);
            $.ajax({
                url: '{{route('addObjQuestion')}}',
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function (){
                    $('#addQuestion').attr("disabled", "disabled");
                    $('#singleQ').css("opacity", ".5");
                },
                success: function (response) {
                    console.log(response);
                    $('#addQuestion').removeAttr("disabled");
                    $('#singleQ').css("opacity", "");
                    $('#msg').empty();
                    if(response == "success"){
                        $('.theoption').each(function(){
                            $(this).val('');
                        });
                        $('.answer .takeoption').empty();
                        $('#msg').append('<div class="alert alert-success">\
                            Question added successfully\
                            <i class="pull-right ti-close"></i>\
                            </div>');
                        document.getElementById('singleQ').reset();
                    }else if(response == "logout"){
                        location.href = '/logout';
                    }else if(response == "Validation error"){
                        $('#msg').append('<div class="alert alert-danger">\
                            Please check that you fill all fields and Try again\
                            <i class="pull-right ti-close"></i>\
                            </div>');
                    }else{
                        $('#msg').append('<div class="alert alert-danger">\
                            Oops! Something went wrong! Try again\
                            <i class="pull-right ti-close"></i>\
                            </div>');
                    }
                    $(window).scrollTop(0);
                    //$('html body').animate({"scrollTop": 0}, "fast");

                }
            });
        });

        $(document).on('blur', '.theoption', function () {
            var ans = $(this).val();
            var opt = $('.answers .takeoption input[type="radio"]').length;
            var theid = opt+1;
            if(ans != "" && ans != null && opt < 6){
                $('.answers .takeoption').append('<label class="rdiobox rdiobox-success mg-t-15">\
                    <input type="radio" name="answer" id="answer'+theid+'" value="'+ans+'"><span>'+ ans +'</span>\
                    </label>');
            }
        });


        /*$(document).ready(function () {
         if($('.apdinput').length > 0){
         $('.resetOption').show();
         }else{
         $('.resetOption').hide();
         }
         })*/

        $(document).on('click', '.resetOption', function () {
            $('.theoption').val('');
            $('.takeoption').empty();
            $('#moreoptions').empty();
        });


        $(document).on('click', '#submitNew', function () {
            var ths = $(this);
            //alert();
            ths.attr('disabled', true);
                var data = $("input[name='catName']").val();
                var duration = $("input[name='duration']").val();
                var sub = $('select[name="catStatus"]').val();

                $.post('/subnewcategory', {catName:data, catStatus:sub, duration:duration}, function (response) {
                    console.log(response);
                    $('#msg').empty();
                    if(response.success){
                        $('#msg').append('<div class="row alert alert-success alert-dismissible fade show" role="alert">'+ response.category+' Added Successfully</div>');
                        $('body,html').animate({
                            scrollTop: 0
                        }, 800);
                        $('#addnewModal').modal('toggle');
                        $('#selectCategory').load(document.URL + ' #selectCategory>*');
                        $('#excelCat').load(document.URL + ' #excelCat>*');
                        $("input[name='location']").val('');
                        $("input").val('');
                    }else if(response == "login"){
                        location.href = "{{route('logout')}}";
                    }else{
                        $('#msg').append('<div class="row alert alert-danger alert-dismissible fade show" role="alert">Oops! Unable to add item. Make sure all fields are filled.</div>');
                        $('body,html').animate({
                            scrollTop: 0
                        }, 800);
                    }
                });

            ths.attr('disabled', false);
            ths.text('Add');
        });


        $('.theoption').on('input', function() {
            let ths = $(this);
            if(ths.attr('id') == "option1"){
                if($(document).find('#answer1').length > 0){
                    $('#answer1').next().next().text(ths.val());
                    $('#answer1').val(ths.val());
                }
            }else if(ths.attr('id') == "option2"){
                if($(document).find('#answer2').length > 0){
                    $('#answer2').next().next().text(ths.val());
                    $('#answer2').val(ths.val());
                }
            }else if(ths.attr('id') == "option3"){
                if($(document).find('#answer3').length > 0){
                    $('#answer3').next().next().text(ths.val());
                    $('#answer3').val(ths.val());
                }
            }else if(ths.attr('id') == "option4"){
                if($(document).find('#answer4').length > 0){
                    $('#answer4').next().next().text(ths.val());
                    $('#answer4').val(ths.val());
                }
            }
        });

    </script>
@endsection