@extends('Layouts.adminDashboardLayout')
@section('content')
    <div class="mt-2 container">@include('includes.messages')</div>
    <div id="msg"></div>
    <div class="slim-mainpanel">
        <div class="container mg-t-60">
            {{--@foreach($categories as $category)
                <div class="col-md-3 mg-t-20 mg-md-t-0">
                    <a href="{{route('questions', [$category->id, 'categories'=>$category->category])}}">
                        <div class="card card-body bg-primary tx-white bd-0">
                            <p class="card-text">{{$category->category}}</p>
                        </div><!-- card -->
                    </a>
                </div>
            @endforeach--}}
            <div class="overflow-x-auto">
                <table class="table table-striped table-bordered dataTable" id="table-3">
                    <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Category</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @for($i=0; $i < count($categories); $i++ )
                        <tr>
                            <td>{{$i+1}}</td>
                            <td>{{$categories[$i]->category}}</td>
                            <td><button class="btn btn-sm btn-info edit" data-toggle="modal"  data-id="{{$categories[$i]->id}}" data-category="{{$categories[$i]->category}}" data-duration="{{$categories[$i]->duration}}" data-status="{{$categories[$i]->category_status}}" data-target='#editCatModal'>Edit</button><a class="btn btn-sm btn-info"  href="{{route('questions', [$categories[$i]->id, 'categories'=>$categories[$i]->category])}}">View</a></td>
                        </tr>
                    @endfor
                    </tbody>
                </table>
            </div>
        </div>

        {{--edit category modal--}}
        <div class="modal fade" id="editCatModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="addnewTitle">Edit Category</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" class="zmdi zmdi-close"></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="msg"></div>
                        <p id="contentArea">
                        <input type="hidden" id="theId">
                        <div class="form-group">
                            <input type="text" id="catName" name="catName" class="form-control" placeholder="Type Category name">
                        </div>



                        <div class="form-group">
                            <select class="">
                                <option>Minute</option>
                            </select>

                            <input name="duration" id="duration" type="number" class="">
                        </div>

                        <div class="form-group">
                            <select class="form-control" id="catStatus" name="catStatus">
                                <option value="1">Compulsory</option>
                                <option value="0">Not Compulsory</option>
                            </select>
                        </div>

                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-outline" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="submitNew">Edit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('includes.alerts')
@endsection
@section('script')
    <script>
        $.ajaxSetup({
            headers:
                {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });

        $(document).on('click', '.edit', function(){
            $('input[name=catName]').val($(this).data('category'));
            $('#theId').val($(this).data('id'));

            $('input[name=duration]').val($(this).data('duration'));
            var status = $(this).data('status')
            $('select[name="catStatus"] option').each(function () {
                if($(this).val() == status){
                    $(this).attr('selected', 'selected');
                }
            });
        });

        $(document).on('click', '#submitNew', function () {
            var ths = $(this);
            //alert();
            var id = $('#theId').val();
            //ths.attr('disabled', true);
            var data = $("input[name='catName']").val();
            var duration = $("input[name='duration']").val();
            var sub = $('select[name="catStatus"]').val();
            console.log({id:id, categoryName:data, categoryStatus:sub, duration:duration});
            $.ajax({
                url: '{{route('editCategory')}}',
                type: 'POST',
                data: {id:id, categoryName:data, categoryStatus:sub, duration:duration},
                beforeSend: function () {
                    ths.attr("disabled", "disabled");
                    //$('#singleQ').css("opacity", ".5");
                },
                success: function (response) {
                    console.log(response);
                    ths.attr("disabled", false);
                    if(response.success){
                        $('#success').modal('toggle');
                        let msg = "<p>Category Updated Successfully</p>";
                        $('#successmsg').html(msg);
                    }else{
                        $('#error').modal('toggle');
                        let msg = "<p> Error while Updating Category</p>";
                        $('#message').html(msg);
                    }
                }
            });
        });
    </script>
@endsection