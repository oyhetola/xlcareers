@extends('Layouts.adminDashboardLayout')
@section('content')
    <div class="mt-2 container">@include('includes.messages')</div>
    <div class="slim-mainpanel">
        <div class="container pd-t-50">
                <div class="container"><button id="" class="btn btn-sm btn-info sendmsg" data-toggle="modal" data-target="#sendMessage">Send Message</button></div>
            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                        @if(Auth::check() && Auth::user()->user_role == "admin")
                        <li class="breadcrumb-item"><a href="{{route('adminDashboard')}}">Home</a></li>
                        @else
                        <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
                        @endif
                    <li class="breadcrumb-item active" aria-current="page">Tested</li>
                </ol>
                <h6 class="slim-pagetitle">Tested Candidates</h6>
            </div>
            <div class="card mg-b-60">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2">
                            <label>Country</label>
                            <select class="form-control" name="country">
                                <option>All</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label>Region</label>
                            <select class="form-control" name="region">
                                <option>All</option>
                                @foreach($regions as $region)
                                    <option value="{{$region->id}}">{{$region->region}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label>State</label>
                            <select class="form-control" name="state">
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label>Age From</label>
                            <select class="form-control" name="ageFrom">
                                <option>All</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label>Age To</label>
                            <select class="form-control" name="ageTo">
                                <option>All</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label>Discipline</label>
                            <input class="form-control" name="discipline" id="search">
                            <div id="loadResult" style="position: absolute; z-index: 100;"></div>
                        </div>
                        <div class="col-md-2">
                            <label>Qualification</label>
                            <select name="educationLevel" class="form-control">
                                <option selected>All</option>
                                <option>Primary School Leaving Certificate</option>
                                <option>Secondary School Leaving Certificate</option>
                                <option>Ordinary National Diploma (OND)</option>
                                <option>Higher National Diploma (HND)</option>
                                <option>Bachelors Degree (BSC, BA)</option>
                                <option>Graduate Diploma</option>
                                <option>Masters Degree (MSc.)</option>
                                <option>Doctorate Degree (PhD.)</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label></label>
                            <div class="col-md text-center"><button class="btn btn-success" id="filterTested">Filter</button></div>
                        </div>
                    </div>
                    {{--<div class="row">
                        <div class="col-md text-center"><button class="btn btn-success">Filter</button></div>
                    </div>--}}
                </div>
            </div>
            <div class="row"><button class="btn btn-info btn-sm" style="margin-left: 75%" id="selectAll">Select all for recommendation</button> <button class="btn btn-danger btn-sm" style="margin-left: 75%" id="deselectAll">Deselect all</button></div>
            <div class="row">
                <table id="loadStudents" class="table display responsive nowrap dataTable no-footer dtr-inline" role="grid" aria-describedby="datatable1_info" style="width: 1026px;">
                    <thead>
                    <tr role="row">
                        <th>First name</th>
                        <th>Last name</th>
                        <th>Age</th>
                        <th>Tests</th>
                        <th>Action</th>
                        <th>Multiple</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($candidates as $candidate)
                        <tr role="row">
                            <td>{{$candidate->candidate->user->firstname}}</td>
                            <td>{{$candidate->candidate->user->lastname}}</td>
                            <td>{{date('Y') - date('Y', strtotime($candidate->candidate->date_of_birth))}} </td>
                            <td>{{$candidate->result}}</td>
                            <td><a href="{{route('viewCandidateProfile', [$candidate->user_id])}}" class="btn btn-sm btn-info">View Profile</a> @if($candidate->candidate->user->resume != null)<a href="{{url('/')}}/resume/{{$candidate->user_id}}/{{$candidate->candidate->user->resume}}" target="_blank" class="btn btn-sm btn-info">CV</a>@endif</td>
                            <td><input type="checkbox" data-candidate="{{$candidate->user_id}}" class="selectMultiple"></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-md">
                <button class="btn btn-info btn-sm" data-toggle="modal" data-target="#assigntests">Assign Test</button>
                <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#recommendForJob">Recommend</button>
                {{--<button class="btn btn-sm btn-info" data-toggle="modal" data-target="#sendMessage">Send Message</button>--}}
            </div>
        </div>
    </div>

    {{--Modal for assign--}}
    <div id="assigntests" class="modal fade" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content tx-size-sm">
                <div class="modal-header pd-x-20">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">ASSIGN TEST TO CANDIDATE</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body pd-20">
                    <p class="mg-b-20 mg-sm-b-40"></p>
                    <div class="row mt-5 experience">
                        {{--<label class="col-sm-4 form-control-label section-title"><span class="tx-danger"></span>EXPERIENCE</label>--}}
                        <div class="col-md-12 col-sm-12 mg-t-10 mg-sm-t-0">
                            <div id="assignForm">
                                <div class="form-group">
                                    <label>Select Test</label>
                                    <select type="text" name="testName" class="form-control">
                                        <option selected disabled>Select Test</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->category}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            {{--<div class="row"><p class="float-right addExperienceButton"><i class="fa fa-plus"></i> Add new job Experience</p></div>--}}
                        </div>
                    </div>
                </div><!-- modal-body -->
                <div class="modal-footer">
                    <button type="button" id="assignToCandidates" class="btn btn-primary">Assign</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div>


    {{--Modal for recommend--}}
    <div id="recommendForJob" class="modal fade" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content tx-size-sm">
                <div class="modal-header pd-x-20">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Recommend Candidates for an opening</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body pd-20">
                    <p class="mg-b-20 mg-sm-b-40"></p>
                    <div class="row mt-5 experience">
                        {{--<label class="col-sm-4 form-control-label section-title"><span class="tx-danger"></span>EXPERIENCE</label>--}}
                        <div class="col-md-12 col-sm-12 mg-t-2 mg-sm-t-0">
                            <div id="recommendForm">
                                <div class="form-group">
                                    <label>Select Client</label>
                                    <select type="text" name="client" class="form-control">
                                        <option selected disabled>Select Client</option>
                                        @foreach($clients as $client)
                                            <option value="{{$client->user_id}}">{{$client->user->firstname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Select Job Opening</label>
                                    <select type="text" name="jOpening" class="form-control">
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                            {{--<div class="row"><p class="float-right addExperienceButton"><i class="fa fa-plus"></i> Add new job Experience</p></div>--}}
                        </div>
                    </div>
                </div><!-- modal-body -->
                <div class="modal-footer">
                    <button type="button" id="recommendToAdmin" class="btn btn-primary">Recommend</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div>




    
    @include('includes.alerts');
@endsection
@section('script')
    <script>
        $.ajaxSetup({
            headers:
                {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });

        $('#deselectAll').css('display', 'none');

        $('#selectAll').on('click', function () {
           $('.selectMultiple').prop('checked', 'checked');
            $('#deselectAll').css('display', 'block');
        });

        $('#deselectAll').on('click', function () {
            $('.selectMultiple').prop('checked', false);
            $(this).css('display', 'none');
        });


        $('#loadSudents').DataTable({
            responsive: true,
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ items/page',
            }
        });

        populateState($('select[name=state]'));


        $('#assignToCandidates').on('click', function () {
            let students = [];
            $('.selectMultiple').each(function () {
                if($(this).is(':checked')){
                    students.push($(this).data('candidate'));
                }
            });
            let course = $('select[name=testName] option:selected').val();
            //return console.log(students, course);
            if(students.length == 0 || course == null || course == "Select Test"){
                $('#error').modal('toggle');
                $('#message').html('Make sure at least one check box is checked and a test is selected');
            }else{
                $.ajax({
                    type: 'POST',
                    url: "{{route('assignTest')}}",
                    data: {students:students, course: course},
                    //cache: false,
                    beforeSend: function (){
                        $('#assignToCandidates').attr("disabled", "disabled");
                        $('#assignForm').css("opacity", ".5");
                    },
                    success:function (data) {
                        console.log(data);
                        $('.selectMultiple').prop('checked', '');
                        $('#assignToCandidates').removeAttr("disabled");
                        $('#assignForm').css("opacity", "");
                        //ths.attr('disabled', false);
                        if(data.error){
                            $('#error').modal('toggle');
                            let msg = "<p>"+data.error+".</p>";
                            $('#message').html(msg);
                        }else if(data.success){
                            $('#success').modal('toggle');
                            $('#assigntests').modal('toggle');
                            let msg = "<p>"+data.success+"</p>";
                            $('#successmsg').html(msg);
                            $('#assignForm select option').filter(function() {
                                return ($(this).text() == 'Select Test'); //To select Blue
                            }).prop('selected', true);
                            //setTimeout(function (){ location.reload(); }, 1200);
                        }
                    },
                    error: function (error){
                        console.log(error);
                        $('#error').modal('toggle');
                        let msg = "<p>"+data.error+".</p>";
                        $('#message').html(msg);
                        $('#assignToCandidates').attr('disabled', false);
                        $('#assignForm').css("opacity", "");
                    }
                });
            }
           /* if(course == null || course == "Select Test"){
                $('#error').modal('toggle');
                $('#message').html('Make sure you select a test.');
            }*/
            /*if(students.length != 0 && course != null){

            }else{
                $('#error').modal('toggle');
                $('#message').html('Make sure at least one check box is checked and a test is selected');
//                alert('');
            }*/

        });

        $('select[name=client]').on('change', function () {
            let id = $('select[name=client] option:selected').val();
            //alert(id);
            let options;
            let element = $('select[name=jOpening]');
            $.get('/getOpenings', {id:id}, function (data) {
                console.log(data)
                for(let i=0; i < data.jobOpenings.length; i++){
                    options += "<option value='"+data.jobOpenings[i].id+"'>"+data.jobOpenings[i].title+"</option>";
                }
                element.html("<option disabled selected>Select Opening</option>"+options);
            });
        });

        $('#recommendToAdmin').on('click', function () {
            let students = [];
            $('.selectMultiple').each(function () {
                if($(this).is(':checked')){
                    students.push(parseInt($(this).data('candidate')));
                }
            });
            let client = $('select[name=client] option:selected').val();
            let job = $('select[name=jOpening] option:selected').val();
            //console.log(students);

            if(students.length == 0 || client == null || job == null || job == "Select Opening"){
                $('#error').modal('toggle');
                $('#message').html('Make sure at least one check box is checked ! | Make sure you select a client and the corresponding job opening you are recommending for');
            }else{
                $.ajax({
                    type: 'POST',
                    url: "{{route('recommendToAdmin')}}",
                    data: {students:students, job: job, client:client},
                    //cache: false,
                    beforeSend: function (){
                        $('#recommendToAdmin').attr("disabled", "disabled");
                        $('#recommendForm').css("opacity", ".5");
                    },
                    success:function (data) {
                        console.log(data);
                        $('#recommendToAdmin').removeAttr("disabled");
                        $('#recommendForm').css("opacity", "");
                        //ths.attr('disabled', false);
                        if(data.error){
                            $('#error').modal('toggle');
                            let msg = "<p>"+data.error+".</p>";
                            $('#message').html(msg);
                        }else if(data.success){
                            $('.selectMultiple').prop('checked', '');
                            $('#success').modal('toggle');
                            $('#recommendForJob').modal('toggle');
                            let msg = "<p>"+data.success+"</p>";
                            $('#successmsg').html(msg);
                            $('select[name=client] option').filter(function() {
                                return ($(this).text() == 'Select Client');
                            }).prop('selected', true);
                            $('select[name=job] option').filter(function() {
                                return ($(this).text() == 'Select Opening');
                            }).prop('selected', true);
                            //setTimeout(function (){ location.reload(); }, 1200);
                        }
                    },
                    error: function (error){
                        console.log(error);
                        $('#error').modal('toggle');
                        let msg = "<p>"+error.error+".</p>";
                        $('#message').html(msg);
                        $('#recommendToAdmin').attr('disabled', false);
                        $('#recommendForm').css("opacity", "");
                    }
                });
            }

        });

        $('#search').on('keyup',function() {

            $('#searchres').empty();
            if ($(this).val() != '') {
                var searchKey = $(this).val();
                $.post('{{route("findDiscipline")}}', {searchKey: searchKey}, function (data) {
                    console.log(data);
                    if(data != "Please log out"){
                        if(data.searchResults.length !== 0){
                            $('#loadResult').empty();
//                            $('#loadResult').append('<p><b>Search Results</b></p>');
                            for(var i = 0; i<data.searchResults.length; i++){
                                $('#loadResult').append('<div class="actualResult" style="background: #fcfcfc; padding: 3px 5px; border-bottom: 1px solid grey">'+data.searchResults[i].fieldOfStudy+'</div>')
                            }
                        }else{
                            $('#loadResult').empty();
                            //$('#searchres').empty();
                            $('#loadResult').append('<li class="k s">No result(s) found</li>');
                        }
                    }else{
                        location.href = '/';
                    }
                });
            }else{
                //exitSearch();
            }
        });


        $(document).on('click', '.actualResult', function () {
            let res = $(this).text();
            $('#search').val(res);
            $('#search').text(res);
            $('#loadResult').empty();
        });

        $('#search').focusout(function () {
            setTimeout(function () {
                $('#loadResult').empty();
            }, 1000)
        });
    </script>
    <script src="{{asset('js/filterFetch.js')}}"></script>
@endsection