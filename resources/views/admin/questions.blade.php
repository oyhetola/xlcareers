@extends('Layouts.adminDashboardLayout')
@section('content')
    <div class="mt-2 container">@include('includes.messages')</div>
    <div id="msg"></div>
    <div class="slim-mainpanel">
        <div class="container mg-t-60">
            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Questions</li>
                </ol>
                <h6 class="slim-pagetitle">Questions</h6>
            </div>
            <div class="overflow-x-auto">
                <table class="table table-striped table-bordered dataTable" id="table-3">
                    <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Question</th>
                        <th>Options</th>
                        <th>Answer</th>
                        <th>Question Image</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @for($i=0; $i < count($questions); $i++ )
                            <tr>
                                <td>{{$i+1}}</td>
                                <td>{{$questions[$i]->question}}</td>
                                <td>{{$questions[$i]->options}}</td>
                                <td>{{$questions[$i]->answer}}</td>
                                <td>{{$questions[$i]->question_image}}</td>
                                <td><button class="btn btn-sm btn-info edit"  data-id="{{$questions[$i]->id}}" data-question="{{$questions[$i]->question}}" data-ans="{{$questions[$i]->answer}}" data-image="{{$questions[$i]->question_image}}"  data-opt="{{$questions[$i]->options}}" data-toggle='modal' data-target='#exampleModal'>Edit</button><button class="btn btn-sm btn-danger delete" data-toggle='modal' data-target='#confirmDelete' data-id="{{$questions[$i]->id}}" data-question="{{$questions[$i]->question}}">Delete</button></td>
                            </tr>
                        @endfor
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    {{--edit modal--}}
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLabel">Edit Question</h4>
                </div>
                <form id="editQn" method="post" enctype="multipart/form-data">
                    <div class="modal-body">

                        <input type="hidden" class="form-control" id="theId" name="theId">
                        <div class="form-group">
                            <label  class="form-control-label">Question</label>
                            <input type="text" class="form-control" id="quest" name="quest">
                        </div>
                        <div class="form-group">
                            <label  class="form-control-label">Options</label>
                            <input type="text" class="form-control" id="opt" name="opt">
                        </div>
                        <div class="form-group">
                            <label  class="form-control-label">Answer</label>
                            <input type="text" class="form-control" id="ans" name="ans">
                        </div>
                        {{--<div class="form-group">
                            <label  class="form-control-label">Question Image</label>
                            <input type="text" class="form-control" id="qimage">
                        </div>--}}
                        <div class="form-group" id="takeExtra">

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="submitEdit">Edit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    {{--confirm delete--}}
    <div class="modal fade small-modal" tabindex="-1" role="dialog" id="confirmDelete" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="mySmallModalLabel">Confirm Delete</h4>
                </div>
                <div class="modal-body" id="takeDeleteDetails">
                    <input type="hidden" id="getId">
                    Are you sure you want to delete this question:
                    <b><i id="tquest"></i> ?</b>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="del">Delete</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $.ajaxSetup({
            headers:
                {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });




        $('#table-3').DataTable({
            responsive: true,
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ items/page',
            }
        });


        $('.dataTables_length select').select2({ minimumResultsForSearch: Infinity });



        var userId = "{{Auth::user()->id}}";

        function populateDataTable(data) {
            // clear the table before populating it with more data
            //var tabpane = $('.tab-content').find('.tab-pane.active');
            //var tableName = tabpane.find('table').attr('id');
            //return alert(tableName);
            $("#table-3").DataTable().clear();
            var length = Object.keys(data.questions).length;
            //if(questionType == "objective"){
                for(var i = 0; i < length; i++) {
                    var question = data.questions[i];

                    // You could also use an ajax property on the data table initialization
                    $('#table-3').dataTable().fnAddData( [
                        i+1,
                        question.question,
                        question.options,
                        question.answer,
                        (question.question_image!==null? question.question_image: 'No image'),
                        "<button class='btn btn-sm btn-primary edit' data-id='"+question.id+"' data-question='"+question.question+"' data-ans='"+question.answer+"' data-image='"+question.question_image+"'  data-opt='"+question.options+"' data-toggle='modal' data-target='#exampleModal'>Edit</button> <button class='btn btn-sm btn-danger delete' data-toggle='modal' data-target='#confirmDelete' data-id='"+question.id+"' data-question='"+question.question+"'>Delete</button>"
                    ]);
                }
            /*}else if(questionType == "theory"){
                for(var i = 0; i < length; i++) {
                    var question = data.questions[i];

                    // You could also use an ajax property on the data table initialization
                    $('#'+tableName).dataTable().fnAddData( [
                        i+1,
                        question.question,
                        question.answer,
                        (question.question_image!==null? question.question_image: 'No image'),
                        "<button class='btn btn-sm btn-primary edit' data-id='"+question.id+"' data-question='"+question.question+"' data-ans='"+question.answer+"' data-image='"+question.question_image+"'  data-opt='"+question.options+"' data-toggle='modal' data-target='#exampleModal'>Edit</button> <button class='btn btn-sm btn-danger delete' data-toggle='modal' data-target='#confirmDelete' data-id='"+question.id+"' data-question='"+question.question+"' data-toggle='modal' data-target=''>Delete</button>"
                    ]);
                }
            }*//*else{
                for(var i = 0; i < length; i++) {
                    var question = data.questions[i];

                    // You could also use an ajax property on the data table initialization
                    $('#'+tableName).dataTable().fnAddData( [
                        i+1,
                        question.question,
                        question.options,
                        question.answer,
                        (question.question_image!==null? question.question_image: ''),
                        "<button class='btn btn-sm btn-primary edit' data-id='"+question.id+"' data-question='"+question.question+"' data-ans='"+question.answer+"' data-image='"+question.question_image+"'  data-opt='"+question.options+"' data-toggle='modal' data-target='#exampleModal'>Edit</button> <button class='btn btn-sm btn-danger delete' data-toggle='modal' data-target='#confirmDelete' data-id='"+question.id+"' data-question='"+question.question+"' data-toggle='modal' data-target=''>Delete</button>"
                    ]);
                }
            }*/

        }

        $(document).on('click', '.edit', function(){
            $('#theId').val($(this).data('id'));
            $('#quest').val($(this).data('question'));
            var question = $(this).data('question');
            $('#ans').val($(this).data('ans'));
            $('#opt').val($(this).data('opt'));
            $('#qimage').val($(this).data('image'));
            var img = $(this).data('image');
            if(img != null){
                $('#takeExtra').empty();
                $('#takeExtra').append('<img src="/storage/'+userId+'/'+img+'" width="400px" height="300px">')
            }else{
                $('#takeExtra').empty();
                $('#takeExtra').append('<input type="file" id="qimage" name="qimage" class="form-control" accept="image/*">');
            }
        });



        $('form#editQn').on('submit', function (e) {
            e.preventDefault();
            //var dat = {question: $('#quest').val(), options: $('#opt').val(), ans:$('#ans').val(), qimage: $('#qimage').val };
            var formdata = new FormData(this);
            var tabpane = $('.tab-content').find('.tab-pane.active');
            var tableName = tabpane.find('table').attr('id');
            $.ajax({
                url: '{{route('admin.editQuestion')}}',
                type: 'POST',
                data: formdata,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $('#submitEdit').attr("disabled", "disabled");
                    $('#editQn').css("opacity", ".5");
                },
                success: function (response) {
                    console.log(response);
                    $('#submitEdit').removeAttr("disabled");
                    $('#editQn').css("opacity", "");
                    $('#msg').empty();
                    if (response.questions !== null && response.status == 1) {
                        $('#msg').empty();
                        /*$('#msg').append('<div class="alert alert-success">\
                         Questions updated successfully\
                         <i class="pull-right ti-close"></i>\
                         </div>');*/
                        var qtype;
                        /*if(tableName == "table-1"){
                            qtype = "objective"
                            populateDataTable(response, qtype);
                        }else if(tableName == "table-3"){
                            qtype = "theory"
                            populateDataTable(response);
                        }*/
                        populateDataTable(response);

                        $('#success').modal('show');
                        $('#successmsg').empty();
                        $('#successmsg').append('Question successfully updated');
                    }else if(response == "Not authorized"){
                        location.href = "/"
                    }else if(response.errors !== null){
                        $('#successmsg').empty();
                        $('#successmsg').append('<div class="alert alert-danger">\
                            Make sure the fields : question, answer and options are not empty\
                            <i class="pull-right ti-close"></i>\
                            </div>');
                    }else {
                        $('#successmsg').empty();
                        $('#successmsg').append('<div class="alert alert-danger">\
                            Error in connection\
                            <i class="pull-right ti-close"></i>\
                            </div>');
                    }
                    $('#exampleModal').modal('toggle');
                    $('html body').animate({scrollTop:40}, "fast");
                }
            });
        });

        var del;

        $(document).on('click', '.delete', function(){
            del = $(this);
            $('#getId').val($(this).data('id'));
            $('#tquest').text($(this).data('question'));
        });

        $(document).on('click', '#del', function(){
            var ths = $(this);
            $("#table-1").DataTable().row(del.parents('tr')).remove().draw();
            $.ajax({
                url: '{{route('admin.deleteQuestion')}}',
                type: 'POST',
                data: {id:$('#getId').val()},
                beforeSend: function () {
                    ths.attr('disabled', true);
                    ths.text('..processing');
                },
                success: function (resp) {
                    console.log(resp);
                    ths.attr('disabled', false);
                    ths.text('Delete');
                    if(resp == "success"){
                        $('#confirmDelete').modal('toggle');
                        $('#success').modal('toggle');
                        $('#successmsg').empty();
                        $('#successmsg').append('Question successfully updated');
                    }else{
                        $('#successmsg').empty();
                        $('#successmsg').append('<div class="alert alert-danger">\
                            Error in connection, please try again.\
                            <i class="pull-right ti-close"></i>\
                            </div>');
                        $(window).scrollTop(0);
                    }

                }
            });
        });
    </script>
@endsection