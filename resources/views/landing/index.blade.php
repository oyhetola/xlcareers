<!DOCTYPE html>
<html lang="en">
	
<!-- Mirrored from multifour.com/projects/weber/templates/agency2/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 02 Sep 2019 07:37:12 GMT -->
<head>
		<meta charset="UTF-8">
		<title>XLTalentHub</title>
		
<link rel="icon" href="images/favicon.png" type="image/x-icon">
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width,initial-scale=1.0,viewport-fit=cover">
		<link rel="stylesheet" href="{{asset('landingAssets/css/fonts.css')}}" />
		<link rel="stylesheet" href="{{asset('landingAssets/css/bootstrap.weber.css')}}" />
		<link rel="stylesheet" href="{{asset('landingAssets/css/fx.css')}}" />
		<link rel="stylesheet" href="{{asset('landingAssets/css/magnific-popup.css')}}" />
		<link rel="stylesheet" href="{{asset('landingAssets/css/aos.css')}}" />
		<link rel="stylesheet" href="{{asset('landingAssets/css/custom.css')}}" />
		<link rel="stylesheet" href="{{asset('landingAssets/css/index.css')}}" />
	</head>
    <body class="light-page">
		<div id="wrap">
			<header id="header-text-text" class="pt-75 pb-75 pt-lg-250 pb-lg-250 text-center dark">
    			<div class="container">
        			<div class="row align-items-center">
            			<div class="col-lg text-lg-right">
                			<h1 style="color: #b23415"><strong>XLTalentHub.</strong></h1>
            			</div>
            			<hr class="sep-vertical-x2-lg ml-30 mr-30">
            			<div class="col-lg text-lg-left">
                			<p class="text-secondary">
                                <h3>Hire | Be hired</h3>
                                XLTalentHub help job seekers to set up their profiles online and get profiled by interested companies when their skills are needed.
                            </p>
            			</div>
            			<div class="col-12">
                            <a href="{{route('renderRegister')}}" class="btn btn-outline-light btn-lg mt-20 mt-lg-20 smooth" target="_self">Get Started</a>
                            <a href="{{route('viewLogin')}}" class="btn btn-outline-light btn-lg mt-20 mt-lg-20 smooth" target="_self">Login</a>
            			</div>
						<div class="col-12">
							<a href="{{route('jobBoard')}}" class="btn btn-outline-light btn-lg mt-20 mt-lg-70 smooth"> Find jobs</a>
						</div>
        			</div>
    			</div>
    			<div class="bg-wrap">
        			<div class="bg"></div>
    			</div>
			</header><nav id="nav-logo-menu-2" class="navbar navbar-expand-lg dark sticky-top fixed-top">
    			<div class="container">
        			<div class="row align-items-center">
            			<div class="col-auto mr-auto">
                			<a class="navbar-brand" href="#">
                        			<!-- <img src="images/logo-2-light.png" height="30" class="mw-100" alt="logo"> -->
                                    <h3><strong>XLTalenthub</strong></h3>
                                </a>
            			</div>
            			<div class="col-auto hidden-lg">
                			<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target=".main-menu-collapse" aria-expanded="false" aria-label="Toggle navigation"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
            			</div>
            			<div class="col-lg-auto collapse navbar-collapse main-menu-collapse">
                			<ul class="navbar-nav ml-auto">
                    			<li class="nav-item">
                        			<a class="nav-link" href="#">Home</a>
                    			</li>
                    			<li class="nav-item">
                        			<a class="nav-link" href="{{route('viewLogin')}}">Login</a>
                    			</li>
                    			<li class="nav-item">
                        			<a class="nav-link" href="{{route('renderRegister')}}">Sign Up</a>
                    			</li>
                    			<!-- <li class="nav-item">
                        			<a class="nav-link" href="#">Navbar link</a>
                    			</li> -->
                			</ul>
            			</div>
        			</div>
    			</div>
    			<div class="bg-wrap">
        			<div class="bg"></div>
    			</div>
            </nav>
            <section id="chart-center-chart" class="pt-50 pb-50 pt-md-150 pb-md-150 light">
            <div class="container">
        			<div class="row">
            			<div class="col-lg-12">
                			<div class="content-box border-x2 padding-x3">
                    			<h4>We are committed to help companies the hire best talents fit for varoius roles. You as an individual can also have a wonderful career path from this paltform.</h4>
                    			<div class="position-absolute l-50 t-100 centered padding bg-default">
                        			<img src="images/client-logo-light.png" height="20" alt="XLTalenthub">
                    			</div>
                			</div>
            			</div>
        			</div>
    			</div>
    			<div class="bg-wrap">
        			<div class="bg"></div>
    			</div>
			</section>
			{{--<section id="benefits-5col-counter" class="pt-50 pt-md-100 pb-md-50 dark counter-up">
				<div class="container">
					<div class="row">
						<div class="col">
							<h3 class="mb-10"><strong class="count-up-data">80%</strong></h3>
							<h4 class="mb-50">Clients that say Talenthub has simplified their recruitment process</h4>
						</div>
						<div class="col">
							<h3 class="mb-10"><strong class="count-up-data">70%</strong></h3>
							<h4 class="mb-50">Job seekers that Got jobs from here</h4>
						</div>
						<div class="col">
							<h3 class="mb-10"><strong class="count-up-data"></strong></h3>
							<h4 class="mb-50"></h4>
						</div>
					</div>
				</div>
				<div class="bg-wrap">
					<div class="bg"></div>
				</div>
			</section>--}}
			<footer id="footer-text-logo-text-2" class="pt-75 pb-75 dark text-center">
    			<div class="container">
        			<div class="row align-items-center">
            			<div class="col-md order-md-3 text-md-right">
                			<!--<ul class="list-unstyled list-inline padding-list mb-20 mb-md-0">
                    			<li><a href="#"><strong>Link 1</strong></a></li>
                    			<li><a href="#"><strong>Link 2</strong></a></li>
                    			<li><a href="#"><strong>Link 3</strong></a></li>
                    			<li><a href="#"><strong>Link 4</strong></a></li>
                			</ul> -->
            			</div>
            			<div class="col-md order-lg-1 text-md-left">
                			<p class="mb-0 text-secondary">© XL Outsourcing Limited. All rights reserved.</p>
            			</div>
            			<div class="col-md-auto order-md-2">
                            XLTalentHub
                			<!-- <img class="mw-100 mt-30 mt-md-0" src="images/logo-1.png" height="70" alt="logo"> -->
            			</div>
        			</div>
    			</div>
    			<div class="bg-wrap">
        			<div class="bg"></div>
    			</div>
			</footer>
		</div>
		<footer></footer>
		<div class="modal-container"></div>
		
		
		<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCByts0vn5uAYat3aXEeK0yWL7txqfSMX8"></script> -->
		<script src="../../../../../cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
		<script src="{{asset('landingAssets/js/jquery-2.1.4.min.js')}}"></script>
		<script src="{{asset('landingAssets/js/bootstrap.min.js')}}"></script>
		<script src="{{asset('landingAssets/js/jquery.magnific-popup.min.js')}}"></script>
		<script src="{{asset('landingAssets/js/jquery.smooth-scroll.min.js')}}"></script>
		<script src="{{asset('landingAssets/js/jquery.waypoints.min.js')}}"></script>
		<script src="{{asset('landingAssets/js/countUp-jquery.js')}}"></script>
		<script src="{{asset('landingAssets/js/aos.js')}}"></script>
		<script src="{{asset('landingAssets/js/custom.js')}}"></script>
		<script src="{{asset('landingAssets/js/index.js')}}"></script>
	</body>

<!-- Mirrored from multifour.com/projects/weber/templates/agency2/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 02 Sep 2019 07:38:18 GMT -->
</html>