@extends('Layouts.actualJobLayout')
@section('content')
    <div class="mt-2 container">@include('includes.messages')</div>
    <div id="msg"></div>
    <div class="slim-mainpanel">
        {{--<div class="section-wrapper">--}}
            <div class="col-md-8 offset-md-2 mt-5">
                <div class="card card-body">
                    <p class="text-center" style="text-align: center; font-size: larger; color: green">You successfully submitted your application</p>
                    {{--@if($result)--}}
                        <p style="text-align: center;"><b></b></p>
                        {{--@if($result->badge_received == 1 && $result->score >= 80)--}}
                            <div class="col-md-12" style="text-align: center"><i class="fa fa-check-circle" style="font-size: 100px; color: green"></i> </div>
                            {{--<div style="text-align: center; font-weight: bold; color: #2b542c" class="mt-1 mb-1">Badge Received! Welldone </div>--}}
                        {{--@endif()--}}
                        <div style="text-align: center !important;" class="row mt-2">
                            <a style="align-self: center; margin-left: 240px" class="btn btn-success" href="https://xlafricagroup.com"><i class="ti-dashboard"></i> Home</a>
                            <a style="align-self: center" class="btn btn-info" href="{{route('jobBoard')}}"><i class="ti-dashboard"></i> Explore more opportunities</a>
                        </div>
                    {{--@endif--}}
                </div>
            </div>
        {{--</div>--}}
    </div>
@endsection