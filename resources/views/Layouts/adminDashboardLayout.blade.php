<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Slim">
    <meta name="twitter:description" content="XL Africa Group | Careers">
    <meta name="twitter:image" content="">
    <meta name="csrf-token" content="{{csrf_token()}}">

    <!-- Facebook -->
    <!-- <meta property="og:url" content="http://themepixels.me/slim"> -->
    <meta property="og:title" content="XL Africa Group">
    <meta property="og:description" content="Recriuitment, Outsourced staffs">

    <meta property="og:image" content="">
    <meta property="og:image:secure_url" content="">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="XL AFRICA GROUP, Careers">
    <meta name="author" content="ThemePixels">

    <title>XL Africa Group | Careers</title>

    <!-- Vendor css -->
    <link href="{{asset('lib/fontawesome/css/fontawesome.css')}}" rel="stylesheet">
    <link href="{{asset('lib/fontawesome/css/all.css')}}" rel="stylesheet">
<!-- <link href="{{asset('lib/jqvmap/css/jqvmap.min.css')}}" rel="stylesheet"> -->
<!-- <link href="{{asset('lib/Ionicons/css/ionicons.css')}}" rel="stylesheet"> -->
    <link href="{{asset('lib/datatables/css/jquery.dataTables.css')}}" rel="stylesheet">
    <link href="{{asset('lib/jquery.steps/css/jquery.steps.css')}}" rel="stylesheet">

    <!-- Slim CSS -->
    <link rel="stylesheet" href="{{asset('css/slim.css')}}">
    {{--<link rel="stylesheet" href="{{asset('css/app.css')}}">--}}
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">
    <link rel="stylesheet" href="{{asset('lib/mdb/mdb.min.css')}}">
    <link href="{{asset('lib/datatables/css/jquery.dataTables.css')}}" rel="stylesheet">
    <link href="{{asset('lib/select2/css/select2.min.css')}}" rel="stylesheet">

</head>
<body class="dashboard-4">
<div class="slim-header">
    <div class="container">
        <div class="slim-header-left">
            <h2 class="slim-logo"><a href="{{route('landing')}}"><img src="/img/logo2.png" alt="" title="Logo"></a></h2>

            {{--<div class="search-box">
                <input type="text" class="form-control" placeholder="Search">
                <button class="btn btn-primary"><i class="fa fa-search"></i></button>
            </div>--}}<!-- search-box -->
        </div><!-- slim-header-left -->
        <div class="slim-header-right">
            {{--<div class="dropdown dropdown-a">
                <a href="#" class="header-notification" data-toggle="dropdown">
                    <i class="icon ion-ios-bolt-outline"></i>
                </a>
                <div class="dropdown-menu">
                    <div class="dropdown-menu-header">
                        <h6 class="dropdown-menu-title">Activity Logs</h6>
                        <div>
                            <a href="#">Filter List</a>
                            <a href="#">Settings</a>
                        </div>
                    </div><!-- dropdown-menu-header -->
                    <div class="dropdown-activity-list">
                        <div class="activity-label">Today, December 13, 2017</div>
                        <div class="activity-item">
                            <div class="row no-gutters">
                                <div class="col-2 tx-right">10:15am</div>
                                <div class="col-2 tx-center"><span class="square-10 bg-success"></span></div>
                                <div class="col-8">Purchased christmas sale cloud storage</div>
                            </div><!-- row -->
                        </div><!-- activity-item -->
                        <div class="activity-item">
                            <div class="row no-gutters">
                                <div class="col-2 tx-right">9:48am</div>
                                <div class="col-2 tx-center"><span class="square-10 bg-danger"></span></div>
                                <div class="col-8">Login failure</div>
                            </div><!-- row -->
                        </div><!-- activity-item -->
                        <div class="activity-item">
                            <div class="row no-gutters">
                                <div class="col-2 tx-right">7:29am</div>
                                <div class="col-2 tx-center"><span class="square-10 bg-warning"></span></div>
                                <div class="col-8">(D:) Storage almost full</div>
                            </div><!-- row -->
                        </div><!-- activity-item -->
                        <div class="activity-item">
                            <div class="row no-gutters">
                                <div class="col-2 tx-right">3:21am</div>
                                <div class="col-2 tx-center"><span class="square-10 bg-success"></span></div>
                                <div class="col-8">1 item sold <strong>Christmas bundle</strong></div>
                            </div><!-- row -->
                        </div><!-- activity-item -->
                        <div class="activity-label">Yesterday, December 12, 2017</div>
                        <div class="activity-item">
                            <div class="row no-gutters">
                                <div class="col-2 tx-right">6:57am</div>
                                <div class="col-2 tx-center"><span class="square-10 bg-success"></span></div>
                                <div class="col-8">Earn new badge <strong>Elite Author</strong></div>
                            </div><!-- row -->
                        </div><!-- activity-item -->
                    </div><!-- dropdown-activity-list -->
                    <div class="dropdown-list-footer">
                        <a href="page-activity.html"><i class="fa fa-angle-down"></i> Show All Activities</a>
                    </div>
                </div><!-- dropdown-menu-right -->
            </div><!-- dropdown -->--}}
            {{--<div class="dropdown dropdown-b">
                <a href="#" class="header-notification" data-toggle="dropdown">
                    <i class="icon ion-ios-bell-outline"></i>
                    <span class="indicator"></span>
                </a>
                <div class="dropdown-menu">
                    <div class="dropdown-menu-header">
                        <h6 class="dropdown-menu-title">Notifications</h6>
                        <div>
                            <a href="#">Mark All as Read</a>
                            <a href="#">Settings</a>
                        </div>
                    </div><!-- dropdown-menu-header -->
                    <div class="dropdown-list">
                        <!-- loop starts here -->
                        <a href="#" class="dropdown-link">
                            <div class="media">
                                <img src="../img/img8.jpg" alt="">
                                <div class="media-body">
                                    <p><strong>Suzzeth Bungaos</strong> tagged you and 18 others in a post.</p>
                                    <span>October 03, 2017 8:45am</span>
                                </div>
                            </div><!-- media -->
                        </a>
                        <!-- loop ends here -->
                        <a href="#" class="dropdown-link">
                            <div class="media">
                                <img src="../img/img9.jpg" alt="">
                                <div class="media-body">
                                    <p><strong>Mellisa Brown</strong> appreciated your work <strong>The Social Network</strong></p>
                                    <span>October 02, 2017 12:44am</span>
                                </div>
                            </div><!-- media -->
                        </a>
                        <a href="#" class="dropdown-link read">
                            <div class="media">
                                <img src="../img/img10.jpg" alt="">
                                <div class="media-body">
                                    <p>20+ new items added are for sale in your <strong>Sale Group</strong></p>
                                    <span>October 01, 2017 10:20pm</span>
                                </div>
                            </div><!-- media -->
                        </a>
                        <a href="#" class="dropdown-link read">
                            <div class="media">
                                <img src="../img/img2.jpg" alt="">
                                <div class="media-body">
                                    <p><strong>Julius Erving</strong> wants to connect with you on your conversation with <strong>Ronnie Mara</strong></p>
                                    <span>October 01, 2017 6:08pm</span>
                                </div>
                            </div><!-- media -->
                        </a>
                        <div class="dropdown-list-footer">
                            <a href="page-notifications.html"><i class="fa fa-angle-down"></i> Show All Notifications</a>
                        </div>
                    </div><!-- dropdown-list -->
                </div><!-- dropdown-menu-right -->
            </div><!-- dropdown -->--}}
            <div class="dropdown dropdown-c">
                <a href="#" class="logged-user" data-toggle="dropdown">
                    <img src="../img/user-image.png" alt="">
                    <span>@if(Auth::check()){{Auth::user()->firstname}}@endif</span>
                    <i class="fa fa-angle-down"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <nav class="nav">
                        <a href="{{route('adminDashboard')}}" class="nav-link"><i class="icon ion-person"></i> Dashboard</a>
                        <a href="{{route('myProfile')}}" class="nav-link"><i class="icon ion-person"></i> Profile</a>
                        <a href="{{route('logout')}}" class="nav-link"><i class="icon ion-forward"></i> Sign Out</a>
                    </nav>
                </div><!-- dropdown-menu -->
            </div><!-- dropdown -->
        </div><!-- header-right -->
    </div><!-- container -->
</div><!-- slim-header -->

<div class="slim-navbar">
    <div class="container">
        <ul class="nav">
            <li class="nav-item with-sub active">
                <a class="nav-link" href="{{route('adminDashboard')}}">
                    <i class="icon ion-ios-home-outline"></i>
                    <span>Dashboard</span>
                </a>
            </li>


            {{--<li class="nav-item with-sub">
                <a class="nav-link" href="#" data-toggle="dropdown">
                    <i class="icon ion-ios-gear-outline"></i>
                    <span>Examining</span>
                </a>
                <div class="sub-item">
                    <ul>
                        <li><a href="{{route('addQuestion')}}">Upload Questions</a></li>
                        <li><a href="{{route('allCategories')}}">All Categories</a></li>

                    </ul>
                </div><!-- dropdown-menu -->
            </li>--}}
            {{--<li class="nav-item with-sub">
                <a class="nav-link" href="#">
                    <i class="icon ion-ios-chatboxes-outline"></i>
                    <span>Candidates</span>
                    <span class="square-8"></span>
                </a>
                <div class="sub-item">
                    <ul>
                        <li><a href="{{route('testedCandidates')}}">Tested Candidates</a></li>
                        <li><a href="{{route('recommendedCandidates')}}">Recommended Candidates</a></li>
                        <li><a href="{{route('approvedCandidates')}}">Approved Candidates</a></li>
                        <li><a href="{{route('acceptedCandidates')}}">Accepted Candidates</a></li>
                        <li><a href="{{route('appliedCandidates')}}">Applications</a></li>
                        <li><a href="{{route('returnCandidatesView')}}">All Candidates</a></li>
                    </ul>
                </div>
            </li>--}}
            @if(Auth::check() && Auth::user()->user_role == "admin")
                <li class="nav-item with-sub">
                    <a class="nav-link" href="#" data-toggle="dropdown">
                        <i class="icon ion-ios-chatboxes-outline"></i>
                        <span>Openings</span>
                    </a>
                    <div class="sub-item">
                        <ul>
                            <li><a href="{{route('fetchClients')}}">Job Openings</a></li>
                            <li><a href="{{route('inviteMails')}}">Invite Mail Messages</a></li>
                        </ul>
                    </div>

                </li>

            @endif
            <li class="nav-item">
                <a class="nav-link" href="{{route('myProfile')}}">
                    <i class="icon ion-ios-chatboxes-outline"></i>
                    <span>Settings</span>
                    <span class="square-8"></span>
                </a>
            </li>
            @if(Auth::check() && Auth::user()->user_role == "admin")
            <li class="nav-item">
                <a class="nav-link" href="{{route('allUsers')}}">
                    <i class="icon ion-ios-chatboxes-outline"></i>
                    <span>App Users</span>
                    {{--<span class="square-8"></span>--}}
                </a>
            </li>
            @endif
        </ul>
    </div><!-- container -->
</div><!-- slim-navbar -->
@yield('content')

{{--Modal for sendMessage--}}
<div id="sendMessage" class="modal fade" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content tx-size-sm">
                <div class="modal-header pd-x-20">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Send Message</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body pd-20">
                    <p class="mg-b-20 mg-sm-b-40 tx-danger">*Please ensure you have all concerned candidates selected first.</p>
                    <div class="row mt-5 experience">
                        <label class="col-sm-4 form-control-label"><span class="tx-danger"></span></label>
                        <div class="col-md-12 col-sm-12 mg-t-3 mg-sm-t-0">
                            <div id="snd">
                                <form id="doMailSend">
                                    <div class="form-group">
                                        <label>Subject</label>
                                        <input type="text" class="form-control" name="emailSubject">
                                    </div>
                                    <div class="form-group">
                                        <label>Type your message*</label>
                                        <textarea class="form-control" id="mailMessage" rows="4"></textarea>
                                    </div>
                                    <button type="button" id="sendMessageToCandidates" class="btn btn-primary">Send</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </form>
                            </div>
                            {{--<div class="row"><p class="float-right addExperienceButton"><i class="fa fa-plus"></i> Add new job Experience</p></div>--}}
                        </div>
                    </div>
                </div><!-- modal-body -->
                <div class="modal-footer">

                </div>
            </div>
        </div><!-- modal-dialog -->
    </div>

<div class="clearfix mb-4"></div>
<div class="slim-footer">
    <div class="container">
        <p>Copyright {{date('Y')}} &copy; All Rights Reserved. XL Africa Group</p>
    </div><!-- container -->
</div><!-- slim-footer -->

@include('includes.modals.modals')
<script src="{{asset('lib/jquery/js/jquery.js')}}"></script>
<script src="{{asset('lib/popper.js/js/popper.js')}}"></script>
<script src="{{asset('lib/bootstrap/js/bootstrap.js')}}"></script>
<script src="{{asset('lib/datatables/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('lib/datatables-responsive/js/dataTables.responsive.js')}}"></script>
<script src="{{asset('lib/mdb/mdb.min.js')}}"></script>
<link href="{{asset('fontawesome/js/fontawesome.js/fontawesome.min.js')}}" rel="stylesheet">
<!-- <script src="{{asset('resources/js/app.js')}}"></script> -->
<script src="{{asset('lib/jquery.cookie/js/jquery.cookie.js')}}"></script>
<script src="{{asset('lib/jquery.steps/js/jquery.steps.js')}}"></script>
<script src="{{asset('lib/moment/js/moment.js')}}"></script>
<script src="{{asset('lib/jquery-ui/js/jquery-ui.js')}}"></script>
<script src="{{asset('lib/parsleyjs/js/parsley.js')}}"></script>

<script src="{{asset('lib/select2/js/select2.min.js')}}"></script>
<script src="{{asset('js/slim.js')}}"></script>
<script src="{{asset('js/custom.js')}}"></script>
<script src="{{asset('js/candidateInfo.js')}}"></script>
<script src="https://cdn.tiny.cloud/1/govn3ssxe3euc0pke4fimfb9h5dt4se49nf1h3d49d6k71k8/tinymce/5/tinymce.min.js"></script>
@yield('script')
<script>

    tinymce.init({
        selector: '#mailMessage',
        /*height: 300,
        menubar: false,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code',
            'insertdatetime media table paste code help wordcount'
        ],
        toolbar: 'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tiny.cloud/css/codepen.min.css'
        ]*/
    });


    $('#deadline').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true
    });

    $('#sendMessageToCandidates').on('click', function () {
//            e.preventDefault();
            let students = [];
            $('.selectMultiple').each(function () {
                if($(this).is(':checked')){
                    students.push(parseInt($(this).data('candidate')));
                }
            });
            let subject = $('input[name=emailSubject]').val();
            tinyMCE.triggerSave();
            let message =$('#mailMessage').val();
            //let formdata =  new FormData(this);
            if(students.length == 0){
                $('#error').modal('toggle');
                $('#sendMessage').modal('toggle');
                $('#message').html('Make sure at least one check box is checked and the message box is required.');
            }else{
                $.ajax({
                    type: 'POST',
                    url: "{{route('sendMail')}}",
                    data: {students:students, message: message, subject:subject},
                    //cache: false,
                    beforeSend: function (){
                        $('#sendMessage').prop("disabled", "disabled");
                        $('#snd').css("opacity", ".5");
                    },
                    success:function (data) {
                        console.log(data);
                        $('.selectMultiple').prop('checked', '');
                        $('#deselectAll').css('display', 'none');
                        $('#sendMessage').removeAttr("disabled");
                        $('#snd').css("opacity", "");
                        //ths.attr('disabled', false);
                        if(data.error){
                            $('#error').modal('toggle');
                            let msg = "<p>"+data.error+".</p>";
                            $('#message').html(msg);
                        }else if(data.success){
                            $('#success').modal('toggle');
                            let msg = "<p>"+data.success+"</p>";
                            $('#successmsg').html(msg);
                            $('#sendMessage').modal('toggle');
                            $('#mailMessage').val('');
                            //setTimeout(function (){ location.reload(); }, 1200);
                        }
                    },
                    error: function (error){
                        console.log(error);
                        $('#error').modal('toggle');
                        let msg = "<p>"+data.error+".</p>";
                        $('#message').html(msg);
                        $('#sendMessage').attr('disabled', false);
                        $('#snd').css("opacity", "");
                    }
                });
            }
    });
</script>
</body>
</html>