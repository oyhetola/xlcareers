@extends('Layouts.clientDashboardLayout')
@section('content')
    <div class="mt-2 container">@include('includes.messages')</div>
    <div class="slim-mainpanel">
        <div class="container pd-t-50">
            <div class="row">
                <div class="col-lg-6">
                    <h2 class="tx-inverse mg-b-15"> {{Auth::user()->firstname}} Dashboard</h2>
                    <p class="mg-b-40">.</p>

                    <h6 class="slim-card-title mg-b-15">Your Stats</h6>
                    <div class="row no-gutters">
                        <div class="col-sm-4">
                            <div class="card card-earning-summary">
                                <h6>Total Jobs Opened</h6>
                                <h1>{{$jobs}}</h1>
                                <span>Job opportunities from us</span>
                            </div><!-- card -->
                        </div><!-- col-6 -->
                        <div class="col-sm-4">
                            <div class="card card-earning-summary">
                                <h6>Total Acceptance made</h6>
                                <h1>{{$totalEmployment}}</h1>
                                <span>All acceptance for various jobs</span>
                            </div><!-- card -->
                        </div><!-- col-6 -->
                    </div><!-- row -->
                </div><!-- col-6 -->
                @if(Auth::user()->client->approval_status == 1)
                    <div class="col-lg-3 mg-t-20 mg-sm-t-30 mg-lg-t-0">
                    <ul class="nav nav-activity-profile mg-t-20">
                        <li class="newjob" data-id="{{Auth::id()}}"><a  class="nav-link" data-toggle="modal" data-target="#addjobOpenings"><i class="icon ion-ios-redo tx-purple"></i> Create New job opening</a></li>
                        {{--<li class="nav-item"><a href="{{route('educationDetails')}}" class="nav-link"><i class="icon ion-image tx-primary"></i> Filter </a></li>--}}
                        {{--<li class="nav-item"><a href="{{route('experienceDetails')}}" class="nav-link"><i class="icon ion-document-text tx-success"></i> Experience </a></li>--}}
                    </ul>
                    {{--<div class="mg-t-15">
                        <div class="card card-dash-headline">
                            <h6 class="slim-card-title">TRENDS</h6>
                            <h6 class="visitor-operating-label"></h6>


                        </div><!-- card -->
                    </div>--}}
                </div>
                @else
                    <div class="col-lg-3 mg-t-20 mg-sm-t-30 mg-lg-t-0">
                        <div class="row">
                            <p class="font-weight-bold">Your account have not been approved by the admin hence you can't enjoy perform any action yet.</p>
                            <p>You can also contact our team representative via this email address : c.achinihu@xlafricagroup.com</p>
                        </div>

                    </div>
                @endif
            {{--<div class="col-lg-6 mg-t-20 mg-sm-t-30 mg-lg-t-0">
                @if(Auth::user()->resume_status == 0)
                    <form class="md-form" id="resumeForm" method="post">
                        <h3>Upload your resume(.pdf*, .docx*)</h3>
                        <div class="file-field">
                            <div class="btn btn-primary btn-sm float-left">
                                <span>UPLOAD RESUME</span>
                                <input type="file" name="theResume">
                            </div>
                            <div>
                                <button type="submit" id="uploadResume" class="btn btn-success">Upload</button>
                            </div>
                            --}}{{--<div class="file-path-wrapper">
                                <input class="file-path validate" type="text" placeholder="Upload your file">
                            </div>--}}{{--
                        </div>
                    </form>
                @endif
                <div>
                    <a href="" class="btn btn-info">Take a test</a>
                </div>
            </div>--}}<!-- col-6 -->
            </div><!-- row -->
            <div class="clearfix"></div>
            <div id="addjobOpenings" class="modal fade" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content tx-size-sm">
                        <div class="modal-header pd-x-20">
                            <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Create Opening</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body pd-20">
                            <div class="row mt-2 mg-l-2">
                                <form method="post" id="jobopen" enctype="multipart/form-data">
                                    <input type="hidden" name="theClientId" id="theClientId">
                                    {{-- <div class="form-group">
                                         <label>Company Name</label>
                                         <input type="text" name="companyName" class="form-control" placeholder="Company Name">
                                     </div>--}}
                                    <div class="form-group">
                                        <label>Job Title</label>
                                        <input type="text" name="jobTitle" class="form-control" placeholder="Title">
                                    </div>
                                    <div class="form-group" >
                                        <label>Deadline to conclude process</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <i class="icon ion-calendar tx-16 lh-0 op-6"></i>
                                                </div>
                                            </div>
                                            <input type="text" id="deadline" name="deadline" class="form-control fc-datepicker" data-provide='datepicker' data-date-container='#addjobOpenings' placeholder="YYYY/MM/DD" >
                                        </div>
                                    </div>
                                    <div class="form-group mb-4">
                                        <label>Description</label>
                                        <textarea rows="5" name="jobDescription" class="form-control"></textarea>
                                    </div>
                                </form>
                            </div>
                        </div><!-- modal-body -->
                        <div class="modal-footer">
                            <button type="submit" id="submitJobOpening" class="btn btn-primary">Add</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div><!-- modal-dialog -->
            </div>

        </div><!-- slim-mainpanel -->
    </div>
    @include('includes.modals.modals')
    @include('includes.alerts')


@endsection
@section('script')
    <script>
        //alert();


        $.ajaxSetup({
            headers:
                {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });

        $('.newjob').on('click',function(){
            var id = $(this).data('id');
            $('#theClientId').val(id);
        });


        $('#submitJobOpening').on('click', function () {
            let ths = $(this);
            //e.preventDefault();
            //ths.d
            let theClientId = $('input[name=theClientId]').val();
            let jobTitle = $('input[name=jobTitle]').val();
            let deadline = $('input[name=deadline]').val();
            let jobDescription = $('textarea[name=jobDescription]').val();
            let formData = {theClientId:theClientId, jobTitle: jobTitle, deadline: deadline, jobDescription: jobDescription};
            console.log(formData);
            $.ajax({
                type: 'POST',
                url: "/submitJobOpening",
                data: {theClientId:theClientId, jobTitle: jobTitle, deadline: deadline, jobDescription: jobDescription},
                //cache: false,
                beforeSend: function (){
                    $('#submitJobOpening').attr("disabled", "disabled");
                    $('#jobopen').css("opacity", ".5");
                },
                success:function (data) {
                    console.log(data);
                    $('#submitJobOpening').removeAttr("disabled");
                    $('#jobopen').css("opacity", "");
                    //ths.attr('disabled', false);
                    if(data.error){
                        $('#error').modal('toggle');
                        let msg = "<p>"+data.error+".</p>";
                        $('#message').html(msg);
                    }else if(data.success){
                        $('#success').modal('toggle');
                        let msg = "<p>"+data.success+"</p>";
                        $('#successmsg').html(msg);
                        $('#jobopen input').val('');
                        $('#jobopen textarea').val('');
                        setTimeout(function (){ location.reload(); }, 1200);
                    }
                },
                error: function (error){
                    console.log(error);
                    ths.attr('disabled', false);
                }
            });
        });
    </script>
    <script src="{{asset('js/')}}"></script>
@endsection