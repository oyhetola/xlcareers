@extends('Layouts.clientDashboardLayout')
@section('content')
    <div class="mt-2 container">@include('includes.messages')</div>
    <div class="slim-mainpanel">
        <div class="container pd-t-50">
            <div><button class="btn btn-sm btn-amber pull-right" id="goback">Back</button></div>
            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    {{--<li class="breadcrumb-item"><a href="{{route('adminDashboard')}}">Dashboard</a></li>
                    --}}{{--<li class="breadcrumb-item"><a href="#">Pages</a></li>--}}{{--
                    <li class="breadcrumb-item active" aria-current="page">Profile Page</li>--}}
                </ol>
                @if(count($recommendedCandidates) > 0)
                <h6 class="slim-pagetitle">Candidates recommended for {{$recommendedCandidates[0]->jobOpening->title}}</h6>
                @endif
            </div>
            <div class="card mg-b-60">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-2">
                            <label></label>
                            <select type="text" name="accept" class="form-control">
                                <option selected>Recommended</option>
                                <option>Accepted</option>
                            </select>
                        </div>

                        <div class="col-md-4">
                            <div class="col-md text-center"><button class="btn btn-success" id="clientFilterCandidates">Filter</button></div>
                        </div>
                    </div>
                    {{--<div class="row">
                        <div class="col-md text-center"><button class="btn btn-success">Filter</button></div>
                    </div>--}}
                </div>
            </div>
            <div class="row"><button class="btn btn-info btn-sm" style="margin-left: 75%" id="selectAll">Select all for approval</button> <button class="btn btn-danger btn-sm" style="margin-left: 75%" id="deselectAll">Deselect all</button></div>
            <div class="row">
                <table id="loadStudents" class="table display responsive nowrap dataTable no-footer dtr-inline" role="grid" aria-describedby="datatable1_info" style="width: 1026px;">
                    <thead>
                    <tr role="row">
                        <th>First name</th>
                        <th>Last name</th>
                        <th>Age</th>
                        <th>Tests</th>
                        <th>Action</th>
                        <th>Multiple</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($recommendedCandidates) > 0)
                    @foreach($recommendedCandidates as $recommendedCandidate)
                         <tr role="row">
                             <td>{{$recommendedCandidate->user->firstname}}</td>
                             <td>{{$recommendedCandidate->user->lastname}}</td>
                             <td>{{$recommendedCandidate->user->personalInfo->date_of_birth}}</td>
                             <td>{{$recommendedCandidate->result}}</td>
                             <td><a href="{{route('viewCandidateProfile', [$recommendedCandidate->user_id])}}" class="btn btn-sm btn-info">View Profile</a>
                                 {{--@if($recommendedCandidate->user->resume != null)<a href="{{url('/')}}/resume/{{$recommendedCandidate->user_id}}/{{$recommendedCandidate->user->resume}}" target="_blank" class="btn btn-sm btn-info">CV</a>@endif--}}
                             </td>
                             <td><input type="checkbox" data-candidate="{{$recommendedCandidate->user_id}}" class="selectMultiple"></td>
                         </tr>
                     @endforeach
                     @endif
                    </tbody>
                </table>
            </div>
            <div class="col-md">
                <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#acceptForJob">Accept</button>
                {{--<button class="btn btn-sm btn-info" data-toggle="modal" data-target="#sendMessage">Send Message</button>--}}
            </div>
        </div>
    </div>

    {{--Modal for assign--}}
    <div id="assigntests" class="modal fade" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content tx-size-sm">
                <div class="modal-header pd-x-20">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">ASSIGN TEST TO CANDIDATE</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body pd-20">
                    <p class="mg-b-20 mg-sm-b-40"></p>
                    <div class="row mt-5 experience">
                        {{--<label class="col-sm-4 form-control-label section-title"><span class="tx-danger"></span>EXPERIENCE</label>--}}
                        <div class="col-md-12 col-sm-12 mg-t-10 mg-sm-t-0">
                            <div id="assignForm">
                                <div class="form-group">
                                    <label>Select Test</label>
                                    <select type="text" name="testName" class="form-control">
                                        <option selected disabled>Select Test</option>

                                        <option value=""></option>

                                    </select>
                                </div>
                            </div>
                            {{--<div class="row"><p class="float-right addExperienceButton"><i class="fa fa-plus"></i> Add new job Experience</p></div>--}}
                        </div>
                    </div>
                </div><!-- modal-body -->
                <div class="modal-footer">
                    <button type="button" id="assignToCandidates" class="btn btn-primary">Assign</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div>


    {{--Modal for recommend--}}



    {{--Approve--}}
    <div id="acceptForJob" class="modal fade" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content tx-size-sm">
                <div class="modal-header pd-x-20">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Accept Candidates for employment</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body pd-20">
                    <p class="mg-b-20 mg-sm-b-40"></p>
                    <div class="row mt-5 experience">
                        {{--<label class="col-sm-4 form-control-label section-title"><span class="tx-danger"></span>EXPERIENCE</label>--}}
                        <div class="col-md-12 col-sm-12 mg-t-0 mg-sm-t-0">
                            <div id="recommendForm">
                                <div class="form-group">
                                    <label>Select Job Opening</label>
                                    <select type="text" name="jOpening" class="form-control">
                                        <option value="{{(count($recommendedCandidates) > 0 ? $recommendedCandidates[0]->jobOpening_id : '')}}" selected>{{(count($recommendedCandidates) > 0 ? $recommendedCandidates[0]->jobOpening->title: '')}}</option>
                                    </select>
                                </div>
                            </div>
                            {{--<div class="row"><p class="float-right addExperienceButton"><i class="fa fa-plus"></i> Add new job Experience</p></div>--}}
                        </div>
                    </div>
                </div><!-- modal-body -->
                <div class="modal-footer">
                    <button type="button" id="accept" class="btn btn-primary">Accept</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div>



    {{--Modal for sendMessage--}}
    <div id="sendMessage" class="modal fade" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content tx-size-sm">
                <div class="modal-header pd-x-20">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Send Message</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body pd-20">
                    <p class="mg-b-20 mg-sm-b-40"></p>
                    <div class="row mt-5 experience">
                        {{--<label class="col-sm-4 form-control-label section-title"><span class="tx-danger"></span>EXPERIENCE</label>--}}
                        <div class="col-md-12 col-sm-12 mg-t-3 mg-sm-t-0">
                            <div id="">
                                <div class="form-group">
                                    <label>Type your message</label>
                                    <textarea class="form-control" rows="4"></textarea>
                                </div>
                            </div>
                            {{--<div class="row"><p class="float-right addExperienceButton"><i class="fa fa-plus"></i> Add new job Experience</p></div>--}}
                        </div>
                    </div>
                </div><!-- modal-body -->
                <div class="modal-footer">
                    <button type="button" id="assignToCandidates" class="btn btn-primary">Send</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div>
    @include('includes.alerts');
@endsection
@section('script')
    <script>
        $.ajaxSetup({
            headers:
                {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });

        $('#deselectAll').css('display', 'none');

        $('#loadSudents').DataTable({
            responsive: true,
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ items/page',
            }
        });

        $('#goback').click(function () {
            window.history.back();
        });


        $('#assignToCandidates').on('click', function () {
            let students = [];
            $('.selectMultiple').each(function () {
                if($(this).is(':checked')){
                    students.push($(this).data('candidate'));
                }
            });
            let course = $('select[name=testName] option:selected').val();
            //return console.log(students, course);
            if(students.length == 0 || course == null || course == "Select Test"){
                $('#error').modal('toggle');
                $('#message').html('Make sure at least one check box is checked and a test is selected');
            }else{
                $.ajax({
                    type: 'POST',
                    url: "{{route('assignTest')}}",
                    data: {students:students, course: course},
                    //cache: false,
                    beforeSend: function (){
                        $('#assignToCandidates').attr("disabled", "disabled");
                        $('#assignForm').css("opacity", ".5");
                    },
                    success:function (data) {
                        console.log(data);
                        $('.selectMultiple').prop('checked', '');
                        $('#assignToCandidates').removeAttr("disabled");
                        $('#assignForm').css("opacity", "");
                        //ths.attr('disabled', false);
                        if(data.error){
                            $('#error').modal('toggle');
                            let msg = "<p>"+data.error+".</p>";
                            $('#message').html(msg);
                        }else if(data.success){
                            $('#success').modal('toggle');
                            $('#assigntests').modal('toggle');
                            let msg = "<p>"+data.success+"</p>";
                            $('#successmsg').html(msg);
                            $('#assignForm select option').filter(function() {
                                return ($(this).text() == 'Select Test'); //To select Blue
                            }).prop('selected', true);
                            //setTimeout(function (){ location.reload(); }, 1200);
                        }
                    },
                    error: function (error){
                        console.log(error);
                        $('#error').modal('toggle');
                        let msg = "<p>"+data.error+".</p>";
                        $('#message').html(msg);
                        $('#assignToCandidates').attr('disabled', false);
                        $('#assignForm').css("opacity", "");
                    }
                });
            }
            /* if(course == null || course == "Select Test"){
             $('#error').modal('toggle');
             $('#message').html('Make sure you select a test.');
             }*/
            /*if(students.length != 0 && course != null){

             }else{
             $('#error').modal('toggle');
             $('#message').html('Make sure at least one check box is checked and a test is selected');
             //                alert('');
             }*/

        });

        $('select[name=client]').on('change', function () {
            let id = $('select[name=client] option:selected').val();
            //alert(id);
            let options;
            let element = $('select[name=jOpening]');
            $.get('/getOpenings', {id:id}, function (data) {
                console.log(data)
                for(let i=0; i < data.jobOpenings.length; i++){
                    options += "<option value='"+data.jobOpenings[i].id+"'>"+data.jobOpenings[i].title+"</option>";
                }
                element.html("<option>All</option>"+options);
            });
        });

        $('#selectAll').on('click', function () {
            $('.selectMultiple').prop('checked', 'checked');
            $('#deselectAll').css('display', 'block');
        });

        $('#deselectAll').on('click', function () {
            $('.selectMultiple').attr('checked', false);
            $(this).css('display', 'none');
        });

        $('#accept').on('click', function () {
            let students = [];
            $('.selectMultiple').each(function () {
                if($(this).is(':checked')){
                    students.push(parseInt($(this).data('candidate')));
                }
            });
            //let client = $('select[name=client] option:selected').val();
            let job = $('select[name=jOpening] option:selected').val();
            //console.log(students);

            if(students.length == 0 || job == null || job == "Select Opening" || job == "All"){
                $('#error').modal('toggle');
                $('#message').html('Make sure at least one check box is checked ! | Make sure you select the job opening you are recruiting for');
            }else{
                $.ajax({
                    type: 'POST',
                    url: "{{route('clientAcceptForJob')}}",
                    data: {students:students, job: job},
                    //cache: false,
                    beforeSend: function (){
                        $('#recommendToAdmin').attr("disabled", "disabled");
                        $('#recommendForm').css("opacity", ".5");
                    },
                    success:function (data) {
                        console.log(data);
                        $('#recommendToAdmin').removeAttr("disabled");
                        $('#recommendForm').css("opacity", "");
                        //ths.attr('disabled', false);
                        if(data.error){
                            $('#error').modal('toggle');
                            let msg = "<p>"+data.error+".</p>";
                            $('#message').html(msg);
                        }else if(data.success){
                            $('.selectMultiple').prop('checked', '');
                            $('#success').modal('toggle');
                            $('#recommendForJob').modal('toggle');
                            let msg = "<p>"+data.success+"</p>";
                            $('#successmsg').html(msg);
                            $('select[name=client] option').filter(function() {
                                return ($(this).text() == 'Select Client');
                            }).prop('selected', true);
                            $('select[name=job] option').filter(function() {
                                return ($(this).text() == 'Select Opening');
                            }).prop('selected', true);
                            //setTimeout(function (){ location.reload(); }, 1200);
                        }
                    },
                    error: function (error){
                        console.log(error);
                        $('#error').modal('toggle');
                        let msg = "<p>"+error.error+".</p>";
                        $('#message').html(msg);
                        $('#recommendToAdmin').attr('disabled', false);
                        $('#recommendForm').css("opacity", "");
                    }
                });
            }

        })
    </script>
    <script src="{{asset('js/clientFilter.js')}}"></script>
@endsection