@extends('Layouts.dashboardLayout')
@section('content')
    <div class="mt-2 container">@include('includes.messages')</div>
    <div class="container">
    </div>
    <div class="slim-mainpanel">
        <div class="container">
            @if(count($objQuestions) > 0)
            <input type="hidden" id="categoryid" value="{{$objQuestions[0]->category_id}}">
            @endif
            <div class="row">
                <div class="col-md-3">
                    <div class="card-icon-wrapper danger" style="margin-left: 50%">
                        <div id="min">@if(count($objQuestions) > 0){{$objQuestions[0]->category->duration}}</div>: <div id="sec">00</div>@endif
                    </div>
                    <div style="margin-left: 52%; margin-top: 0 !important;"><h2>Minute</h2></div>
                </div>
                <div class="col-md-6">
                    @if(count($objQuestions) > 0)
                        @foreach($objQuestions as $key=>$objQuestion)
                            <div class="card questionContainer" id="q{{$key+1}}" data-questionid="{{$objQuestion->id}}">
                                <div class="card-header bg-info text-white">Question {{$key+1}}</div>
                                <div class="card-body p-5">
                                    <p class="mg-l-4" style="font-size: large"><b>{{$objQuestion->question}}</b></p>
                                    @if($objQuestion->question_image !== null)
                                        <div class="row mt-2 mb-2" style="text-align: center;" >
                                            <img src="/questionImageStorage/{{$objQuestion->question_image}}" class="card-img" width="460px" height="100px">
                                        </div>
                                    @endif
                                    @for($i=0; $i<count($objQuestion->option); $i++)
                                        <div class="d-block ml-2">
                                            <label class="rdiobox mg-t-15">
                                                <input type="radio"  id="radio{{$objQuestion->id}}" name="radio{{$objQuestion->id}}" value="{{$objQuestion->option[$i]}}" class="radvalue"><span> {{$objQuestion->option[$i]}} </span>
                                            </label>
                                            {{--<label class="custom-control custom-radio">
                                                <input id="radio{{$objQuestion->id}}" name="radio{{$objQuestion->id}}" value="{{$objQuestion->option[$i]}}" type="radio" class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">{{$objQuestion->option[$i]}}</span>
                                            </label>--}}
                                        </div>

                                    @endfor
                                </div>
                                {{--<div class="card-footer"></div>--}}
                            </div>
                        @endforeach
                    @else
                        <p>There are no questions for this test yet.</p>
                    @endif

                    <div class="actionButtons"> </div>
                </div>
                <div class="col-md-3">
                </div>
            </div>


        </div>
        <div class="container col-sm-12">
            {{--col-lg-10 offset-lg-1--}}
            <div class="pagination-wrapper">
                <nav aria-label="Page navigation" id="n1">
                    <ul class="pagination mg-b-0">
                        @foreach($objQuestions as $key=>$objQuestion)
                            @if($key == 0)
                                <li data-target="#q{{$key+1}}" class="page-item waves-effect waves-light btnq mt-3 selectButtonactive"><a class="page-link" href="javascript:void(0)">{{$key+1}}</a></li>
                            @else
                                <li data-target="#q{{$key+1}}" class="page-item waves-effect waves-light btnq mt-3"><a class="page-link" href="javascript:void(0)">{{$key+1}}</a></li>
                            @endif
                        @endforeach
                    </ul>
                </nav>
            </div>
            {{--<div class="btn-group" id="n1">
                @foreach($objQuestions as $key=>$objQuestion)
                    @if($key == 0)
                        <button type="button" data-target="#q{{$key+1}}" class="btn btn-primary waves-effect waves-light btnq mt-3 selectButtonactive">{{$key+1}}</button>
                    @else
                        <button type="button" data-target="#q{{$key+1}}" class="btn btn-primary waves-effect waves-light btnq mt-3">{{$key+1}}</button>
                    @endif
                @endforeach
            </div>--}}
            {{--<div class="btn-group" id="n2">
                @foreach($theoryQuestions as $key=>$theoryQuestion)
                    <button type="button" data-target="#qt{{$key+1}}" class="btn btn-secondary waves-effect waves-light btnqt mt-3" >{{$key+1}}</button>
                @endforeach
            </div>--}}
        </div>
    </div>
    @include('includes.alerts')

    {{--//confirm submit--}}
    <div id="confirmSubmit" class="modal fade">
        <div class="modal-dialog" role="document">
            <div class="modal-content tx-size-sm">
                <div class="modal-body tx-center pd-y-20 pd-x-20">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="tx-danger mg-b-20">Are you sure you want to submit this test?</h4>
                    <div class="m"></div>
                    <button type="button" class="btn btn-outline-danger pd-x-25" data-dismiss="modal" aria-label="Close">No</button>
                    <button type="button" class="btn btn-outline-danger pd-x-25" id="csubmitq">Yes</button>
                </div><!-- modal-body -->
            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div><!-- modal -->
@endsection
@section('script')

    <script>
        $.ajaxSetup({
            'headers' : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });
        /*$('#n2').attr('hidden', true);


        $(document).on('click', '.nav-link', function(){
            if($(this).attr('id') == "tb1"){
                $('#n2').attr('hidden', true);
                $('#n1').attr('hidden', false);
            }else if($(this).attr('id') == "tb2"){
                $('#n1').attr('hidden', true);
                $('#n2').attr('hidden', false);
            }
        });*/


        var questionContainer = $('.questionContainer');
        var actionDiv = $('.actionButtons');
        var previous = '<button class="btn btn-info pull-left prevq">Previous</button>';
        var next = '<button class="btn btn-info pull-right nextq">Next</button>';
        var submit = '<button class="btn btn-danger pull-right submitq">Submit</button>';
        var moveToTheory = '<p class="pull-right font-weight-bold" style="color: red">Click on the theory Tab</p>';
        if(questionContainer.length > 1){
            //alert(questionContainer.length);
            questionContainer.each(function () {
                var nexquestion = $(this).next();
                var previousquestion =  $(this).prev();
                if(previousquestion.length == 0){
                    $(this).addClass('activeQuestion');
                    actionDiv.append(next);
                }else{
                    $(this).addClass('hideThisQuestion');
                }
            });
        }else if(questionContainer.length == 1){
            questionContainer.addClass('activeQuestion');
            actionDiv.append(submit);
        }

        $(document).on('click', '.nextq', function () {
            //alert($(document).find('.mylink').length)
            $(document).find('.activeQuestion').each(function () {
                var moveNext =  $(this).next();
                if($(this).hasClass('activeQuestion')){
                    $(this).removeClass('activeQuestion');
                    $(this).addClass('hideThisQuestion');
                    //$('.btnq').removeClass('.selectButtonactive');
                    var t = $(document).find('.selectButtonactive');
//             if(moveNext.length > 0){
                    actionDiv.empty();
                    moveNext.addClass('activeQuestion');
                    if(moveNext.next().length > 0 && moveNext.next().hasClass('questionContainer')){
                        actionDiv.append(previous);
                        actionDiv.append(next);
                        t.next().addClass('selectButtonactive');
                        t.removeClass('selectButtonactive');
                    }else if(moveNext.next().length > 0 && !moveNext.next().hasClass('questionContainer')){
                        actionDiv.append(previous);
                        actionDiv.append(submit);
                        t.next().addClass('selectButtonactive');
                        t.removeClass('selectButtonactive');
                    }
                    else if(moveNext.next().length > 0 && !moveNext.next().hasClass('questionContainer')){
                        actionDiv.append(previous);
                        actionDiv.append(moveToTheory);
                    }

//             }
                }
            })
        });


        $(document).on('click', '.prevq', function () {
            $(document).find('.activeQuestion').each(function () {
                var prevQuestion =  $(this).prev();
                if($(this).hasClass('activeQuestion')){
                    $(this).removeClass('activeQuestion');
                    $(this).addClass('hideThisQuestion');
                    var t = $(document).find('.selectButtonactive');
//            if(prevQuestion){
                    actionDiv.empty();
                    prevQuestion.addClass('activeQuestion');
                    if(prevQuestion.prev().length > 0){
                        actionDiv.append(previous);
                        actionDiv.append(next);
                        t.prev().addClass('selectButtonactive');
                        t.removeClass('selectButtonactive');
                    }else{
                        actionDiv.append(next);
                        t.prev().addClass('selectButtonactive');
                        t.removeClass('selectButtonactive');
                    }

//            }
                }
            })
        });


        $(document).on('click', '.btnq', function () {
            var theId = $(this).data('target').toString();

            $('.btnq').removeClass('selectButtonactive');
            $(this).addClass('selectButtonactive');
            //alert(theId.substring(1));
            let verifyElement = $(document).find(theId);
            if(verifyElement){
                if(!$(theId).hasClass('activeQuestion')){
                    $(document).find('.activeQuestion').each(function () {
                        var prevQuestion = $(this).prev();
                        var nextQuestion = $(this).next();
                        if($(this).hasClass('activeQuestion')){
                            $(this).removeClass('activeQuestion');
                            $(this).addClass('hideThisQuestion');
                        }
                    });
                    $(theId).addClass('activeQuestion');
                    actionDiv.empty();
                    if($(theId).next().length > 0  && $(theId).prev().length > 0 && $(theId).next().hasClass('questionContainer')) {
                        actionDiv.append(previous);
                        actionDiv.append(next);
                    }
                    /*else if($(theId).next() == 0){
                     actionDiv.append(submit)
                     }else if($(theId).prev() == 0){
                     actionDiv.append(next);
                     }*/
                    else if($(theId).next().hasClass('actionButtons')  && $(theId).prev().length > 0){
                        actionDiv.append(previous);
                        actionDiv.append(submit);
                    }else if($(theId).next().hasClass('actionButtons')  && $(theId).prev().length > 0){
                        actionDiv.append(previous);
                        actionDiv.append(moveToTheory);
                    }
                    else if($(theId).next().length > 0 && $(theId).prev().length == 0 && $(theId).next().hasClass('questionContainer')){
                        actionDiv.append(next);
                    }


                }
            }
        });

        $(document).on("click","#back",function (){
            window.history.back();
        });



        $(document).on('click', '.submitq', function () {
            //alert();
            answers =[]
            var lk= $('.questionContainer').length;
            $(document).find('.questionContainer').each(function () {
                if($(this).find('input[type=radio]:checked').length !== 0) {
                    var ans = $(this).find('input[type=radio]:checked');
                    answers.push(ans.val());
                }
            });

            $('#confirmSubmit').find('.m').empty();
            if(lk-answers.length != 0){
                $('#confirmSubmit').find('.m').append('<p>'+(lk-answers.length)+' unanswered question'+(lk-answers.length > 1 ? 's':'')+'</p>');
            }
            $('#confirmSubmit').modal('toggle');
        });

        $(document).on('click', '#csubmitq', function () {
            //alert();
            submitTest();
        });

        function submitTest() {
            //alert();
            var answer = [];
            var novalue = "novalue";
            var qids = [];

            $(document).find('.questionContainer').each(function () {
                if($(this).find('input[type=radio]:checked').length !== 0){
                    var ans = $(this).find('input[type=radio]:checked');
                    answer.push(ans.val());
                }else{
                    answer.push(novalue);
                }
                qids.push($(this).data('questionid'));
            });
            var answerToSend = {};
            //answerToSend = answer;
            for(var i=0; i<answer.length; i++){
                answerToSend[i]  = answer[i];
            }
            var categoryId = $('#categoryid').val();

            //console.log(year);
            console.log({'categoryId':categoryId, 'objAnswers':answerToSend});
            $.ajax({
                url: "{{route('submitAnswers')}}",
                type: "POST",
                beforeSend: function(){
                    $('.submitq').attr('disabled', true);
                    $('.submitq').css('opacity', 0.3);
                    $('#csubmitq').attr('disabled', true);
                    $('#csubmitq').css('opacity', 0.3);
                },
                data: {'categoryId':categoryId, 'objAnswers':answer},
                success: function (response) {
                    console.log(response);
                    $('#msg').empty();
                    $('.submitq').attr('disabled', false);
                    $('.submitq').css('opacity', '');
                    /*$('#csubmitq').attr('disabled', false);
                    $('#csubmitq').css('opacity', '');*/
                    if(response.token){
                        var l = "{{url('/')}}"
//             window.location.href = "/students/getResult/"+response.token.token;
                        window.location.href = l+"/candidate/getResult/"+response.token.token;
                    }else{
                        $('#msg').append('<div class="alert alert-danger">\
                            Ooop! something went wrong, try again\
                            <i class="pull-right ti-close"></i>\
             </div>');
                    }
                }
            });
        }

        /*setInterval(function (args) {
            var val = parseInt($('#min').text());
            if(val != 0){
                val = val-1;
                if(val == 5){
                    $('#reminder').modal('toggle');
                    $('#remindermsg').append(val+ " minutes more.")
                }
                $('#min').html(val);
            }else{
                submitTest();
            }
        }, 60000)*/

        //let sec = 60;

        setTimeout(function (args) {
            var base = setInterval(function (args) {
                var val = parseInt($('#min').text());
                var sec = parseInt($('#sec').text());
                if(sec != 0){
                    sec =  sec - 1;
                }else if(sec == 0 && val == 0){
                    clearInterval(base);
                    submitTest();
                }else{
                    sec = 60;
                    sec =  sec - 1;
                    val = val -1;
                    if(val == 4 && sec == 59){
                        $('#reminder').modal('toggle');
                        $('#remindermsg').empty();
                        $('#remindermsg').append("Less than "+ (val+1) + " minutes more.")
                    }
                    if(val == 0 && sec == 59){
                        $('#reminder').modal('toggle');
                        $('#remindermsg').empty();
                        $('#remindermsg').append("Less than "+ (val+1) + " minutes more.")
                    }
                }
                $('#min').html(val);
                $('#sec').html((sec > 9 ? sec: '0'+sec));
            }, 1000)
        }, 1300);


    </script>
@endsection