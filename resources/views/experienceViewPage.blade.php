@extends('Layouts.dashboardLayout')
@section('content')

    <div class="container pd-t-50">
        <div class="mt-2 container">@include('includes.messages')</div>
        @include('includes.modals.modals')
        <div class="row row-sm">
        <div class="col-lg-12">
            {{--<div class="card card-profile">
                <div class="card-body">
                    <div class="media">
                        <img src="../img/img1.jpg" alt="">
                        <div class="media-body">
                            <h3 class="card-profile-name">Katherine Lumaad</h3>
                            <p class="card-profile-position">Executive Director at <a href="#">ThemePixels, Inc.</a></p>
                            <p>San Francisco, California</p>

                            <p class="mg-b-0">A consummate human capital management professional with international training and talent management implementations experience across the entire universe...<a href="#">Read more</a></p>
                        </div><!-- media-body -->
                    </div><!-- media -->
                </div><!-- card-body -->
                <div class="card-footer">
                    <a href="#" class="card-profile-direct">http://thmpxls.me/profile?id=katherine</a>
                    <div>
                        <a href="#">Edit Profile</a>
                        <a href="#">Profile Settings</a>
                    </div>
                </div><!-- card-footer -->
            </div>--}}<!-- card -->
            <ul class="nav nav-activity-profile mg-t-20">
                    <li class="nav-item" data-toggle="modal" data-target="#experienceInfoModal"><a href="javascript:void(0)" class="nav-link"><i class="icon ion-document-text tx-success"></i> Add Experience </a></li>
            </ul>

            <div class="card card-experience mg-t-20">
                <div class="card-body">
                    <div class="slim-card-title">Work Experience</div>
                    <div class="takemediaExp">
                        @foreach(\Illuminate\Support\Facades\Auth::user()->experience as $experience)
                            <div class="media">
                                <div class="experience-logo">
                                    <i class="icon ion-briefcase"></i>
                                </div><!-- experience-logo -->
                                <div class="media-body">
                                    <h6 class="position-name">{{$experience->company_name}}</h6>
                                    <p class="position-company">{{$experience->job_title}}</p>
                                    <p class="position-year">{{$experience->workStart_month}} {{$experience->workStart_year}} - {{($experience->currentWorkStatus == 1 ? '- present': $experience->workEnd_month .'  '.$experience->workEnd_year)}}&nbsp;-&nbsp;
                                        <a href="#">Edit</a>
                                    </p>
                                </div><!-- media-body -->
                            </div>
                        @endforeach
                    </div><!-- media -->
                </div><!-- card-body -->
                <div class="card-footer">
                    <a href="#"> <i class="fa fa-angle-down"></i></a>
                    {{--<a href="#">Show more<span class="d-none d-sm-inline"> experiences (4)</span> <i class="fa fa-angle-down"></i></a>--}}
                    <a data-toggle="modal" data-target="#experienceInfoModal" href="javascript:void(0)">Add new</a>
                </div><!-- card-footer -->
            </div><!-- card -->


        </div><!-- col-8 -->


    </div>
    </div>
@include('includes.alerts')
@endsection
@section('script')
    <script>
        //alert();
        generateYears();
        populateMonth();
        populateCountry();
        populateState($('select[name=stateoforigin]'));
        populateState($('select[name=stateofresidence]'));
        populateState($('select[name=schoolState]'));

        $.ajaxSetup({
            headers:
                {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });

    </script>
    <script src="{{asset('js/formSubmission.js')}}"></script>
@endsection
