<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/lll', function () {
    return view('landing.index');
})->name('landing');

Route::get('/sign-in', function () {
    return view('login');
})->name('viewLogin');

//data get routes
Route::get('/getCountries', 'dashboardController@getCountries')->name('dashboard')->name('getCountries');
Route::get('/getStates', 'dashboardController@getStates')->name('dashboard')->name('getStates');



//account routes
Route::get('/register', 'registerController@renderRegister')->name('renderRegister');
Route::post('/sign-up', 'registerController@register')->name('signup');
Route::post('/register', 'registerController@registerClient')->name('registerClient');
Route::post('/login', 'loginController@logUserIn')->name('logUserIn');
Route::get('/confirmAccount/{token}', 'registerController@confirmAccount')->name('confirmAccount');
Route::get('/logout', 'loginController@logout')->name('logout');

//user tasks by admin
Route::get('allUsers', 'dashboardController@allUsers')->name('allUsers');
Route::put('suspendUser', 'dashboardController@suspendUser')->name('suspendUser');
Route::put('deleteUser', 'dashboardController@deleteUser')->name('deleteUser');
Route::post('addUser', 'registerController@addUser')->name('addUser');


//candidates dashboard routes
Route::get('/dashboard', 'dashboardController@index')->name('dashboard');
Route::get('/educationDetails', 'dashboardController@educationDetails')->name('educationDetails');
Route::get('/experienceDetails', 'dashboardController@experienceDetails')->name('experienceDetails');
Route::post('/submitPersonalInfo', 'dashboardController@submitPersonalInfo')->name('submitPersonalInfo');
Route::post('/submitEducationInfo', 'dashboardController@submitEducationInfo')->name('submitEducationInfo');
Route::post('/submitExperienceInfo', 'dashboardController@submitExperienceInfo')->name('submitExperienceInfo');
Route::post('/updateExperienceInfo', 'dashboardController@updateExperienceInfo')->name('updateExperienceInfo');
Route::post('/updateEducationInfo', 'dashboardController@updateEducationInfo')->name('updateEducationInfo');
Route::post('/submitResume', 'dashboardController@submitResume')->name('submitResume');
Route::get('/myProfile', 'dashboardController@myProfile')->name('myProfile');
Route::get('/userSetings', 'dashboardController@userSetings')->name('userSetings');
Route::post('/updateProfile', 'dashboardController@updateMe')->name('updateProfile');
Route::post('/updateProfile2', 'dashboardController@updateMe2')->name('updateProfile2');
Route::post('changePassword', 'dashboardController@changePassword')->name('changePassword');
//get questions category
Route::get('/yourCategories', 'dashboardController@loadCategories')->name('yourCategories');
//candidates questions
Route::get('/candidate/questions', 'dashboardController@loadQuestions')->name('loadQuestions');
Route::post('/candidate/submitAnswers', 'dashboardController@submitAnswers')->name('submitAnswers');
Route::get('/candidate/getResult/{token}', 'dashboardController@getResult')->name('getResult');


//admin dashboard
Route::get('/adminDashboard', 'adminDashboardController@index')->name('adminDashboard');
Route::get('/addQuestion', 'adminDashboardController@addQuestion')->name('addQuestion');
Route::post('/addObjQuestion', 'adminDashboardController@addObjQuestion')->name('addObjQuestion');
Route::post('/uploadExcel', 'adminDashboardController@uploadExcel')->name('admin.uploadExcel');
Route::post('/subnewcategory', 'adminDashboardController@subnewcategory')->name('subnewcategory');
Route::get('/allCategories', 'adminDashboardController@allCategories')->name('allCategories');
Route::get('/questions/{id}', 'adminDashboardController@getQuestions')->name('questions');
//edit|delete
Route::post('/admin/editQuestion', ['as'=>'admin.editQuestion', 'uses'=>'adminDashboardController@editQuestion']);
Route::post('/admin/deleteQuestion', ['as'=>'admin.deleteQuestion', 'uses'=>'adminDashboardController@deleteQuestion']);


Route::get('admin/candidates', 'adminDashboardController@returnCandidatesView')->name('returnCandidatesView');
Route::post('/admin/fetchCandidates', 'adminDashboardController@fetchCandidates')->name('fetchCandidates');
Route::get('/testedCandidates', 'adminDashboardController@testedCandidates')->name('testedCandidates');
Route::get('/recommendedCandidates', 'adminDashboardController@recommendedCandidates')->name('recommendedCandidates');
Route::get('/appliedCandidates', 'adminDashboardController@appliedCandidates')->name('appliedCandidates');
Route::get('/acceptedCandidates', 'adminDashboardController@acceptedCandidates')->name('acceptedCandidates');
Route::get('/approvedCandidates', 'adminDashboardController@approvedCandidates')->name('approvedCandidates');
Route::get('/admin/jobOpenings', 'adminDashboardController@jobOpenings')->name('fetchClients');
Route::post('/submitJobOpening', 'adminDashboardController@submitJobOpening')->name('submitJobOpening');
Route::put('suspendClient', 'adminDashboardController@suspendClient')->name('suspendClient');
Route::put('approveClient', 'adminDashboardController@approveClient')->name('approveClient');
Route::get('viewCandidateProfile/{id}', 'adminDashboardController@viewCandidateProfile')->name('viewCandidateProfile');
Route::post('assignTest', 'adminDashboardController@assignTest')->name('assignTest');
Route::get('getOpenings', 'adminDashboardController@getOpenings')->name('getOpenings');
Route::post('recommendToAdmin', 'adminDashboardController@recommendToAdmin')->name('recommendToAdmin');
Route::post('approveForClient', 'adminDashboardController@approveForClient')->name('approveForClient');
Route::post('acceptForJob', 'adminDashboardController@acceptForJob')->name('acceptForJob');
Route::post('editCategory', 'adminDashboardController@editCategory')->name('editCategory');
Route::post('findDiscipline', 'adminDashboardController@findDiscipline')->name('findDiscipline');
Route::post('sendMail', 'adminDashboardController@sendMail')->name('sendMail');
Route::post('sendInviteMail', 'adminDashboardController@sendInviteMail')->name('sendInviteMail');
Route::post('sendRejectionMail', 'adminDashboardController@sendRejectionMail')->name('sendRejectionMail');
Route::get('inviteMails', 'adminDashboardController@loadinviteMails')->name('inviteMails');

//close applicacaton
Route::post('closeApplication', 'adminDashboardController@closeApplication')->name('closeApplication');
Route::post('deleteOpening', 'adminDashboardController@deleteOpening')->name('deleteOpening');

//filter
Route::post('filterCandidates', 'filterController@filterCandidates')->name('filterCandidates');
Route::post('filterRecommended', 'filterController@filterRecommended')->name('filterRecommended');
Route::post('filterApplied', 'filterController@filterApplied')->name('filterApplied');
Route::post('filterApproved', 'filterController@filterApproved')->name('filterApproved');
Route::post('filterAll', 'filterController@filterAll')->name('filterAll');
Route::post('filterTested', 'filterController@filterTested')->name('filterTested');
Route::post('filterAccepted', 'filterController@filterAccepted')->name('filterAccepted');
    //client__filter
    Route::post('clientSwitchFilter', 'filterController@clientSwitchFilter')->name('clientSwitchFilter');
//clients
Route::get('myOpenings', 'clientController@myOpenings')->name('myOpenings');
Route::get('getMyCandidates', 'clientController@getMyCandidates')->name('getMyCandidates');
Route::get('acceptedByme', 'clientController@acceptedByme')->name('acceptedByme');
Route::post('clientAcceptForJob', 'clientController@clientAcceptForJob')->name('clientAcceptForJob');


//Job Board
Route::get('/', 'dashboardController@jobBoard')->name('jobBoard');
Route::get('apply', 'dashboardController@applyForJob')->name('applyForJob');
Route::post('apply', 'dashboardController@signifyInterest')->name('signifyInterest');
Route::get('myApplications', 'dashboardController@myApplications')->name('myApplications');
Route::post('searchJobs', 'dashboardController@searchJobs')->name('searchJobs');

//submit application
Route::post('submitapplication', 'dashboardController@submitapplication')->name('submitapplication');
Route::get('applicationSubmitted', 'dashboardController@applicationSubmitted')->name('applicationSubmitted');
Route::get('applicants', 'dashboardController@applicants')->name('applicants');
Route::post('doSubscribe', 'dashboardController@doSubscribe')->name('doSubscribe');
Route::get('unsubcribe', 'dashboardController@unSubscribe')->name('unsubcribeFromJobNotification');
//password recovery
Route::get('recoverPassword', 'registerController@renderPasswordRecoverPage')->name('renderPasswordRecovery');
Route::get('createNewPassword/{id}', 'registerController@createNewPassword')->name('createNewPassword');
Route::post('changeThePassword', 'registerController@changePassword')->name('changeThePassword');
Route::post('recoverPass', 'registerController@recoverPass')->name('recoverPass');
Route::get('returnToSafety', 'dashboardController@returnToSafety')->name('returnToSafety');

