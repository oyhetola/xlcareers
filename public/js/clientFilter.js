$('#clientFilterCandidates').on('click', function () {
    "use strict";
    let ths = $(this);
    //let client = $('select[name=client] option:selected').val();
    let job = $('select[name=jOpening] option:selected').val();
    let param = $('select[name=accept] option:selected').val();
    var toSend = {job: job, param:param};
    console.log(toSend);
    $.ajax({
        type: "POST",
        url: "/clientSwitchFilter",
        data: toSend,
        //contentType: "application/json; charset=utf-8",
        //dataType: "json",
        beforeSend: function () {
            ths.attr('disabled', true);
            ths.text('..processing');
        },
        success: function (response) {
            ths.attr('disabled', false);
            ths.text('Filter');
            console.log(response);
            if(response.rcandidates.length > 0){
                populateDataTable3(response);
            }else{
                $('#success').modal('toggle');
                $('#successmsg').append('No result(s) returned');
            }
            $(window).scrollTop(200);

        }
    });
});


function populateDataTable3(data) {

    $("#loadStudents").DataTable().clear();
    var length = Object.keys(data.rcandidates).length;

    for(var i = 0; i < length; i++) {
        var candidate = data.rcandidates[i];

        // You could also use an ajax property on the data table initialization
        $('#loadStudents').dataTable().fnAddData( [
            candidate.firstname,
            candidate.lastname,
            new Date().getFullYear() - new Date(candidate.date_of_birth).getFullYear(),
            candidate.result,
            "<a href='/viewCandidateProfile/"+candidate.user_id+"' class='btn btn-sm btn-info'>Profile</a>",
            "<input type='checkbox' data-candidate='"+candidate.user_id+"' class='selectMultiple'>"
        ]);
    }
}
// (candidate.resume != null ? "<a href='/resume/"+candidate.user_id+"/"+candidate.resume+"' target='_blank' class='btn btn-sm btn-info'>CV</a>