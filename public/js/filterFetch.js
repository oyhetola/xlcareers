$(document).ready(
    populateCountry()
);

generateAge();
populateState($('select[name=state]'));
function populateCountry() {
    let options;
    $.get('/getCountries', function (data) {
        for(let i=0; i < data.countries.length; i++){
            if(data.countries[i].country == "Nigeria"){
                options += "<option value='"+data.countries[i].id+"' selected>"+data.countries[i].country+"</option>";
            }else{
                options += "<option value='"+data.countries[i].id+"'>"+data.countries[i].country+"</option>";
            }

        }
        $('select[name=country]').html("<option>All</option>"+options);
    });
}

function populateState(element) {
    let options;
    $.get('/getStates', function (data) {
        for(let i=0; i < data.states.length; i++){
            options += "<option value='"+data.states[i].id+"'>"+data.states[i].state+"</option>";
        }
        element.html("<option>All</option>"+options);
    });
}

function generateAge(){
    var year = 1950;
    var till = (new Date()).getFullYear();
    var options = "";
    for(var y=till-year; y>=1; y--){
        options += "<option value="+ y +">"+ y +"</option>";
    }

    //return console.log(options);
    // if($('select[name=ageFrom]').length > 0){
    //     $('select[name=ageFrom]').each(function () {
            $('select[name=ageFrom]').html("<option>All</option>" + options);
        // })
    // }

    // if($('select[name=ageTo]').length > 0){
    //     $('select[name=ageTo]').each(function () {
            $('select[name=ageTo]').html("<option>All</option>" + options);
        // });
    // }

}

$('#filter').on('click', function () {
    let ths = $(this);
    let country = $('select[name=country] option:selected').val();
    let region =  $('select[name=region] option:selected').val();
    let state =  $('select[name=state] option:selected').val();
    let  ageTo =  $('select[name=ageTo] option:selected').val();
    let  ageFrom =  $('select[name=ageFrom] option:selected').val();
    let  discipline =  $('input[name=discipline]').val();

    var toSend = {'country':country, 'state':state, 'region':region, 'discipline':discipline, 'ageTo':ageTo, 'ageFrom': ageFrom, 'candidateType': 'tested'};
    console.log(toSend);
    $.ajax({
        type: "POST",
        url: "/filterCandidates",
        data: toSend,
        contentType: "application/json; charset=utf-8",
        //dataType: "json",
        beforeSend: function () {
            ths.attr('disabled', true);
            ths.text('..processing');
        },
        success: function (response) {
            return console.log(response);
            if(response.questions.length > 0){
                populateDataTable(response, qtype);
            }else{
                //$("#table-1").DataTable().clear();
                $('#success').modal('toggle');
                $('#msg').empty();
                $('#msg').append('No data returned for this search.');
                //$(window).scrollTop(0);
            }
            ths.attr('disabled', false);
            ths.text('Filter');
            $(window).scrollTop(200);
        }
    });
});



$('#filterRecommended').on('click', function () {
    "use strict";
    let ths = $(this);
    let client = $('select[name=client] option:selected').val();
    let job = $('select[name=jOpening] option:selected').val();
    var toSend = {'client': client, 'job': job};
    console.log(toSend);
    $.ajax({
        type: "POST",
        url: "/filterRecommended",
        data: toSend,
        //contentType: "application/json; charset=utf-8",
        //dataType: "json",
        beforeSend: function () {
            ths.attr('disabled', true);
            ths.text('..processing');
        },
        success: function (response) {
            ths.attr('disabled', false);
            ths.text('Filter');
            console.log(response);
            if(response.rcandidates.length > 0){
                populateDataTable(response);
            }else{
                $('#success').modal('toggle');
                $('#successmsg').append('No result(s) returned');
            }
            $(window).scrollTop(200);

        }
    });
});

$('#filterApproved').on('click', function () {
    "use strict";
    let ths = $(this);
    let client = $('select[name=client] option:selected').val();
    let job = $('select[name=jOpening] option:selected').val();
    var toSend = {'client': client, 'job': job};
    console.log(toSend);
    $.ajax({
        type: "POST",
        url: "/filterApproved",
        data: toSend,
        //contentType: "application/json; charset=utf-8",
        //dataType: "json",
        beforeSend: function () {
            ths.attr('disabled', true);
            ths.text('..processing');
        },
        success: function (response) {
            ths.attr('disabled', false);
            ths.text('Filter');
            console.log(response);
            if(response.rcandidates.length > 0){
                populateDataTable(response);
            }else{
                $('#success').modal('toggle');
                $('#successmsg').append('No result(s) returned');
            }
            $(window).scrollTop(200);

        }
    });
});

$('#filterCandidates').on('click', function () {
    "use strict";
    let ths = $(this);
    let country = $('select[name=country] option:selected').val();
    let state = $('select[name=state] option:selected').val();
    let region = $('select[name=region] option:selected').val();
    let agestart = $('select[name=ageFrom] option:selected').val();
    let ageend = $('select[name=ageTo] option:selected').val();
    let discipline = $('input[name=discipline]').val();
    var toSend = {'country': country, 'region':region, 'discipline': discipline, 'agestart': agestart, 'ageend':ageend, 'state':state};
    console.log(toSend);
    $.ajax({
        type: "POST",
        url: "/filterAll",
        data: toSend,
        //contentType: "application/json; charset=utf-8",
        //dataType: "json",
        beforeSend: function () {
            ths.attr('disabled', true);
            ths.text('..processing');
        },
        success: function (response) {
            ths.attr('disabled', false);
            ths.text('Filter');
            console.log(response);
            if(response.rcandidates.length > 0){
                populateDataTable2(response);
            }else{
                //$("#table-1").DataTable().clear();
                $('#success').modal('toggle');
                // $('#successmsg').empty();
                $('#successmsg').html('No data returned for this search.');
                //$(window).scrollTop(0);
            }
            ths.attr('disabled', false);
            ths.text('Filter');
            $(window).scrollTop(200);
        }
    });
});

$('#filterTested').on('click', function () {
    "use strict";
    let ths = $(this);
    let country = $('select[name=country] option:selected').val();
    let agestart = $('select[name=ageFrom] option:selected').val();
    let ageend = $('select[name=ageTo] option:selected').val();
    let region = $('select[name=region] option:selected').val();
    let state = $('select[name=state] option:selected').val();
    let  discipline =  $('input[name=discipline]').val();
    let  educationLevel =  $('select[name=educationLevel]').val();
    var toSend = {'country': country, 'discipline': discipline, 'state':state, 'region':region, 'agestart': agestart, 'ageend':ageend, 'educationLevel': educationLevel};
    console.log(toSend);
    $.ajax({
        type: "POST",
        url: "/filterTested",
        data: toSend,
        //contentType: "application/json; charset=utf-8",
        //dataType: "json",
        beforeSend: function () {
            ths.attr('disabled', true);
            ths.text('..processing');
        },
        success: function (response) {
            ths.attr('disabled', false);
            ths.text('Filter');
            console.log(response);
            if(response.rcandidates.length > 0){
                populateDataTable(response);
            }else{
                //$("#table-1").DataTable().clear();
                $('#success').modal('toggle');
                // $('#successmsg').empty();
                $('#successmsg').html('No data returned for this search.');
                //$(window).scrollTop(0);
            }
            ths.attr('disabled', false);
            ths.text('Filter');
            $(window).scrollTop(200);
        }
    });
});


$('#filterAccepted').on('click', function () {
    "use strict";
    let ths = $(this);
    let client = $('select[name=client] option:selected').val();
    let job = $('select[name=jOpening] option:selected').val();
    var toSend = {'client': client, 'job': job};
    console.log(toSend);
    $.ajax({
        type: "POST",
        url: "/filterAccepted",
        data: toSend,
        //contentType: "application/json; charset=utf-8",
        //dataType: "json",
        beforeSend: function () {
            ths.attr('disabled', true);
            ths.text('..processing');
        },
        success: function (response) {
            ths.attr('disabled', false);
            ths.text('Filter');
            console.log(response);
            if(response.rcandidates.length > 0){
                populateDataTable3(response);
            }else{
                $('#success').modal('toggle');
                $('#successmsg').append('No result(s) returned');
            }
            $(window).scrollTop(200);

        }
    });
});

function populateDataTable(data) {

    $("#loadStudents").DataTable().clear();
    var length = Object.keys(data.rcandidates).length;

        for(var i = 0; i < length; i++) {
            var candidate = data.rcandidates[i];

            // You could also use an ajax property on the data table initialization
            $('#loadStudents').dataTable().fnAddData( [
                candidate.firstname,
                candidate.lastname,
                new Date().getFullYear() - new Date(candidate.date_of_birth).getFullYear(),
                candidate.result,
                "<a href='/viewCandidateProfile/"+candidate.user_id+"' class='btn btn-sm btn-info'>Profile</a> "+(candidate.resume != null ? "<a href='/resume/"+candidate.user_id+"/"+candidate.resume+"' target='_blank' class='btn btn-sm btn-info'>CV</a>":''),
                "<input type='checkbox' data-candidate='"+candidate.user_id+"' class='selectMultiple'>"
            ]);
        }
}

function populateDataTable2(data) {

    $("#loadStudents").DataTable().clear();
    var length = Object.keys(data.rcandidates).length;

    for(var i = 0; i < length; i++) {
        var candidate = data.rcandidates[i];

        // You could also use an ajax property on the data table initialization
        $('#loadStudents').dataTable().fnAddData( [
            candidate.firstname,
            candidate.lastname,
            new Date().getFullYear() - new Date(candidate.date_of_birth).getFullYear(),
            "<a href='/viewCandidateProfile/"+candidate.user_id+"' class='btn btn-sm btn-info'>Profile</a>"+(candidate.resume != null ? "<a href='/resume/"+candidate.user_id+"/"+candidate.resume+"' target='_blank' class='btn btn-sm btn-info'>CV</a>":''),
            "<input type='checkbox' data-candidate='"+candidate.user_id+"' class='selectMultiple'>"
        ]);
    }
}

function populateDataTable3(data) {

    $("#loadStudents").DataTable().clear();
    var length = Object.keys(data.rcandidates).length;

    for(var i = 0; i < length; i++) {
        var candidate = data.rcandidates[i];

        // You could also use an ajax property on the data table initialization
        $('#loadStudents').dataTable().fnAddData( [
            candidate.firstname,
            candidate.lastname,
            new Date().getFullYear() - new Date(candidate.date_of_birth).getFullYear(),
            candidate.result,
            "<a href='/viewCandidateProfile/"+candidate.user_id+"' class='btn btn-sm btn-info'>Profile</a>"+(candidate.resume != null ? "<a href='/resume/"+candidate.user_id+"/"+candidate.resume+"' target='_blank' class='btn btn-sm btn-info'>CV</a>":''),
        ]);
    }
}

$('#filterApplied').on('click', function () {
    "use strict";
    let ths = $(this);
    let client = $('select[name=client] option:selected').val();
    let job = $('select[name=jOpening] option:selected').val();
    let  discipline =  $('input[name=discipline]').val();
    var toSend = {'client': client, 'job': job, 'discipline':discipline};
    console.log(toSend);
    $.ajax({
        type: "POST",
        url: "/filterRecommended",
        data: toSend,
        //contentType: "application/json; charset=utf-8",
        //dataType: "json",
        beforeSend: function () {
            ths.attr('disabled', true);
            ths.text('..processing');
        },
        success: function (response) {
            ths.attr('disabled', false);
            ths.text('Filter');
            console.log(response);
            if(response.rcandidates.length > 0){
                populateDataTable(response);
            }else{
                $('#success').modal('toggle');
                $('#successmsg').append('No result(s) returned');
            }
            $(window).scrollTop(200);

        }
    });
});