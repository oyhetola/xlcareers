const personalInfoSection = {};
const educationSection = {};
const experienceSection = {};
const resumeSection = {};

$('#submitPersonalInfo').on('click', function () {
    let singleInfo = {};
    let ths = $(this);
    let nextOfKin = $('.personalInfo').find('input[name=nextOfKin]').val();
    let nextOfKinPhoneNum = $('.personalInfo').find('input[name=nextOfKinPhoneNum]').val();
    let nextOfKinAddress = $('.personalInfo').find('textarea[name=nextOfKinAddress]').val();
    let address = $('.personalInfo').find('textarea[name=address]').val();
    let phoneNumber = $('.personalInfo').find('input[name=phone-number]').val();
    let country = $('.personalInfo').find('select[name=country]').val();
    let stateOfOrigin = $('.personalInfo').find('select[name=stateoforigin]').val();
    let countryOfResidence = $('.personalInfo').find('select[name=countryOfResidence]').val();
    let stateOfResidence = $('.personalInfo').find('select[name=stateofresidence]').val();
    let dateOfBirth = $('.personalInfo').find('input[name=dateOfBirth]').val();
    let regionOfResidence;
    if($('select[name=regionOfResidence]').length > 0){
        regionOfResidence = $('.personalInfo').find('select[name=regionOfResidence]').val();
    }
    if(regionOfResidence != null){
        singleInfo['regionOfResidence'] = regionOfResidence;
    }


    singleInfo['nextOfKin'] = nextOfKin;
    singleInfo['dateOfBirth'] = dateOfBirth;
    singleInfo['nextOfKinPhoneNum'] = nextOfKinPhoneNum;
    singleInfo['nextOfKinAddress'] = nextOfKinAddress;
    singleInfo['address'] = address;
    singleInfo['phoneNumber'] = phoneNumber;
    singleInfo['country'] = country;
    singleInfo['stateOfOrigin'] = stateOfOrigin;
    singleInfo['countryOfResidence'] = countryOfResidence;
    singleInfo['stateOfResidence'] = stateOfResidence;

    console.log(singleInfo);
    ths.attr('disabled', true);
    $.ajax({
        type: 'POST',
        url: "/submitPersonalInfo",
        data: singleInfo,
        success:function (data) {
            console.log(data);
            ths.attr('disabled', false);
            if(data.phoneNumber){
                $('#error').modal('toggle');
                let msg = "<p>"+data.phoneNumber[0]+".</p>";
                $('#message').html(msg);
            }else if(data.country){
                $('#error').modal('toggle');
                let msg = "<p>"+data.country[0]+".</p>";
                $('#message').html(msg);
            }else if(data.address){
                $('#error').modal('toggle');
                let msg = "<p>"+data.address[0]+".</p>";
                $('#message').html(msg);
            }else if(data.countryOfResidence){
                $('#error').modal('toggle');
                let msg = "<p>"+data.countryOfResidence[0]+".</p>";
                $('#message').html(msg);
            }else if(data.nextOfkinPhoneNum){
                $('#error').modal('toggle');
                let msg = "<p>"+data.nextOfkinPhoneNum[0]+".</p>";
                $('#message').html(msg);
            }else if(data.nextOfkinAddress){
                $('#error').modal('toggle');
                let msg = "<p>"+data.nextOfkinAddress[0]+".</p>";
                $('#message').html(msg);
            }else if(data.nextOfkin){
                $('#error').modal('toggle');
                let msg = "<p>"+data.nextOfkin[0]+".</p>";
                $('#message').html(msg);
            }else if(data.dateOfBirth){
                $('#error').modal('toggle');
                let msg = "<p>"+data.dateOfBirth[0]+".</p>";
                $('#message').html(msg);
            }else if(data.error){
                $('#error').modal('toggle');
                let msg = "<p>"+data.error+".</p>";
                $('#message').html(msg);
            }else if(data.success){
                $('#success').modal('toggle');
                let msg = "<p>Your Personal information have been successfully uploaded.</p> <a href='/educationDetails'>Fill in your education details.</a>";
                $('#successmsg').html(msg);
                $('.pinfo').remove();
                $('#personalInfoModal').modal('toggle');
            }
        },
        error: function (error){
            console.log(error);
            ths.attr('disabled', false);
            $('#error').modal('toggle');
            let msg = "<p>"+error.error+".</p>";
            $('#message').html(msg);
        }
    });
});

$('#submitEducationInfo').on('click', function () {
    let singleInfo = {};
    let ths =$(this);
    let schoolName = $('.singleEducationDetails').find('input[name=nameOfSchool]').val();
    let schoolCountry = $('.singleEducationDetails').find('select[name=schoolCountry]').val();
    let schoolState = $('.singleEducationDetails').find('select[name=schoolState]').val();
    let schoolStartMonth = $('.singleEducationDetails').find('select[name=schoolStartMonth]').val();
    let schoolEndMonth = $('.singleEducationDetails').find('select[name=schoolEndMonth]').val();
    let schoolStartYear = $('.singleEducationDetails').find('select[name=schoolStartYear]').val();
    let schoolEndYear = $('.singleEducationDetails').find('select[name=schoolEndYear]').val();
    let currentStudyInstitution = $('.singleEducationDetails').find('input[name=currentStudyInstitution]').val();
    let studyField = $('.singleEducationDetails').find('input[name=studyField]').val();
    let educationLevel = $('.singleEducationDetails').find('select[name=educationLevel]').val();
    let regionOfResidence = $('.singleEducationDetails').find('select[name=regionOfResidence]').val();

    singleInfo['schoolName'] = schoolName;
    singleInfo['schoolCountry'] = schoolCountry;
    singleInfo['schoolState'] = schoolState;
    singleInfo['schoolStartMonth'] = schoolStartMonth;
    singleInfo['schoolEndMonth'] = schoolEndMonth;
    singleInfo['schoolStartYear'] = schoolStartYear;
    singleInfo['schoolEndYear'] = schoolEndYear;
    singleInfo['currentStudyInstitution'] = currentStudyInstitution;
    singleInfo['fieldOfStudy'] = studyField;
    singleInfo['educationLevel'] = educationLevel;
    singleInfo['regionOfResidence'] = regionOfResidence;
    if($('input[name=currentStudyInstitution]').is(":checked")){
        singleInfo['current'] = 1;
    }
    else if($('input[name=currentStudyInstitution]').is(":not(:checked)")){
        singleInfo['current'] = 0;
    }

    ths.attr('disabled', true);
    $.ajax({
        type: 'POST',
        url: "/submitEducationInfo",
        data: singleInfo,
        success:function(data){
            console.log(data);
            ths.attr('disabled', false);
            if(data.schoolName){
                $('#error').modal('toggle');
                let msg = "<p>"+data.schoolName[0]+".</p>";
                $('#message').html(msg);
            }else if(data.schoolCountry){
                $('#error').modal('toggle');
                let msg = "<p>"+data.schoolCountry[0]+".</p>";
                $('#message').html(msg);
            }else if(data.schoolStartMonth){
                $('#error').modal('toggle');
                let msg = "<p>"+data.schoolStartMonth[0]+".</p>";
                $('#message').html(msg);
            }else if(data.schoolStartYear){
                $('#error').modal('toggle');
                let msg = "<p>"+data.schoolStartYear[0]+".</p>";
                $('#message').html(msg);
            }else if(data.fieldOfStudy){
                $('#error').modal('toggle');
                let msg = "<p>"+data.fieldOfStudy[0]+".</p>";
                $('#message').html(msg);
            }else if(data.error){
                $('#error').modal('toggle');
                let msg = "<p>"+data.error+".</p>";
                $('#message').html(msg);
            }else  if(data.success){
                $('#success').modal('toggle');
                let msg = "<p>Education history successfully uploaded.</p> <a href='/experienceDetails'>Fill in your work experience</a>";
                $('#successmsg').html(msg);
                $('.einfo').remove();
                $('#educationInfoModal').modal('toggle');
                // location.reload();
                $('.takemediaEdu').
                append('<div class="media">\
                            <div class="experience-logo">\
                                <i class="icon ion-briefcase"></i>\
                            </div><!-- experience-logo -->\
                            <div class="media-body">\
                                <h6 class="position-name">'+data.success.info.school_name+'</h6>\
                                <p class="position-company">'+data.success.info.fieldOfStudy+'</p>\
                                <p class="position-year">'+data.success.info.schoolStart_month+' '+ data.success.info.schoolStart_year +' - '+(data.success.info.schoolEnd_month != null && data.success.info.schoolEnd_year != null ? data.success.info.schoolEnd_month + "  " +data.success.info.schoolEnd_year: "- present")+'&nbsp;-&nbsp;\
                                    <a class="editEducationpop" data-toggle="modal" data-target="#editExperience" data-id="'+data.success.info.id+'" data-school="'+data.success.info.school_name+'" data-schoolstartmonth="'+data.success.info.schoolStart_month+'"\
                data-schoolstartyear="'+data.success.info.schoolStart_year+'" data-schoolendmonth="'+data.success.info.schoolEnd_month+'" data-schoolendyear="'+data.success.info.schoolEnd_year+'"\
                data-country="'+data.success.info.schoolCountry_id+'" data-state="'+data.success.info.schoolState_id+'" data-field="'+data.success.info.fieldOfStudy+'" data-education="'+data.success.info.educationLevel+'">Edit</a>\
                                </p>\
                            </div><!-- media-body -->\
                        <div>');
                $('.singleEducationDetails').find('input').val('');
            
            }
        },
        error: function (error) {
            console.log(error);
            ths.attr('disabled', false);
        }
    });
});
$('#submitExperienceInfo').on('click', function () {
    let singleInfo = {};
    let ths = $(this);
    let common = $('.singleJobExperience');
    let companyName = common.find('input[name=companyName]').val();
    let jobTitle = common.find('input[name=jobTitle]').val();
    let currentWorkPlace = common.find('input[name=currentWorkPlace]').val();
    let workStartMonth = common.find('select[name=workStartMonth]').val();
    let workStartYear = common.find('select[name=workStartYear]').val();
    let workEndMonth = common.find('select[name=workEndMonth]').val();
    let workEndYear = common.find('select[name=workEndYear]').val();
    let jobDescription = common.find('textarea[name=jobDescription]').val();

    if($('input[name=currentWorkPlace]').is(":checked")){
        singleInfo['current'] = 1;
    }
    else if($('input[name=currentWorkPlace]').is(":not(:checked)")){
        singleInfo['current'] = 0;
    }


    singleInfo['companyName'] = companyName;
    singleInfo['jobTitle'] = jobTitle;
    singleInfo['currentWorkPlace'] = currentWorkPlace;
    singleInfo['workStartMonth'] = workStartMonth;
    singleInfo['workStartYear'] = workStartYear;
    singleInfo['workEndMonth'] = workEndMonth;
    singleInfo['workEndYear'] = workEndYear;
    singleInfo['jobDescription'] = jobDescription;

    ths.attr('disabled', true);
    $.ajax({
        type: 'POST',
        url: "/submitExperienceInfo",
        data: singleInfo,
        success:function (data) {
            console.log(data);
            ths.attr('disabled', false);
            if(data.companyName){
                $('#error').modal('toggle');
                let msg = "<p>"+data.companyName[0]+".</p>";
                $('#message').html(msg);
            }else if(data.jobTitle){
                $('#error').modal('toggle');
                let msg = "<p>"+data.jobTitle[0]+".</p>";
                $('#message').html(msg);
            }else if(data.workStartMonth){
                $('#error').modal('toggle');
                let msg = "<p>"+data.workStartMonth[0]+".</p>";
                $('#message').html(msg);
            }else if(data.workStartYear){
                $('#error').modal('toggle');
                let msg = "<p>"+data.workStartYear[0]+".</p>";
                $('#message').html(msg);
            }else if(data.error){
                $('#error').modal('toggle');
                let msg = "<p>"+data.error+".</p>";
                $('#message').html(msg);
            }else if(data.success){
                $('#success').modal('toggle');
                let msg = "<p>Work Experience status successfully uploaded.</p>";
                $('#successmsg').html(msg);
                $('.exinfo').remove();
                $('#experienceInfoModal').modal('toggle');
                $('.takemediaExp').
                append('<div class="media">\
                            <div class="experience-logo">\
                                <i class="icon ion-briefcase"></i>\
                            </div><!-- experience-logo -->\
                            <div class="media-body">\
                                <h6 class="position-name">'+data.success.info.company_name+'</h6>\
                                <p class="position-company">'+data.success.info.job_title+'</p>\
                                <p class="position-year">'+data.success.info.workStart_month+' '+ data.success.info.workStart_year +' - '+(data.success.info.workEnd_month != null && data.success.info.workEnd_year != null ? data.success.info.workEnd_month + "  " +data.success.info.workEnd_year: "- present")+'&nbsp;-&nbsp;\
                                    <a class="editexperiencepop" data-toggle="modal" data-target="#editExperience" data-id="'+data.success.info.id+'" data-title="+jobTitle+" data-company=""\
                data-workstartmonth="'+data.success.info.workStart_month+'" data-workendmonth="'+data.success.info.workEnd_month+'" data-workstartyear="'+data.success.info.workStart_year+'"\
                data-workendyear="'+data.success.info.workEnd_year+'" data-description="'+data.success.info.description+'" data-current="'+data.success.info.currentWorkPlace+'">Edit</a>\
                                </p>\
                            </div><!-- media-body -->\
                        <div>');
                common.find('input').val('');
            }
        },
        error: function (error) {
            console.log(error);
            ths.attr('disabled', false);
        }
    });
});
$('#submitIt').on('click', function () {
    //*Get the values of all inputs in the personal Info section
    // */
    let firstname = $('.personalInfo').find('input[name=firstname]').val();
    let lastname = $('.personalInfo').find('input[name=lastname]').val();
    let email = $('.personalInfo').find('input[name=email]').val();
    let phoneNumber = $('.personalInfo').find('input[name=phone-number]').val();
    let country = $('.personalInfo').find('select[name=country]').val();
    let stateOfOrigin = $('.personalInfo').find('select[name=stateoforigin]').val();
    let countryOfResidence = $('.personalInfo').find('select[name=countryOfResidence]').val();
    let stateOfResidence = $('.personalInfo').find('select[name=stateofresidence]').val();

    /*assign the above values to the object created to hold its data;
    * */

    personalInfoSection['firstname'] = firstname;
    personalInfoSection['lastname'] = lastname;
    personalInfoSection['email'] = email;
    personalInfoSection['phoneNumber'] = phoneNumber;
    personalInfoSection['country'] = country;
    personalInfoSection['stateOfOrigin'] = stateOfOrigin;
    personalInfoSection['countryOfResidence'] = countryOfResidence;
    personalInfoSection['stateOfResidence'] = stateOfResidence;

    /*Get the values in the education section
    * There might be more than one education form to be submitted hence
    * we do a loop through
    * */

    $('.singleEducationDetails').each(function () {
        let singleInfo = {};
        let schoolName = $(this).find('input[name=nameOfSchool]').val();
        let schoolCountry = $(this).find('select[name=schoolCountry]').val();
        let schoolState = $(this).find('select[name=schoolState]').val();
        let schoolStartMonth = $(this).find('select[name=schoolStartMonth]').val();
        let schoolEndMonth = $(this).find('select[name=schoolEndMonth]').val();
        let schoolStartYear = $(this).find('select[name=schoolStartYear]').val();
        let schoolEndYear = $(this).find('select[name=schoolEndYear]').val();
        let currentStudyInstitution = $(this).find('input[name=currentStudyInstitution]').val();
        let studyField = $(this).find('input[name=studyField]').val();
        let educationLevel = $(this).find('select[name=educationLevel]').val();

        singleInfo['schoolName'] = schoolName;
        singleInfo['schoolCountry'] = schoolCountry;
        singleInfo['schoolState'] = schoolState;
        singleInfo['schoolStartMonth'] = schoolStartMonth;
        singleInfo['schoolEndMonth'] = schoolEndMonth;
        singleInfo['schoolStartYear'] = schoolStartYear;
        singleInfo['schoolEndYear'] = schoolEndYear;
        singleInfo['currentStudyInstitution'] = currentStudyInstitution;
        singleInfo['studyField'] = studyField;
        singleInfo['educationLevel'] = educationLevel;

        let educationDetailsLength = Object.keys(educationSection).length;

        educationSection[educationDetailsLength] = singleInfo;

    });


    $('.singleJobExperience').each(function () {
        let singleInfo = {};
        let companyName = $(this).find('input[name=companyName]').val();
        let jobTitle = $(this).find('input[name=jobTitle]').val();
        let currentWorkPlace = $(this).find('input[name=currentWorkPlace]').val();
        let workStartMonth = $(this).find('select[name=workStartMonth]').val();
        let workStartYear = $(this).find('select[name=workStartYear]').val();
        let workEndMonth = $(this).find('select[name=workEndMonth]').val();
        let workEndYear = $(this).find('select[name=workEndYear]').val();
        let jobDescription = $(this).find('textarea[name=jobDescription]').val();


        singleInfo['companyName'] = companyName;
        singleInfo['jobTitle'] = jobTitle;
        singleInfo['currentWorkPlace'] = currentWorkPlace;
        singleInfo['workStartMonth'] = workStartMonth;
        singleInfo['workStartYear'] = workStartYear;
        singleInfo['workEndMonth'] = workEndMonth;
        singleInfo['workEndYear'] = workEndYear;
        singleInfo['jobDescription'] = jobDescription;


        let experienceDetailsLength = Object.keys(experienceSection).length;

        experienceSection[experienceDetailsLength] = singleInfo;

    });

    console.log({personalInfoSection: personalInfoSection, educationSection: educationSection, addExperience: experienceSection});

});


$('form#resumeForm').on('submit', function(e) {
    e.preventDefault();
    let formData = new FormData(this);
    console.log(formData);
    $.ajax({
        type: 'POST',
        url: "/submitResume",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function (){
            $('#uploadResume').attr("disabled", "disabled");
            $('#resumeForm').css("opacity", ".5");
        },
        success:function (data) {
            console.log(data);
            $('#uploadResume').removeAttr("disabled");
            $('#resumeForm').css("opacity", "");
            //ths.attr('disabled', false);
            if(data.nofileError){
                $('#error').modal('toggle');
                let msg = "<p>"+data.nofileError+".</p>";
                $('#message').html(msg);
            }else if(data.formatError){
                $('#error').modal('toggle');
                let msg = "<p>"+data.formatError+".</p>";
                $('#message').html(msg);
            }else if(data.error){
                $('#error').modal('toggle');
                let msg = "<p>"+data.error+".</p>";
                $('#message').html(msg);
            }else if(data.success){
                $('#success').modal('toggle');
                let msg = "<p>"+data.success+"</p>";
                $('#successmsg').html(msg);
                $('#resumeForm').remove();
                //location.reload();
                /* $('.takemediaExp').
                append('<div class="media">\
                            <div class="experience-logo">\
                                <i class="icon ion-briefcase"></i>\
                            </div><!-- experience-logo -->\
                            <div class="media-body">\
                                <h6 class="position-name">'+data.success.info.+'</h6>\
                                <p class="position-company">'+data.success.info.job_title+'</p>\
                                <p class="position-year">'+data.success.info.workStart_month+' '+ data.success.info.workStart_year +' - '+(data.success.info.workEnd_month != null && data.success.info.workEnd_year != null ? data.success.info.workEnd_month + "  " +data.success.info.workEnd_year: "- present")+'&nbsp;-&nbsp;\
                                    <a class="editexperiencepop" data-toggle="modal" data-target="#editExperience" data-id="'+data.success.info.id+'" data-title="+jobTitle+" data-company=""\
                data-workstartmonth="'+data.success.info.workStart_month+'" data-workendmonth="'+data.success.info.workEnd_month+'" data-workstartyear="'+data.success.info.workStart_year+'"\
                data-workendyear="'+data.success.info.workEnd_year+'" data-description="'+data.success.info.description+'" data-current="'+data.success.info.currentWorkPlace+'">Edit</a>\
                                </p>\
                            </div><!-- media-body -->\
                        <div>'); */
            }
        },
        error: function (error) {
            console.log(error);
            ths.attr('disabled', false);
        }
    });
});
