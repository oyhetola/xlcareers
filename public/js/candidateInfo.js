const  newElem = {
    newEducation
:
    '<div class="singleEducationDetails">\
    <hr class="mt-4">\
    <div class="form-group">\
        <label>School</label>\
        <input type="text" name="nameOfSchool" class="form-control" placeholder="Enter name of school">\
        </div>\
        <div class="form-group" >\
        <label>Country</label>\
        <select name="schoolCountry" class="form-control">\
        <option></option>\
        </select>\
        </div>\
        <div class="form-group" >\
        <label>State</label>\
        <select name="schoolState" class="form-control">\
        <option></option>\
        </select>\
        </div>\
        <div class="row">\
        <div class="col-sm-6">\
        <div class="form-group" >\
        <label>From</label>\
        <select name="schoolStartMonth" class="form-control">\
        <option></option>\
        </select>\
        <select name="schoolStartYear" class="form-control">\
        <option></option>\
        </select>\
        </div>\
        </div>\
        <div class="col-sm-6">\
        <div class="form-group" >\
        <label>To</label>\
        <select name="schoolEndMonth" class="form-control">\
        <option></option>\
        </select>\
        <select name="schoolEndYear" class="form-control">\
        <option></option>\
        </select>\
        </div>\
        </div>\
        <div class="form-group" >\
        <label></label>\
        <input name="currentStudyInstitution" type="checkbox">I currently study here\
    </div>\
    </div>\
    <div class="form-group" >\
    <label>Field of Study</label>\
<input type="text" name="studyField" placeholder="e.g. Sciences, Arts, Engineering, Pharmacy, " class="form-control">\
    </div>\
    <div class="form-group mb-4">\
    <label>Level of Education</label>\
<select name="educationLevel" class="form-control">\
    <option selected disabled>Select</option>\
<option>School Leaving Certificate</option>\
<option>Diploma</option>\
<option>Degree</option>\
<option>Graduate Diploma</option>\
<option>Second Degree</option>\
<option>Doctorate Degree</option>\
</select>\
</div>\
</div>\
    <div><p class="deleteEducation"><i class="fa fa-remove"></i> Delete Education</p></div>\
    ',

    addExperience: '<div class="singleJobExperience">\
    <hr class="mt-4">\
    <div class="form-group">\
        <label>Company Name</label>\
    <input type="text" name="companyName" class="form-control" placeholder="Company Name">\
    </div>\
    <div class="form-group">\
    <label>Job Title</label>\
<input type="text" name="jobTitle" class="form-control" placeholder="Role">\
    </div>\
    <div class="row">\
    <div class="col-sm-12">\
    <div class="form-group" >\
    <label></label>\
    <input type="checkbox" name="currentWorkPlace">I currently work here\
</div>\
</div>\
<div class="col-sm-6">\
    <div class="form-group">\
    <label>From</label>\
    <select name="workStartMonth" class="form-control">\
    <option></option>\
    </select>\
    <select name="workStartYear" class="form-control">\
    <option></option>\
    </select>\
    </div>\
    </div>\
    <div class="col-sm-6">\
    <div class="form-group">\
    <label>To</label>\
    <select name="workEndMonth" class="form-control">\
    <option></option>\
    </select>\
    <select name="workEndYear" class="form-control">\
    <option></option>\
    </select>\
    </div>\
    </div>\
    </div>\
    <div class="form-group mb-4">\
    <label>Description</label>\
    <textarea rows="5" name="jobDescription" class="form-control"></textarea>\
    </div>\
    </div>\
    <div><p class="deleteExperience"><i class="fa fa-remove"></i> Delete Education</p></div>'
}


$('.addEducationButton').on('click', function () {
    // alert();
    $('#educationDetails').append(newElem.newEducation);
    generateYears();
    populateMonth()
});

$(document).on('click', '.deleteEducation', function(){
    //alert();
    $(this).parent().prev().remove();
    $(this).remove();
});

$('.addExperienceButton').on('click', function () {
    // alert();
    $('#addExperience').append(newElem.addExperience);
    generateYears();
    populateMonth();
});


$(document).on('click', '.deleteExperience', function(){
    $(this).parent().prev().remove();
    $(this).remove();
});


function generateYears() {
    var year = 1934;
    var till = (new Date()).getFullYear();
    var options = "";
    for(var y=year; y<=till; y++){
        options += "<option value="+ y +">"+ y +"</option>";
    }

    if($('select[name=schoolStartYear]').length > 0){
        $('select[name=schoolStartYear]').each(function () {
            $(this).html("<option selected disabled value='select'>Select Year</option>" + options);
        })
    }

    if($('select[name=schoolEndYear]').length > 0){
        $('select[name=schoolEndYear]').each(function () {
            $(this).html("<option selected disabled value='select'>Select Year</option>" + options);
        });
    }

    if($('select[name=workStartYear]').length > 0){
        $('select[name=workStartYear]').each(function () {
            $(this).html("<option selected disabled value='select'>Select Year</option>" + options);
        });
    }

    if($('select[name=workEndYear]').length > 0){
        $('select[name=workEndYear]').each(function (){
            $(this).html("<option selected disabled value='select'>Select Year</option>" + options);
        });
    }
}

function populateCountry() {
    let options;
    $.get('/getCountries', function (data) {
        for(let i=0; i < data.countries.length; i++){
            if(data.countries[i].country == "Nigeria"){
                options += "<option value='"+data.countries[i].id+"' selected>"+data.countries[i].country+"</option>";
            }else{
                options += "<option value='"+data.countries[i].id+"'>"+data.countries[i].country+"</option>";
            }

        }
        $('select[name=country]').html("<option disabled>Select Country</option>"+options);
        $('select[name=countryOfResidence]').html("<option disabled>Select Country</option>"+options);
        $('select[name=schoolCountry]').html("<option disabled>Select Country</option>"+options);
    });
}

function populateState(element) {
    let options;
    $.get('/getStates', function (data) {
        for(let i=0; i < data.states.length; i++){
            options += "<option value='"+data.states[i].id+"'>"+data.states[i].state+"</option>";
        }
        element.html("<option disabled selected>Select State</option>"+options);
    });
}

$('select[name=countryOfResidence]').on('change', function () {
    if($('select[name=countryOfResidence] option:selected').text() == "NigeriaNigeria"){
        $('select[name=stateofresidence]').prop('disabled', false);
        populateState($('select[name=stateofresidence]'))
    }else{
        $('select[name=stateofresidence]').html('');
        $('select[name=stateofresidence]').prop('disabled', true);
    }
});

/*$('select[name=countryOfResidence]').on('change', function () {
    if($('select[name=countryOfResidence] option:selected').text() == "Nigeria"){
        $('select[name=stateofresidence]').attr('disabled', false);
        populateState($('select[name=stateofresidence]'));
        $('.regn').removeClass('activeQuestion');
        $('.regn').addClass('activeQuestion');
    }else{
        $('select[name=stateofresidence]').html('');
        $('select[name=stateofresidence]').attr('disabled', true);
        $('.regn').addClass('hideThisQuestion');
    }
});*/


function populateMonth(){
    let months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September',
                  'October', 'November', 'December'];
    let options = "";
    for (var i=0; i<months.length; i++){
        options += "<option>"+ months[i]+"</option>";
    }

    if($('select[name=workStartMonth]').length > 0){
        $('select[name=workStartMonth]').each(function (){
            $(this).html("<option selected disabled value='select'>Select Month</option>" + options);
        });
    }

    if($('select[name=workEndMonth]').length > 0){
        $('select[name=workEndMonth]').each(function (){
            $(this).html("<option selected disabled value='select'>Select Month</option>" + options);
        });
    }

    if($('select[name=schoolStartMonth]').length > 0){
        $('select[name=schoolStartMonth]').each(function (){
            $(this).html("<option selected disabled value='select'>Select Month</option>" + options);
        });
    }

    if($('select[name=schoolEndMonth]').length > 0){
        $('select[name=schoolEndMonth]').each(function (){
            $(this).html("<option selected disabled value='select'>Select Month</option>" + options);
        });
    }
}


///validations

$('select[name=workEndYear]').on('change', function () {
    let prevSelection = $('select[name=workStartYear]').val();
    let newSelection = $(this).val();
    if(prevSelection > newSelection){
        alert('End year is invalid, Please change the work end year');
        $('select[name=workEndYear] option').attr('selected', false);
        $('select[name=workEndYear] option').filter('[value=select]')
            .attr('selected', true);
    }
});


$('select[name=workStartYear]').on('change', function () {
    let prevSelection = $('select[name=workEndYear]').val();
    let newSelection = $(this).val();
    if( prevSelection != null && prevSelection !="select" ){
        if(prevSelection < newSelection){
            alert('End year is invalid, Please change the work end year');
            $('select[name=workStartYear] option').attr('selected', false);
            $('select[name=workStartYear] option').filter('[value=select]')
                .attr('selected', true);
        }
    }
});

$('select[name=schoolEndYear]').on('change', function () {
    let prevSelection = $('select[name=schoolStartYear]').val();
    let newSelection = $(this).val();
    if(prevSelection > newSelection){
        alert('End year is invalid, Please change the work end year');
        $('select[name=schoolEndYear] option').attr('selected', false);
        $('select[name=schoolEndYear] option').filter('[value=select]')
            .attr('selected', true);
    }
});


$('select[name=schoolStartYear]').on('change', function () {
    let prevSelection = $('select[name=schoolEndYear]').val();
    let newSelection = $(this).val();
    if(prevSelection != null && prevSelection !="select"){
        if(prevSelection < newSelection){
            alert('End year is invalid, Please change the work end year');
            $('select[name=schoolStartYear] option').attr('selected', false);
            $('select[name=schoolStartYear] option').filter('[value=select]')
                .attr('selected', true);
        }
    }
});

//check is current workplace checkbox is checked
$('input[name=currentWorkPlace]').on('click', function () {
    if($(this).is(":checked")){
        $('select[name=workEndMonth]').attr('disabled', true);
        $('select[name=workEndYear]').attr('disabled', true);
    }
    else if($(this).is(":not(:checked)")){
        $('select[name=workEndMonth]').attr('disabled', false);
        $('select[name=workEndYear]').attr('disabled', false);
    }
});

//check is current studtyplace checkbox is checked
$('input[name=currentStudyInstitution]').on('change', function () {
    if($(this).is(":checked")){
        $('select[name=schoolEndMonth]').attr('disabled', true);
        $('select[name=schoolEndYear]').attr('disabled', true);
    }else if($(this).is(":not(:checked)")){
        $('select[name=schoolEndMonth]').attr('disabled', false);
        $('select[name=schoolEndYear]').attr('disabled', false);
    }
});


