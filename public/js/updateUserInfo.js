$('#updateUserPersonalInfo').on('click', function () {
    "use strict";
    let ths = $(this);
    let firstname = $('input[name=cfirstname]').val();
    let lastname = $('input[name=clastname]').val();
    let email = $('input[name=cemail]').val();
    let username = $('input[name=cusername]').val();
    let toSend = {firstname:firstname, lastname:lastname, email:email, username:username}
    console.log(toSend);
    ths.attr('disabled', true);
    $.ajax({
        type: 'POST',
        url: "/updateProfile2",
        data: toSend,
        success:function (data) {
            console.log(data);
            ths.attr('disabled', false);
            if(data.error){
                $('#error').modal('toggle');
                let msg = "<p>"+data.error+".</p>";
                $('#message').html(msg);
            }else if(data.success){
                $('#success').modal('toggle');
                let msg = "<p>"+data.success+".</p>";
                $('#successmsg').html(msg);
                $('.card-profile-name').html(firstname+" "+lastname);
                $('.dispEmail').html(email);
                $('#editPersonal').modal('toggle');
            }
        },
        error: function (error){
            console.log(error);
            ths.attr('disabled', false);
            $('#error').modal('toggle');
            let msg = "<p>"+error.error+".</p>";
            $('#message').html(msg);
        }
    });

});

$('#updateUserExperience').on('click', function () {
    let singleInfo = {};
    let ths = $(this);
    //let common = $('.singleJobExperience');
    let companyName = $('.updateExp').find('input[name=companyName]').val();
    let jobTitle = $('.updateExp').find('input[name=jobTitle]').val();
    let currentWorkPlace = $('.updateExp').find('input[name=currentWorkPlace]').val();
    let workStartMonth = $('.updateExp').find('select[name=workStartMonth]').val();
    let workStartYear = $('.updateExp').find('select[name=workStartYear]').val();
    let workEndMonth = $('.updateExp').find('select[name=workEndMonth]').val();
    let workEndYear = $('.updateExp').find('select[name=workEndYear]').val();
    let jobDescription = $('.updateExp').find('textarea[name=jobDescription]').val();
    let id = $('input[name=theId]').val();

    if($('input[name=currentWorkPlace]').is(":checked")){
        singleInfo['current'] = 1;
    }
    else if($('input[name=currentWorkPlace]').is(":not(:checked)")){
        singleInfo['current'] = 0;
    }


    singleInfo['companyName'] = companyName;
    singleInfo['jobTitle'] = jobTitle;
    singleInfo['currentWorkPlace'] = currentWorkPlace;
    singleInfo['workStartMonth'] = workStartMonth;
    singleInfo['workStartYear'] = workStartYear;
    singleInfo['workEndMonth'] = workEndMonth;
    singleInfo['workEndYear'] = workEndYear;
    singleInfo['jobDescription'] = jobDescription;
    singleInfo['id'] = id;
    console.log(singleInfo);
    ths.attr('disabled', true);
    $.ajax({
        type: 'POST',
        url: "/updateExperienceInfo",
        data: singleInfo,
        success:function (data) {
            console.log(data);
            ths.attr('disabled', false);
            if(data.error){
                $('#error').modal('toggle');
                let msg = "<p>"+data.error+".</p>";
                $('#message').html(msg);
            }else if(data.success){
                $('#success').modal('toggle');
                let msg = "<p>Work Experience status successfully updated.</p>";
                $('#successmsg').html(msg);
                //$('.exinfo').remove();
                $('#editExperience').modal('toggle');
                $('#exp'+id).find('.position-name').html(companyName);
                $('#exp'+id).find('.position-company').html(jobTitle);
                $('#exp'+id).find('.position-year').html(workStartMonth+", "+workStartYear+" - "+ (workEndMonth != null ? workEndMonth+", "+workEndYear : 'present')+" "+"<a class='editexperiencepop' data-toggle='modal' data-target='#editExperience' data-id='"+id+"' data-title='"+jobTitle+"' data-company='"+companyName+"'\
                data-workstartmonth='"+workStartMonth+"' data-workendmonth='"+workEndMonth+"' data-workstartyear='"+workStartYear+"'\
                data-workendyear='"+workEndYear+"' data-description='"+description+"' data-current='"+currentWorkPlace+"'>Edit</a>");
                /*$('.takemedia').
                append('<div class="media">\
                            <div class="experience-logo">\
                                <i class="icon ion-briefcase"></i>\
                            </div><!-- experience-logo -->\
                            <div class="media-body">\
                                <h6 class="position-name">'+data.success.info.company_name+'</h6>\
                                <p class="position-company">'+data.success.info.job_title+'</p>\
                                <p class="position-year">'+data.success.info.workStart_month+' '+ data.success.info.workStart_year +' - '+(data.success.info.workEnd_month != null && data.success.info.workEnd_year != null ? data.success.info.workEnd_month + "  " +data.success.info.workEnd_year: "- present")+'&nbsp;-&nbsp;\
                                    <a href="#">Edit</a>\
                                </p>\
                            </div><!-- media-body -->\
                        <div>');*/
                //common.find('input').val();
            }
        },
        error: function (error) {
            console.log(error);
            ths.attr('disabled', false);
        }
    });
});


$('#updateEducationInfo').on('click', function () {
    let singleInfo = {};
    let ths =$(this);
    let schoolName = $('.updatedu').find('input[name=nameOfSchool]').val();
    let schoolCountry = $('.updatedu').find('select[name=schoolCountry]').val();
    let schoolState = $('.updatedu').find('select[name=schoolState]').val();
    let schoolStartMonth = $('.updatedu').find('select[name=schoolStartMonth]').val();
    let schoolEndMonth = $('.updatedu').find('select[name=schoolEndMonth]').val();
    let schoolStartYear = $('.updatedu').find('select[name=schoolStartYear]').val();
    let schoolEndYear = $('.updatedu').find('select[name=schoolEndYear]').val();
    let currentStudyInstitution = $('.updatedu').find('input[name=currentStudyInstitution]').val();
    let studyField = $('.updatedu').find('input[name=studyField]').val();
    let educationLevel = $('.updatedu').find('select[name=educationLevel]').val();
    let regionOfResidence = $('.updatedu').find('select[name=regionOfResidence]').val();
    let id= $('.updatedu').find('input[name=eduId]').val();

    singleInfo['schoolName'] = schoolName;
    singleInfo['schoolCountry'] = schoolCountry;
    singleInfo['schoolState'] = schoolState;
    singleInfo['schoolStartMonth'] = schoolStartMonth;
    singleInfo['schoolEndMonth'] = schoolEndMonth;
    singleInfo['schoolStartYear'] = schoolStartYear;
    singleInfo['schoolEndYear'] = schoolEndYear;
    singleInfo['currentStudyInstitution'] = currentStudyInstitution;
    singleInfo['fieldOfStudy'] = studyField;
    singleInfo['educationLevel'] = educationLevel;
    singleInfo['regionOfResidence'] = regionOfResidence;
    singleInfo['id'] = id;

    if($('input[name=currentStudyInstitution]').is(":checked")){
        singleInfo['current'] = 1;
    }
    else if($('input[name=currentStudyInstitution]').is(":not(:checked)")){
        singleInfo['current'] = 0;
    }

    ths.attr('disabled', true);
    $.ajax({
        type: 'POST',
        url: "/updateEducationInfo",
        data: singleInfo,
        success:function(data){
            console.log(data);
            ths.attr('disabled', false);
            if(data.error){
                $('#error').modal('toggle');
                let msg = "<p>"+data.error+".</p>";
                $('#message').html(msg);
            }else if(data.fieldOfStudy){
                $('#error').modal('toggle');
                let msg = "<p>"+data.fieldOfStudy[0]+".</p>";
                $('#message').html(msg);
            }else  if(data.success){
                $('#success').modal('toggle');
                let msg = "<p>"+data.success+"</a>";
                $('#successmsg').html(msg);
                //$('.einfo').remove();
                $('#editEducation').modal('toggle');
                $('#edu'+id).find('.position-name').html(studyField);
                $('#edu'+id).find('.position-company').html(schoolName);
                $('#edu'+id).find('.position-year').html(schoolStartMonth+", "+schoolStartYear+" - "+ (schoolEndMonth != null ? schoolEndMonth+", "+schoolEndYear : 'present')+" "+"<a class='editEducationpop' data-toggle='modal' data-target='#editExperience' data-id='"+id+"' data-school='"+schoolName+"'data-schoolstartmonth='"+schoolStartMonth+"'\
                data-schoolstartyear='"+schoolStartYear+"' data-schoolendmonth='"+schoolEndMonth+"' data-schoolendyear='"+schoolEndYear+"'\
                data-country='"+schoolCountry+"' data-state='"+schoolState+"' data-field='"+studyField+"' data-education='"+educationLevel+"'>Edit</a>");
                /*$('.takemedia').
                append('<div class="media">\
                            <div class="experience-logo">\
                                <i class="icon ion-briefcase"></i>\
                            </div><!-- experience-logo -->\
                            <div class="media-body">\
                                <h6 class="position-name">'+data.success.info.school_name+'</h6>\
                                <p class="position-company">'+data.success.info.fieldOfStudy+'</p>\
                                <p class="position-year">'+data.success.info.schoolStart_month+' '+ data.success.info.schoolStart_year +' - '+(data.success.info.schoolEnd_month != null && data.success.info.schoolEnd_year != null ? data.success.info.schoolEnd_month + "  " +data.success.info.schoolEnd_year: "- present")+'&nbsp;-&nbsp;\
                                    <a href="#">Edit</a>\
                                </p>\
                            </div><!-- media-body -->\
                        <div>');*/
                //$('.singleEducationDetails').find('input').val('');
            }
        },
        error: function (error) {
            console.log(error);
            ths.attr('disabled', false);
        }
    });
});